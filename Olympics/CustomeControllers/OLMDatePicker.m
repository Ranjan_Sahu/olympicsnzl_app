//
//  UIDropdown.m
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMDatePicker.h"

@implementation OLMDatePicker
@synthesize delegate,increamentedHeight,optionTextfield,currentSelectedIndex,questionLbl;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired defaultValue:(NSString*)defaultValue delegate:(id)sender lineColor:(UIColor *)formatColor
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        currentSelectedIndex = -1;
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        
        questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        questionLbl.textColor = color_pinkFont;
        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:font}
                                                 context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.hidden = true;
        
        
        questionLbl.frame = CGRectMake(5 , increamentedHeight - 5, self.frame.size.width, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height - 5;
        questionLbl.text = question;
        
        
        [self addSubview:questionLbl];
        
        if(IS_IPAD)
        {
            optionTextfield = [[DesabledPasteOption_UITextField alloc] initWithFrame:CGRectMake(10, increamentedHeight, self.frame.size.width - self.frame.origin.x, 50)];
            [optionTextfield setFont:app_font24];
            increamentedHeight = increamentedHeight + 55;
            
        }
        else
        {
            optionTextfield = [[DesabledPasteOption_UITextField alloc] initWithFrame:CGRectMake(10, increamentedHeight, self.frame.size.width - self.frame.origin.x, 30)];
            [optionTextfield setFont:app_font13];
            increamentedHeight = increamentedHeight + 30;
        }

        [optionTextfield setBorderStyle:UITextBorderStyleNone];
        optionTextfield.textColor = color_gray;
        optionTextfield.placeholder = question;
        [self addSubview:optionTextfield];
        optionTextfield.delegate = self;
        
        optionsPicker = [[UIDatePicker alloc] init];
        optionsPicker.datePickerMode = UIDatePickerModeDate;
        optionsPicker.tag = 5;
        [optionsPicker addTarget:self action:@selector(dateIsChanged:) forControlEvents:UIControlEventValueChanged];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
        NSDate *date;
        
        if(defaultValue && ![defaultValue isKindOfClass:[NSNull class]] && [defaultValue length]>0)
        {
            date = [dateFormat dateFromString:defaultValue];
        }
        else
        {
            date = [NSDate date];
        }
        
        if(!date)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd/MM/yyyy"];
            
            if(defaultValue && ![defaultValue isKindOfClass:[NSNull class]] && [defaultValue length]>0)
            {
                date = [dateFormat dateFromString:defaultValue];

                NSDate *someDateInUTC = date;
                NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
                NSDate *dateInLocalTimezone = [someDateInUTC dateByAddingTimeInterval:timeZoneSeconds];
                date = dateInLocalTimezone;
            }
            else
            {
                date = [NSDate date];
            }
        }
        
        if(date)
        {
            [optionsPicker setDate:date];
            [self dateIsChanged:optionsPicker];

        }
        else
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"dd/MM/yyyy"];
            NSDate *date = [NSDate date];
            if(date)
            {
                [optionsPicker setDate:date];
                [self dateIsChanged:optionsPicker];
                
            }
        }
        
        
        UIToolbar *DoneButtonToolbar = [[UIToolbar alloc] init];
        [DoneButtonToolbar sizeToFit];
        DoneButtonToolbar.tintColor = [UIColor colorWithRed:0.1176 green:0.0902 blue:0.2549 alpha:1.0f];
        UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleDone target:self
                                                                      action:@selector(PickerViewShouldHide)];
        doneButton.tag = 5;
        [DoneButtonToolbar setItems:[NSArray arrayWithObjects:flexibleItem,doneButton, nil]];
        optionTextfield.inputAccessoryView = DoneButtonToolbar;
        optionTextfield.inputView = optionsPicker;
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:formatColor];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            
            
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 18 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];
            }
            else
            {
               mandetoryLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 30 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];
            }
            
            mandetoryLabel.textColor = [UIColor blackColor];
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
        }
        
    }
        return self;
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return TRUE;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [delegate OLMDatepickerDidStartEditing:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [delegate OLMDatepickerDidEndEditing:textField];
}


#pragma mark - PickerView Delegate

-(void)dateIsChanged:(id)datePicker
{
    questionLbl.hidden = FALSE;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    optionTextfield.text = [dateFormatter stringFromDate:[datePicker date]];
}

-(void)PickerViewShouldHide
{
    [optionTextfield resignFirstResponder];
}


-(NSDictionary*)GetAnswerInfo
{
    if(_IsRequired && [optionTextfield.text length] == 0)
    {
        return nil;
    }
    NSDictionary *response = @{ [contollerInfo valueForKey:@"FieldIdName"]:optionTextfield.text };
    return response;
}




@end

//
//  UIDropdown.m
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMDropDown.h"

@implementation OLMDropDown
@synthesize delegate,increamentedHeight,optionTextfield,currentSelectedIndex,questionLbl;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId Options:(NSArray*)options IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)value delegate:(id)sender lineColor:(UIColor *)formatColor
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        currentSelectedIndex = -1;
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        optionsList = [[NSArray alloc] initWithArray:options];
        
        questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        questionLbl.textColor = color_pinkFont;
        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:font}
                                             context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.hidden = true;
        
        questionLbl.frame = CGRectMake(5 , increamentedHeight - 5, self.frame.size.width, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height - 5;
        questionLbl.text = question;
        
        [self addSubview:questionLbl];
        
        if(IS_IPAD)
        {
            optionTextfield = [[DesabledPasteOption_UITextField alloc] initWithFrame:CGRectMake(10, increamentedHeight, self.frame.size.width - self.frame.origin.x, 50)];
            [optionTextfield setFont:app_font24];
            increamentedHeight = increamentedHeight + 55;
            
        }
        else
        {
            optionTextfield = [[DesabledPasteOption_UITextField alloc] initWithFrame:CGRectMake(10, increamentedHeight, self.frame.size.width - self.frame.origin.x, 30)];
            [optionTextfield setFont:app_font13];
            increamentedHeight = increamentedHeight + 30;
        }
        
        [optionTextfield setBorderStyle:UITextBorderStyleNone];
        
        optionTextfield.textColor = color_gray;
        optionTextfield.placeholder = question;
        [self addSubview:optionTextfield];
        optionTextfield.delegate = self;
        
        
        optionsPicker = [[UIPickerView alloc] init];
        optionsPicker.dataSource = self;
        optionsPicker.delegate = self;
        optionsPicker.tag = 5;
        
        if(value >= 0)
        {
            NSUInteger index = [options indexOfObjectPassingTest:
                                ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
                                {
                                    return [[dict objectForKey:@"Key"] integerValue] == value;
                                }
                                ];
            if(index != NSNotFound)
            {
                [optionsPicker selectRow:index inComponent:0 animated:YES];
                [self pickerView:optionsPicker didSelectRow:index inComponent:0];
            }
        }
        
        UIToolbar *DoneButtonToolbar = [[UIToolbar alloc] init];
        [DoneButtonToolbar sizeToFit];
        DoneButtonToolbar.tintColor = [UIColor colorWithRed:0.1176 green:0.0902 blue:0.2549 alpha:1.0f];
        UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleDone target:self
                                                                      action:@selector(PickerViewShouldHide)];
        doneButton.tag = 5;
        [DoneButtonToolbar setItems:[NSArray arrayWithObjects:flexibleItem,doneButton, nil]];
        optionTextfield.inputAccessoryView = DoneButtonToolbar;
        optionTextfield.inputView = optionsPicker;
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:formatColor];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            mandetoryLabel.textAlignment = NSTextAlignmentCenter;
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 20 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];
            }
            else
            {
                mandetoryLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 30 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];
            }
            mandetoryLabel.textColor = [UIColor blackColor];
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
            
            UILabel *dropLabel = [[UILabel alloc] init];
            dropLabel.textAlignment = NSTextAlignmentCenter;
            
            if(IS_IPAD)
            {
                dropLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 60 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 50, 50);
                dropLabel.font = [UIFont fontWithName:@"icomoon" size:24];
            }
            else
            {
                dropLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 60 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 30, 30);
                dropLabel.font = [UIFont fontWithName:@"icomoon" size:13];
            }
            
            dropLabel.textColor = formatColor;
            dropLabel.text = @"\ue921";
            [self addSubview:dropLabel];
        }
        else
        {
            UILabel *dropLabel = [[UILabel alloc] init];
            dropLabel.textAlignment = NSTextAlignmentCenter;
            
            if(IS_IPAD)
            {
                dropLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 20 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 50, 50);
                dropLabel.font = [UIFont fontWithName:@"icomoon" size:24];
            }
            else
            {
                dropLabel.frame = CGRectMake(optionTextfield.frame.origin.x - 30 + optionTextfield.frame.size.width, optionTextfield.frame.origin.y, 30, 30);
                dropLabel.font = [UIFont fontWithName:@"icomoon" size:13];
            }
            
            dropLabel.textColor = formatColor;
            dropLabel.text = @"\ue921";
            [self addSubview:dropLabel];
        }
    }
        return self;
}

#pragma mark - TextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([optionsList count]>0)
    {
        return TRUE;
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:@"No options available."
                                                                preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Ok"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                    }];
        [alert addAction:alert_cancel_action];
        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
        
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No options available." delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        //[alert show];
        
        return FALSE;
    }
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self pickerView:optionsPicker didSelectRow:[optionsPicker selectedRowInComponent:0] inComponent:0];
    [delegate OLMDropdownDidStartEditing:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [delegate OLMDropdownDidEndEditing:textField];
}


#pragma mark - PickerView Delegate
-(void)PickerViewShouldHide
{
    [optionTextfield resignFirstResponder];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return  [optionsList count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[optionsList objectAtIndex:row] valueForKey:@"Value"];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    questionLbl.hidden = FALSE;
    optionTextfield.text = [[optionsList objectAtIndex:row] valueForKey:@"Value"];
    currentSelectedIndex = row;
}

-(NSDictionary*)GetAnswerInfo
{
    if(currentSelectedIndex <= 0)
    {
        return nil;
    }
    NSDictionary *response = @{ [contollerInfo valueForKey:@"FieldIdName"]:[[optionsList objectAtIndex:currentSelectedIndex] valueForKey:@"Key"] };
    return response;
}

-(void) setSelected:(NSInteger) index{
    [self pickerView:optionsPicker didSelectRow:0 inComponent:0];
}

-(NSDictionary*)GetHardcodedAnswer
{
    NSDictionary *response = @{ [contollerInfo valueForKey:@"FieldIdName"]:@0 };
    return response;
}




@end

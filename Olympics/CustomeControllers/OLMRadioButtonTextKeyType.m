//
//  UIRadioButton.m
//  MIRadioButtonGroup
//
//  Created by Vikas Karambalkar on 25/10/14.
//
//

#import "OLMRadioButtonTextKeyType.h"

@implementation OLMRadioButtonTextKeyType
@synthesize delegate, increamentedHeight,currentSelectedIndex;
-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId options:(NSArray*)optionsList IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)defaultValue delegate:(id)sender lineColor:(UIColor *)formatColor
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        optionsArray = [NSArray arrayWithArray:optionsList];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        currentSelectedIndex = -1;

        radioButtons = [[NSMutableArray alloc]init];
        UILabel *questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        questionLbl.textColor = color_pinkFont;
        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width-10, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:font}
                                                 context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.frame = CGRectMake(5 , increamentedHeight - 5, self.frame.size.width, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height + 5;
        questionLbl.text = question;
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(questionLbl.frame.origin.x - 50 + questionLbl.frame.size.width, questionLbl.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];
            }
            else
            {
                mandetoryLabel.frame = CGRectMake(questionLbl.frame.origin.x - 33 + questionLbl.frame.size.width, questionLbl.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];
            }
            
            mandetoryLabel.textColor = [UIColor blackColor];
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
        }
        
        [self addSubview:questionLbl];
        for (int i = 0; i < [optionsArray count]; i++)
        {
            UIFont *lblfont = app_font13;
            if(IS_IPAD)
                lblfont = app_font24;
            
            CGSize stringsize = [[[optionsArray objectAtIndex:i] valueForKey:@"strVal"] sizeWithFont:lblfont constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
            
            UIButton *btTemp = [[UIButton alloc]initWithFrame:CGRectMake(0, increamentedHeight,self.frame.size.width - 10,stringsize.height)];
            [btTemp.titleLabel setFont:lblfont];
            [btTemp setTitleColor:color_gray forState:UIControlStateNormal];
            btTemp.titleEdgeInsets = UIEdgeInsetsMake(-3, 10, 0, 0);
            btTemp.titleLabel.numberOfLines = 50;
             increamentedHeight = increamentedHeight + stringsize.height + 5;
            [btTemp addTarget:self action:@selector(radioButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            btTemp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            btTemp.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
            [btTemp setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
            [btTemp.imageView setContentMode:UIViewContentModeScaleAspectFit];
            [btTemp setTitle:[[optionsArray objectAtIndex:i] valueForKey:@"strVal"] forState:UIControlStateNormal];
            btTemp.tag = i;
            [radioButtons addObject:btTemp];
            [self addSubview:btTemp];
        }
        
        if(defaultValue > 0)
        {
            NSUInteger index = [optionsArray indexOfObjectPassingTest:
                                ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
                                {
                                    return [[dict objectForKey:@"strKey"] integerValue] == defaultValue;
                                }
                                ];
            if(index != NSNotFound)
            {
                [self setSelected:index];
            }
        }
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:formatColor];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        
    }
    return self;
}


-(void) radioButtonClicked:(UIButton *) sender{
    for(int i=0;i<[radioButtons count];i++){
        [[radioButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        
    }
    [sender setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateNormal];
    if([delegate respondsToSelector:@selector(OLMRadioButtonTextKeyType:didSelectOptionAtIndex:)])
    {
        [delegate OLMRadioButtonTextKeyType:self didSelectOptionAtIndex:[sender tag]];
    }
    currentSelectedIndex = [sender tag];

}

-(void) setSelected:(NSInteger) index{
    [self radioButtonClicked:[radioButtons objectAtIndex:index]];
}

-(void)clearAll{
    for(int i=0;i<[radioButtons count];i++){
        [[radioButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
    }
}

-(NSDictionary*)GetAnswerInfo
{
    if(currentSelectedIndex < 0)
    {
        return nil;
    }
    NSDictionary *response = @{ [contollerInfo valueForKey:@"FieldIdName"]:[[optionsArray objectAtIndex:currentSelectedIndex] valueForKey:@"strKey"] };
    return response;
}


@end

//
//  UIDropdown.h
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DesabledPasteOption_UITextField.h"
@class OLMDatePicker;
@protocol OLMDdatePickerDelegate<NSObject>
-(void)OLMDatepickerDidStartEditing:(UITextField*)textField;
-(void)OLMDatepickerDidEndEditing:(UITextField*)textField;
@required
@end


@interface OLMDatePicker : UIView<UITextFieldDelegate>
{
    UIDatePicker *optionsPicker;
    NSMutableDictionary *contollerInfo;
}

@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property(nonatomic,retain) DesabledPasteOption_UITextField *optionTextfield;
@property(nonatomic,retain) UILabel *questionLbl;
@property (weak)id<OLMDdatePickerDelegate>delegate;
@property (nonatomic) NSInteger currentSelectedIndex;
@property (nonatomic) NSInteger questionTypeId;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired defaultValue:(NSString*)defaultValue delegate:(id)sender lineColor:(UIColor *)formatColor;
-(NSDictionary*)GetAnswerInfo;
@end

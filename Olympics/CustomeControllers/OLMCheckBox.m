//
//  UICheckBox.m
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMCheckBox.h"

@implementation OLMCheckBox
@synthesize increamentedHeight, delegate,questionLbl;
-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId options:(NSArray*)optionsList IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)defaultValue delegate:(id)sender lineColor:(UIColor *)formatColor
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    
    CGFloat btnWidth = 16;
    if(IS_IPAD)
        btnWidth = 20;
    
    if (self)
    {
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        optionsArray = [NSArray arrayWithArray:optionsList];
        _selectedOptionIdArray = [[NSMutableArray alloc]init];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        checkBoxButtons = [[NSMutableArray alloc]init];
        questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        
        questionLbl.textColor = color_pinkFont;
        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:font}
                                                 context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.frame = CGRectMake(5 , increamentedHeight - 5, self.frame.size.width - 10, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height + 10;
        
        questionLbl.text = question;
        [self addSubview:questionLbl];
        
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(questionLbl.frame.origin.x - 50 + questionLbl.frame.size.width, questionLbl.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];

            }
            else
            {
                mandetoryLabel.frame = CGRectMake(questionLbl.frame.origin.x - 30 + questionLbl.frame.size.width, questionLbl.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];

            }
            mandetoryLabel.textColor = [UIColor blackColor];
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
        }
        
        for (int i = 0; i < [optionsArray count]; i++)
        {
            UIFont *lblfont = app_font13;
            if(IS_IPAD)
                lblfont = app_font24;
            
            //CGSize stringsize = [[[optionsArray objectAtIndex:i] valueForKey:@"strVal"] sizeWithFont:lblfont constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
            
            CGSize stringsize = [[[optionsArray objectAtIndex:i] valueForKey:@"AHOption"] sizeWithFont:lblfont constrainedToSize:CGSizeMake(self.frame.size.width - btnWidth - 20, CGFLOAT_MAX)];

            //UIButton *btTemp = [[UIButton alloc]initWithFrame:CGRectMake( 0, increamentedHeight,self.frame.size.width - 10,stringsize.height)];
            
            UIButton *btTemp = [[UIButton alloc]initWithFrame:CGRectMake( 0, increamentedHeight,btnWidth,btnWidth)];

            UILabel *btTextLabel = [[UILabel alloc]initWithFrame:CGRectMake( btnWidth + 10, increamentedHeight-2,self.frame.size.width - btnWidth - 20,stringsize.height)];

            //[btTemp setTitleColor:color_gray forState:UIControlStateNormal];
            //[btTemp.titleLabel setFont:lblfont];
            //btTemp.titleLabel.numberOfLines = 50;
            
            btTextLabel.textColor = color_gray;
            btTextLabel.font = lblfont;
            btTextLabel.numberOfLines = 50;
            [btTextLabel layoutIfNeeded];
            
            increamentedHeight = increamentedHeight + stringsize.height + 10;
            [btTemp addTarget:self action:@selector(CheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            //btTemp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //btTemp.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
            [btTemp setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
             [btTemp.imageView setContentMode:UIViewContentModeScaleAspectFit];
            
            //[btTemp setTitle:[[optionsArray objectAtIndex:i] valueForKey:@"AHOption"] forState:UIControlStateNormal];
            //btTemp.titleEdgeInsets = UIEdgeInsetsMake(-3, 10, 0, 0);
            
            btTextLabel.text = [[optionsArray objectAtIndex:i] valueForKey:@"AHOption"];
            
            btTemp.tag = i;
            [checkBoxButtons addObject:btTemp];
            [self addSubview:btTemp];
            [self addSubview:btTextLabel];
            
            if([[[optionsArray objectAtIndex:i] valueForKey:@"IsSelected"] boolValue])
            {
                [self setSelected:i];
            }
        }
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:formatColor];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
    }
    return self;
}

-(void) CheckBoxButtonClicked:(UIButton *) sender{
    if(!sender.isAccessibilityElement)
    {
        sender.isAccessibilityElement = TRUE;
        [sender setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
          [_selectedOptionIdArray addObject:[optionsArray objectAtIndex:sender.tag]];
    }
    else
    {
        sender.isAccessibilityElement = FALSE;
        [sender setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
        
        if ([_selectedOptionIdArray containsObject:[optionsArray objectAtIndex:sender.tag]]) {
            [_selectedOptionIdArray removeObject:[optionsArray objectAtIndex:sender.tag]];
        }

    }
    
    if([delegate respondsToSelector:@selector(OLMCheckBox:didSelectOptionAtIndex:)])
    {
        [delegate OLMCheckBox:self didSelectOptionAtIndex:[sender tag]];
    }
}

-(void)clearAll{
    for(int i=0;i<[checkBoxButtons count];i++){
        [[checkBoxButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
    }
}

-(NSDictionary*)GetAnswerInfo
{
   NSMutableArray *AnswersList = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < [checkBoxButtons count]; i++)
    {
        UIButton *btn = (UIButton*)checkBoxButtons[i];
        if(btn.isAccessibilityElement)
        {
            [AnswersList addObject:[optionsArray objectAtIndex:i]];
        }
    }
     [contollerInfo setObject:AnswersList forKey:@"AnswerObject"];
    return contollerInfo;
}

-(void) setSelected:(NSInteger) index{
    [self CheckBoxButtonClicked:[checkBoxButtons objectAtIndex:index]];
}

@end

//
//  UITextFieldView.m
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMTextField.h"

@implementation OLMTextField
@synthesize increamentedHeight,delegate,textFieldView,questionLbl;
-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired defaultValue:(NSString*)defaultValue keyboardType:(UIKeyboardType)keyboard delegate:(id)sender lineColor:(UIColor *)formatColor
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    
    if (self)
    {
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        
        questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        
        questionLbl.textColor = color_pinkFont;

        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:font}
                                                 context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.frame = CGRectMake(5 , increamentedHeight - 5, self.frame.size.width, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height - 5;
        questionLbl.text = question;
        
        [self addSubview:questionLbl];
        
        if(IS_IPAD)
        {
            textFieldView = [[UITextField alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width - self.frame.origin.x, 50)];
            [textFieldView setFont:app_font24];
            increamentedHeight = increamentedHeight + 55;

        }
        else
        {
            textFieldView = [[UITextField alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width - self.frame.origin.x, 30)];
            [textFieldView setFont:app_font13];
            increamentedHeight = increamentedHeight + 30;
        }
        
        textFieldView.backgroundColor = [UIColor clearColor];
        textFieldView.textColor = color_gray;
        textFieldView.textAlignment = NSTextAlignmentLeft;
        textFieldView.placeholder = question;
        textFieldView.text = [NSString stringWithFormat:@"%@",defaultValue];
        if([textFieldView.text length] == 0)
        {
            questionLbl.hidden = TRUE;
        }
        [self addSubview:textFieldView];
        textFieldView.delegate = self;
        textFieldView.keyboardType = keyboard;
        [self SetLeftPaddingFor:textFieldView];
        
        UIToolbar *DoneButtonToolbar = [[UIToolbar alloc] init];
        [DoneButtonToolbar sizeToFit];
        DoneButtonToolbar.tintColor = [UIColor colorWithRed:0.1176 green:0.0902 blue:0.2549 alpha:1.0f];
        UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleDone target:self
                                                                      action:@selector(ResignFirstResponderTextView)];
        doneButton.tag = 5;
        [DoneButtonToolbar setItems:[NSArray arrayWithObjects:flexibleItem,doneButton, nil]];
        textFieldView.inputAccessoryView = DoneButtonToolbar;
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:formatColor];
            
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            mandetoryLabel.textColor = [UIColor blackColor];
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(textFieldView.frame.origin.x - 5 + textFieldView.frame.size.width, textFieldView.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];
            }
            else
            {
                mandetoryLabel.frame = CGRectMake(textFieldView.frame.origin.x - 18 + textFieldView.frame.size.width, textFieldView.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];
            }
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
        }
    }
    return self;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
   if(self.questionTypeId == Field_NumericTextField)
   {
       [[NSOperationQueue mainQueue] addOperationWithBlock:^{
           [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
       }];
   }
    return NO;
}

-(void)ResignFirstResponderTextView
{
    [textFieldView resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [delegate OLMTextFieldDidBeginEditing:textField];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
     [delegate OLMTextFieldDidEndEditing:textField];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[text stringByTrimmingCharactersInSet: set] length] == 0)
    {
        if([text length] == 0)
            return true;
        else
            return false;
    }
    else
    {
        if([text length] > 0)
        {
            questionLbl.hidden = false;
        }
        else
        {
            if(_isQuestionNeeded == TRUE)
                questionLbl.hidden = false;
            else
                questionLbl.hidden = true;
            
        }
        return true;
    }
}

-(NSDictionary*)GetAnswerInfo
{
    if(_IsRequired && [textFieldView.text length] == 0)
    {
        return nil;
    }
        
    NSDictionary *response = @{ [contollerInfo valueForKey:@"FieldIdName"]:textFieldView.text };
    return response;
    
}

-(void)SetLeftPaddingFor:(UITextField*)textfield
{
    UIImageView *imgArrow = [[UIImageView alloc] init];
    imgArrow.frame = CGRectMake(0.0, 0.0, 10.0, textfield.frame.size.height);
    imgArrow.contentMode = UIViewContentModeCenter;
    
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = imgArrow;
}


@end

//
//  UICheckBox.h
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OLMSingleCheckBox;
@protocol OLMSingleCheckBoxDelegate<NSObject>
@required
-(void)OLMSingleCheckBox:(OLMSingleCheckBox*)singleCheckBox didSelectOptionAtIndex:(NSInteger)index;
@end

@interface OLMSingleCheckBox : UIView
{
    NSMutableArray *checkBoxButtons;
    NSMutableDictionary *contollerInfo;
    NSArray *optionsArray;
}

@property (weak)id<OLMSingleCheckBoxDelegate>delegate;
@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property (nonatomic) NSInteger questionTypeId;
@property (nonatomic) UIButton *clickableButton;


-(id)initWithFrame:(CGRect)frame questionId:(NSInteger)questionId fieldIdName:(NSString*)fieldIdName OptionTitle:(NSString*)OptionTitle isRequired:(BOOL)IsRequired defaultSelected:(BOOL)defaultSelected delegate:(id)sender lineColor:(UIColor *)formatColor;
-(NSDictionary*)GetAnswerInfo;

@end

//
//  CommonSettings.h
//  SipSnapp
//
//  Created by Derek on 23/05/14.
//  Copyright (c) 2014 SS. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface CommonSettings : NSObject


@property (nonatomic) BOOL isInternetReachable;


+(CommonSettings*) sharedInstance;
-(void) resetContents;

-(void) showHUD;
-(void) hideHUD;

-(BOOL)isInternetReachable ;
-(void)AlertViewWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelButtonTitle InView:(id)viewController;
-(NSString*)requiredScreenForName:(NSString*)screen;
- (BOOL)validateEmailWithString:(NSString*)email;
- (BOOL)validatePhone:(NSString *)phoneNumber;
- (BOOL)validateOnlyNumeric: (NSString *)fieldText;
-(float) getHeightForText:(NSString*)text withFont:(UIFont*)font andWidth:(float)width minHeight:(float)minHeight;
-(float) getWidthForText:(NSString*)text withFont:(UIFont*)font andHeight:(float)height minWidth:(float)minWidth;
-(NSString*)getDateOnlyString:(NSString *)dateString;

//-(NSString*)Get24HourTime;
@end

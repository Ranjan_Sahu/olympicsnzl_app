//
//  CommonSettings.m
//  SipSnapp
//
//  Created by Derek on 23/05/14.
//  Copyright (c) 2014 SS. All rights reserved.
//

#import "CommonSettings.h"
#import "MBProgressHUD.h"
#import "UIImage+animatedGIF.h"


@interface CommonSettings () <MBProgressHUDDelegate>
@property(nonatomic, strong) MBProgressHUD *progressView;
@property (atomic, strong) UIActivityIndicatorView* indicator;

@end

@implementation CommonSettings
{
    UIImageView *loader;
}
@synthesize progressView;
@synthesize isInternetReachable;


static CommonSettings *sharedInstance;


+(CommonSettings*) sharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[CommonSettings alloc] init];
        [sharedInstance resetContents];
    }
    return sharedInstance;
}

-(void) resetContents
{
    if (self.progressView) {
        [self.progressView hide:NO];
    }
   
}

-(void)allocateLoaderImageView
{
    loader=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, 250, 200)];
    [loader setBackgroundColor:[UIColor clearColor]];
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Olympic_Rings_loader" withExtension:@"gif"];
    loader.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
}

//-(void) showHUD
//{
//    if (self.progressView)
//    {
//        [self.progressView hide:NO];
//    }
//    self.progressView = [MBProgressHUD showHUDAddedTo:APP_DELEGATE.window animated:YES];
//    self.progressView.animationType = MBProgressHUDAnimationFade;
//    self.progressView.mode = MBProgressHUDModeIndeterminate;
//    self.progressView.removeFromSuperViewOnHide = YES;
//    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
//}

//- (void)showProgressHud :(NSString *)messageOnHud
-(void) showHUD
{
    if(!loader)
    {
        [self allocateLoaderImageView];
    }
    if(self.progressView)
    {
        [self.progressView hide:YES];
        [self.progressView removeFromSuperview];
        self.progressView = nil;
    }
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    self.progressView = [[MBProgressHUD alloc] init];
    self.progressView.labelFont = app_font16;
    self.progressView.mode=MBProgressHUDModeCustomView;
    self.progressView.labelText=nil;
    self.progressView.detailsLabelText=nil;
    self.progressView.color=[UIColor clearColor];
    UIView *view1=[[UIView alloc] initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [view1 setBackgroundColor:[UIColor clearColor]];
    [view1.layer setMasksToBounds:YES];
    UIImageView *backView=[[UIImageView alloc] initWithFrame:view1.frame];
    [backView setBackgroundColor:[UIColor blackColor]];
    backView.alpha=0.6;
    [view1 addSubview:backView];
    loader.center=view1.center;
    
//    UIView *view2=[[UIView alloc] initWithFrame:CGRectMake(0,0, 300,200)];
//    [view2 setBackgroundColor:[UIColor whiteColor]];
//    [view2.layer setCornerRadius:12.0];
//    [view2.layer setMasksToBounds:YES];
//    view2.center=view1.center;
//    [view1 addSubview:view2];
   
    [view1 addSubview:loader];
    
    self.progressView.customView=view1;
    self.progressView.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [window addSubview:self.progressView];
    [self.progressView show:YES];
}

-(void) hideHUD
{
    [self.progressView hide:YES];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
}

-(void) showActivityIndicator
{
    if (self.indicator) {
        [self.indicator stopAnimating];
        [self.indicator removeFromSuperview];
    }
    
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicator.color = [UIColor colorWithRed:87.0f/255.0f green:96.0f/255.0f blue:25.0f/255.0f alpha:1.0f];
    self.indicator.center = CGPointMake(APP_DELEGATE.window.center.x, 270);
    
    [self.indicator startAnimating];
    [APP_DELEGATE.window addSubview:self.indicator];
}

-(void) hideActivityIndicator
{
    for (UIActivityIndicatorView *activity in [APP_DELEGATE.window subviews])
    {
        if ([activity isKindOfClass:[UIActivityIndicatorView class]])
        {
            [activity stopAnimating];
            activity.hidden = YES;
            [activity removeFromSuperview];
            [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.0]];
            break;
        }
    }
}


-(BOOL)isInternetReachable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if(networkStatus == NotReachable)
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}


//// Method returns 24 hr time
//-(NSString*)Get24HourTime
//{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [ formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];
//    NSString *dateString = [formatter stringFromDate:[NSDate date]];
////    dateString = @"2015-01-22 12:22:28 pm.774";
//    NSRange amRange = [dateString rangeOfString:@"am"];
//    NSRange pmRange = [dateString rangeOfString:@"pm"];
//    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
//
//    if(!is24h)
//    {
//        NSString *date = [[dateString componentsSeparatedByString:@" "] objectAtIndex:0];
//        NSString *time = [[dateString componentsSeparatedByString:@" "] objectAtIndex:1];
//        NSString *ampm = [[dateString componentsSeparatedByString:@" "] objectAtIndex:2];
//        NSString *hrs = [[time componentsSeparatedByString:@":"] objectAtIndex:0];
//        
//        if ([[dateString lowercaseString] rangeOfString:@"pm"].location != NSNotFound)
//        {
//            int convertHrs = [hrs intValue];
//            if(convertHrs != 12)
//                convertHrs  = convertHrs + 12;
//            
//            hrs = [NSString stringWithFormat:@"%d",convertHrs];
//        } else
//        {
//            int convertHrs = [hrs intValue];
//            if(convertHrs == 12)
//                convertHrs  = 0;
//            hrs = [NSString stringWithFormat:@"%d",convertHrs];
//        }
//        dateString = [NSString stringWithFormat:@"%@ %@:%@:%@.%@",date,hrs, [[time componentsSeparatedByString:@":"] objectAtIndex:1], [[time componentsSeparatedByString:@":"] objectAtIndex:2], [[ampm componentsSeparatedByString:@"."] objectAtIndex:1]];
//    }
//    return dateString;
//}

#pragma mark - Date Formatting methods
-(NSString*)getDateOnlyString:(NSString *)dateString
{
    if([dateString isEqualToString:@""] || [dateString isKindOfClass:[NSNull class]])
    {
        return @"-";
    }
    else {
        NSString *dateOnlyString = @"";
        
        // Original date
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSDate *convertedDate = [df dateFromString:dateString];
        
        // Formate original date
        NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
        [df1 setDateFormat:@"dd/MM/yyyy"];
        dateOnlyString = [df1 stringFromDate:convertedDate];
        
        return  dateOnlyString;
    }
}

-(void)AlertViewWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelButtonTitle InView:(id)viewController
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                                  style:(UIAlertActionStyleCancel)
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
    [alert addAction:alert_cancel_action];
    [viewController presentViewController:alert animated:YES completion:nil];
}

-(NSString*)requiredScreenForName:(NSString*)screen
{
    if(IS_IPAD)
    {
        //NSLog(@"ipadScreen");
        screen = [screen stringByAppendingString:@"Ipad"];
    }
    return screen;
}

-(float) getHeightForText:(NSString*)text withFont:(UIFont*)font andWidth:(float)width minHeight:(float)minHeight
{
    CGSize constraint = CGSizeMake(width , 20000.0f);
    CGSize title_size;
    float totalHeight;
    
    SEL selector = @selector(boundingRectWithSize:options:attributes:context:);
    if ([text respondsToSelector:selector]) {
        title_size = [text boundingRectWithSize:constraint
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{ NSFontAttributeName : font }
                                        context:nil].size;
        
        totalHeight = ceil(title_size.height);
    } else {
        title_size = [text sizeWithFont:font
                      constrainedToSize:constraint
                          lineBreakMode:NSLineBreakByWordWrapping];
        totalHeight = title_size.height ;
    }
    
    CGFloat height = MAX(totalHeight, minHeight);
    return height;
}

-(float) getWidthForText:(NSString*)text withFont:(UIFont*)font andHeight:(float)height minWidth:(float)minWidth
{
    
    CGSize size = [text sizeWithAttributes:
                   @{NSFontAttributeName: font}];
    CGSize adjustedSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    CGFloat width =  adjustedSize.width;
    return width;
}

#pragma - mark Validation Methods

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

- (BOOL)validateOnlyNumeric: (NSString *)fieldText
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([fieldText rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        return TRUE;
    else
        return FALSE;
}
@end

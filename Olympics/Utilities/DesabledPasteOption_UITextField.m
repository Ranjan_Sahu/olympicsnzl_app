//
//  DesabledPasteOption.m
//  SalesAuto
//
//  Created by Vikas Karambalkar on 08/01/15.
//  Copyright (c) 2015 vikas. All rights reserved.
//

#import "DesabledPasteOption_UITextField.h"

@implementation DesabledPasteOption_UITextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    return false;
}

@end

//
//  myGamesCollectionViewCell.h
//  Olympics
//
//  Created by webwerks on 11/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myGamesCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewGameBanner;
@property (weak, nonatomic) IBOutlet UILabel *lblGameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gameTitleLabelHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidth;

@end

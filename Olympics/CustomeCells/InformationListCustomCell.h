//
//  InformationListCustomCell
//  Olympics
//
//  Created by webwerks on 7/26/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationListCustomCell : UITableViewCell<UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblEventNameText;
@property (weak, nonatomic) IBOutlet UILabel *lblFieldText;
@property (weak, nonatomic) IBOutlet UIButton *btnFormSubmitted;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnFormSubmittedWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnFormSubmittedHeight;
@property (weak, nonatomic) IBOutlet UILabel *DateLabel;
@property (weak, nonatomic) IBOutlet UIView *viewFormBack;

@end

//
//  addressListTableViewCell.m
//  Olympics
//
//  Created by webwerks2 on 7/12/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "addressListTableViewCell.h"

@implementation addressListTableViewCell
@synthesize lblLock,lblDisplayAddress,lblAddressName;

- (void)awakeFromNib {
    [super awakeFromNib];
    if(IS_IPAD)
    {
        [self.lblAddressName setFont: app_font24];
        [self.lblDisplayAddress setFont: app_font18];
    }
    else if (IS_IPHONE)
    {
        [self.lblAddressName setFont: app_font18];
        [self.lblDisplayAddress setFont: app_font15];
    }    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  DropDownCustomCell.h
//  Olympics
//
//  Created by webwerks on 7/28/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitleText;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgCount;
@property (weak, nonatomic) IBOutlet UIView *viewMsgCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthViewMsgCount;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewMsgCount;

@end

//
//  CollectionViewCell.m
//  Olympics
//
//  Created by webwerks on 07/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "myProfileCollectionViewCell.h"

@implementation myProfileCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    if(IS_IPAD)
    {
        self.titleLebel.font = app_font_bold_15;
        self.submenuLogoLabel.font = [UIFont fontWithName:@"icomoon" size:60];
    }
    else
    {
        self.titleLebel.font = app_font_bold_10;
        self.submenuLogoLabel.font = [UIFont fontWithName:@"icomoon" size:35];
    }
    //self.visibilityMark.layer.cornerRadius = self.visibilityMark.frame.size.height/2;
    //self.visibilityMark.clipsToBounds = YES;
}

@end

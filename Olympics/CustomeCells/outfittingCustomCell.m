//
//  outfittingCustomCell.m
//  Olympics
//
//  Created by Vicky on 18/08/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "outfittingCustomCell.h"

@implementation outfittingCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [APP_DELEGATE SetLeftPaddingFor:_notesTextField width:10 imageIfAny:nil];
    [APP_DELEGATE SetRightPaddingFor:_notesTextField width:10 imageIfAny:nil];

    // Configure the view for the selected state
}

@end

//
//  AdhocCustomCell.m
//  Olympics
//
//  Created by webwerks on 8/31/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AdhocCustomCell.h"

@implementation AdhocCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

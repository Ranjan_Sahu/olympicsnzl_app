//
//  CollectionViewCell.h
//  Olympics
//
//  Created by webwerks on 07/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myProfileCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *blueView;
@property (weak, nonatomic) IBOutlet UILabel *titleLebel;
@property (weak, nonatomic) IBOutlet UIImageView *visibilityMark;

//@property (weak, nonatomic) IBOutlet UIImageView *visibilityMark;
@property (weak, nonatomic) IBOutlet UILabel *submenuLogoLabel;


@end

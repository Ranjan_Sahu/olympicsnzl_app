//
//  contactListTableViewCell.h
//  Olympics
//
//  Created by webwerks2 on 7/13/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface contactListTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailType;
@property (weak, nonatomic) IBOutlet UITextField *textEmail;
@property (weak, nonatomic) IBOutlet UITextField *textEmailType;

@end

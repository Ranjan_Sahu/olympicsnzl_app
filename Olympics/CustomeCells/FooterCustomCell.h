//
//  FooterCustomCell.h
//  Olympics
//
//  Created by webwerks on 7/21/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterCustomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *addLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

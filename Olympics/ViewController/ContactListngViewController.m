//
//  ContactListngViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "ContactListngViewController.h"


#define USE_MG_DELEGATE 1
@interface ContactListngViewController ()
{
    NSMutableArray *swipeCellButtons;
    NSMutableArray *arrayContactlist;
    NSDictionary *contactDetails;
    UITableViewCellAccessoryType accessory;
    NSArray *phonetypeToshowArray;
    NSIndexPath *currentIndexPathToDelete;
    
    
}

@end

@implementation ContactListngViewController
@synthesize tableviewContactlist;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue901"];
    
    // Do any additional setup after loading the view from its nib.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.isNeedToRefresh=FALSE;
    [self callServiceToGetContactlist];
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
        [self callServiceToGetContactlist];
    }
    
}

#pragma - mark WebService Call
-(void)callServiceToGetContactlist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetContactDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)callServiceDeleteContact {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"PhoneNumberID" : [contactDetails valueForKey:@"PhoneNumberID"]
                                 };
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteContactDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Delete;
    
}

-(void)callServiceEditContact {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"PhoneNumberID" : [contactDetails valueForKey:@"PhoneNumberID"]
                                 };
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_EditContactDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Edit;
    
}


#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    if (_serviceCalled == Service_Called_Delete) {
        
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            
            [arrayContactlist removeObjectAtIndex:currentIndexPathToDelete.row];
            
            [tableviewContactlist beginUpdates];
            [tableviewContactlist deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [tableviewContactlist endUpdates];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if (_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstGridPhone"] isKindOfClass:[NSNull class]])
            {
                arrayContactlist = [[responseDictionery objectForKey:@"lstGridPhone"] mutableCopy];
                
                if([arrayContactlist count] > 0) {
                    [tableviewContactlist reloadData];
                }
            }
            phonetypeToshowArray = [responseDictionery objectForKey:@"lstddlPhoneType"];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_Edit)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            
            AddContactViewController *contact = [[AddContactViewController alloc]initWithNibName:@"AddContactViewController" bundle:nil];
            contact.isLogedInUsersDetail = _isLogedInUsersDetail;
            contact.playerDetails = _playerDetails;
            //Show, User Wants to add details or edit details.
            contact.serviceCalled = Service_Called_Edit;
            contact.phoneTypeArray = [[NSArray alloc]initWithArray:phonetypeToshowArray];
            contact.contactDetails = responseDictionery;
            contact.owner=self;
            [self.navigationController pushViewController:contact animated:YES];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/**************************** TABLEVIEW Functions ****************************************************/
/****************************************************************************************************/
#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrayContactlist.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrayContactlist count]) {
        
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewContactlist dequeueReusableCellWithIdentifier:itemCell];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        CGFloat cellHeight = 30.0f;
        NSDictionary *dictionary = [arrayContactlist objectAtIndex:indexPath.row];
        NSString *descriptionString = [[dictionary valueForKey:@"PhoneNumberWithCode"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"PhoneType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cellHeight = titleHeight + descriptionHeight + cellHeight;
        return cellHeight;
        
    }
    else{
        if(IS_IPAD){
            return 84;
        }
        else{
            return 60;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrayContactlist count])
    {
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewContactlist dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        NSDictionary *dictionary = [arrayContactlist objectAtIndex:indexPath.row];
        
        NSString *descriptionString = [[dictionary valueForKey:@"PhoneNumberWithCode"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"PhoneType"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cell.titleHeightConstraint.constant = titleHeight;
        cell.descriptionHeightConstraint.constant = descriptionHeight;
        
        bool IsPrimary =  [[dictionary valueForKey:@"IsPrimary"] boolValue];
        cell.lblLock.text = @"\ue903";
        
        cell.lblAddressName.text = titleString;
        cell.lblDisplayAddress.text = descriptionString;
        
        
        if (IsPrimary == true) {
            cell.lblLock.alpha = 0.7;
            cell.lblLock.hidden  = FALSE;
        } else {
            cell.lblLock.alpha = 0.5;
            cell.lblLock.hidden  = TRUE;
        }
        
        cell.delegate = self;
        return cell;
    }
    else
    {
        static NSString *itemCell = @"FooterCustomCell";
        FooterCustomCell *cell = (FooterCustomCell *)[self.tableviewContactlist dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FooterCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.titleLabel.text = @"Add Contacts";
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [arrayContactlist count]){
        
        AddContactViewController *contact = [[AddContactViewController alloc]initWithNibName:@"AddContactViewController" bundle:nil];
        contact.isLogedInUsersDetail = _isLogedInUsersDetail;
        contact.playerDetails = _playerDetails;
        //Show, User Wants to add details or edit details.
        contact.serviceCalled = Service_Called_Add;
        contact.phoneTypeArray = [[NSArray alloc]initWithArray:phonetypeToshowArray];
        contact.owner=self;
        [self.navigationController pushViewController:contact animated:YES];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

//*************************************************************//
-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray *result = [NSMutableArray array];
    NSString* titles[2] = {@"DELETE", @"EDIT"};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *edit = [UIImage imageNamed:@"edit"];
    UIImage *icons[2] = {delete,edit};
    
    UIColor *colors[2] = {[UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1], [UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1]};
    
    for (int i = 0; i < 2; ++i)
    {
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray *) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
              swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        swipeSettings.onlySwipeButtons = YES;
        return [self createRightButtons:data.rightButtonsCount];
    }
}
#endif


-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    
    NSIndexPath *indexPath = [self.tableviewContactlist indexPathForCell:cell];
    //NSLog(@"%@",indexPath);
    contactDetails = [arrayContactlist objectAtIndex:indexPath.row];
    
    if (index == 0) {
        
        // Show popup.
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete Your Contact Details?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         //Is User Wants to delete contact Details Then Continiue.
                                                                         //NSLog(@"Delete");
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDeleteContact];
                                                                         
                                                                     }];
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // code to be written for version lower than ios 8.0...
        }
        //------------------------------------------------------------------------
    } else {
        [self callServiceEditContact];
    }
    
    return YES;
}


@end

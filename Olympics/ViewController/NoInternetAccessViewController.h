//
//  NoInternetAccessViewController.h
//  Olympics
//
//  Created by webwerks on 12/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoInternetAccessViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;

- (IBAction)DidSelectRetry:(id)sender;

@end

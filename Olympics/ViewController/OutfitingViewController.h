//
//  OutfitingViewController.h
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "outfittingCustomCell.h"

// custom controllers Imports
#import "Question_Object.h"
#import "OLMDropDown.h"
#import "Service.h"

@interface OutfitingViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MGSwipeTableCellDelegate, Service_delegate, UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UIWebViewDelegate>
{
    UIPickerView *sizePickerView;
    NSIndexPath *selectedIndexPath;
    UITextField *sizeSelectionTextField;
    NSString *quickGuidUrl;
}

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (nonatomic, assign) Service_Called serviceCalled;
@property (nonatomic, assign) BOOL isNeedToRefresh;
@property (strong, nonatomic) IBOutlet UITableView *tableviewOutfiting;
@property (weak, nonatomic) IBOutlet UIWebView *headerWebView;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webViewSuperViewHeightConstraint;
@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectQuickGuidLink:(id)sender;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;


@end

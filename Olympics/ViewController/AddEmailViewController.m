//
//  AddEmailViewController.m
//  Olympics
//
//  Created by webwerks2 on 7/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AddEmailViewController.h"
#import "EmailListingViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
@interface AddEmailViewController ()
{
    CGFloat animatedDistance;
    NSString *previousText,*currentText;
}

@end

@implementation AddEmailViewController
@synthesize incrementedHeight, controlls ,emailTypeArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _titleLabel.font = app_font_bold_15;
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue918"];
    self.isChangesMade = FALSE;
    isFirstTime = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if(isFirstTime)
    {
        [self StartAddingControllers];
        isFirstTime = false;
    }
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if ([self.owner class]==[EmailListingViewController class]) {
        [(EmailListingViewController *)self.owner setIsNeedToRefresh:FALSE];
    }
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                if([answersDict valueForKey:@"Email"])
                {
                    NSString *email = [answersDict valueForKey:@"Email"];
                    if(![COMMON_SETTINGS validateEmailWithString:email])
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Invalid Email Address" CancelButtonTitle:@"Ok" InView:self];
                        [requestParameters removeAllObjects];
                        return;
                    }
                }
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

#pragma - mark WebService Call
-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    
    if(_serviceCalled == Service_Called_Edit){
        NSString *valueEmailID = [_emailDetails valueForKey:@"EmailId"];
        [userinformation setValue:valueEmailID forKey:@"EmailID"];
    }
    else if (_serviceCalled == Service_Called_Add){
        
        [userinformation setValue:@"0" forKey:@"EmailID"];
        
    }
    else{
        //NSLog(@"Not Valid type found!!");
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateEmailDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
    {
        
        if ([self.owner class]==[EmailListingViewController class]) {
            [(EmailListingViewController *)self.owner setIsNeedToRefresh:TRUE];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers {
    
    //Field_DropdownList.
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    
    if(![emailTypeArray isKindOfClass:[NSNull class]] && emailTypeArray && [emailTypeArray count]>0)
    {
        NSInteger initialIndex = 0;
        if(_serviceCalled == Service_Called_Edit){
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"Key == %@",_emailDetails[@"EmailTypeId"]];
            
            NSArray *filteredValues = [emailTypeArray filteredArrayUsingPredicate:filterPredicate];
            
            if(filteredValues.count){
                initialIndex = [[filteredValues lastObject][@"Key"]integerValue];
            }
        }
        
        OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Type" fieldIdName:@"EmailTypeId" questionId:101 Options:emailTypeArray IsRequired:YES defaultValue:initialIndex delegate:self lineColor:color_lightGray];
        controlObj.questionTypeId = Field_DropdownList;
        controlObj.tag = 0;
        [controlls addObject:controlObj];
        [self.baseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    // Field_TextField.
    OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Email" fieldIdName:@"Email" questionId:102 IsRequired:YES defaultValue: (_serviceCalled == Service_Called_Edit) ?(_emailDetails[@"Email"]?_emailDetails[@"Email"]:@""):@"" keyboardType:UIKeyboardTypeEmailAddress  delegate:self lineColor:color_lightGray];
    controlObj.questionTypeId = Field_TextField;
    controlObj.tag = 1;
    [controlls addObject:controlObj];
    [self.baseScrollView addSubview:controlObj];
    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    
    // Chech_Box_Field.
    OLMSingleCheckBox *checkBoxOneObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:103 fieldIdName:@"IsPrimary" OptionTitle:@"Primary" isRequired:YES defaultSelected:(_serviceCalled == Service_Called_Edit) ?[_emailDetails[@"IsPrimary"] boolValue]:NO delegate:self lineColor:color_lightGray];
    checkBoxOneObj.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj.tag = 2;
    [controlls addObject:checkBoxOneObj];
    [self.baseScrollView addSubview:checkBoxOneObj];
    incrementedHeight = incrementedHeight + checkBoxOneObj.frame.size.height + 5;
    
    self.isChangesMade = FALSE;
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMSingleCheckBox:(OLMSingleCheckBox *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}
#pragma mark - OLMTextView Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end

//
//  addAddressListViewController.h
//  Olympics
//
//  Created by webwerks2 on 7/14/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMSingleCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "Service.h"

@interface AddAddressListViewController : UIViewController<Service_delegate>

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (nonatomic, retain) NSArray *fieldsToShowArray;
@property (nonatomic, strong) NSArray *typesArray;

@property (nonatomic, strong) NSArray *statesArray;
@property (nonatomic, strong) NSArray *countriesArray;
@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *vanigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogi;
@property(nonatomic,assign) Service_Called serviceCalled;
@property(nonatomic, strong) NSDictionary *addressDetails;
@property (nonatomic, strong) UIViewController *owner;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign, nonatomic) BOOL isFirstTime;
@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end

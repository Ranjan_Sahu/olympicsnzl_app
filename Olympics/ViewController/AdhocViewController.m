//
//  DynamicFormViewController.m
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AdhocViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 320;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface AdhocViewController ()
{
    BOOL isSectionSelected, isSectionExpanded;
    NSUInteger currSection, prvSection;
    AdhocCustomCell *cell;
    NSMutableArray *arrayToShowRowData, *fieldsToShowArray;
    CGFloat rowHeight,headerHeight,headertitlelblLeading,subtractedWidth;
    NSString *previousText,*currentText;
}
@end

@implementation AdhocViewController
@synthesize incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    animatedDistance = 0;
    
    _navigatiobBarLogi.text = [NSString stringWithFormat:@"\ue924"];
    _contentTableView.delegate = self;
    _contentTableView.dataSource = self;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.isChangesMade = FALSE;
    
    headerHeight = 40.0f;
    headertitlelblLeading = 15.0f;
    subtractedWidth = 25.0f;
    if(IS_IPAD)
    {
        headerHeight = 80.0f;
        headertitlelblLeading = 50.0f;
        subtractedWidth = 40.0f;
    }
    
    isSectionSelected = FALSE;
    isSectionExpanded = FALSE;
    
    [_contentTableView registerNib:[UINib nibWithNibName:@"AdhocCustomCell" bundle:nil] forCellReuseIdentifier: @"AdhocCustomCell"];
    
    [_contentTableView registerNib:[UINib nibWithNibName:@"AdhocDummyCell" bundle:nil] forCellReuseIdentifier: @"AdhocDummyCell"];
    
    // DO OtherProcessing
    [self CallServiceTogetSubMenues];
}

- (IBAction)DidSelectBackButton:(id)sender {
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    [self.view endEditing:YES];
    
    for (int i=0; i<controlls.count; i++) {
        
        NSMutableArray *dictMainAndSubControlls = [[NSMutableArray alloc]initWithArray:[[self.controlls objectAtIndex:i] objectForKey:@"Controls"]];

        for (int j = 0; j<dictMainAndSubControlls.count; j++) {
            NSMutableDictionary *subDictMainAndSubControlls = [dictMainAndSubControlls objectAtIndex:j];
            
            if(![[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
            {
                if([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMRadioButton class]])
                {
                    OLMRadioButton *RadioButtonObj  = (OLMRadioButton*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [RadioButtonObj GetAnswerInfo];
                    if(!answersDict && RadioButtonObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
                else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMDropDown class]]))
                {
                    OLMDropDown *DropdownObj  = (OLMDropDown*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
                    if(!answersDict && DropdownObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
                else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMCheckBox class]]))
                {
                    OLMCheckBox *CheckBoxObj  = (OLMCheckBox*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
                    if(!answersDict && CheckBoxObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
                else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMTextField class]]))
                {
                    OLMTextField *TextFieldObj  = (OLMTextField*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
                    if(!answersDict && TextFieldObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
                else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMTextView class]]))
                {
                    OLMTextView *TextViewObj  = (OLMTextView*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [TextViewObj GetAnswerInfo];
                    if(!answersDict && TextViewObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
                else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMRadioButtonTextKeyType class]]))
                {
                    OLMRadioButtonTextKeyType *CheckBoxObj  = (OLMRadioButtonTextKeyType*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
                    if(!answersDict && CheckBoxObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
                else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMDatePicker class]]))
                {
                    OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
                    if(!answersDict && DatePickerObj.IsRequired == 1)
                    {
                        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                        return;
                    }
                }
            }
        }
    }
    
    [self  CallServiceToSaveDataWithArray:fieldsToShowArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews
{
    _contentTableViewTop.constant = -animatedDistance;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
}

#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if(_isLogedInUsersDetail)
    {
        [parameters setValue:@false forKey:@"IsMyTeamMember"];
    }
    else
    {
        [parameters setValue:@true forKey:@"IsMyTeamMember"];
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_AdhocQuestionAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithArray:(NSMutableArray*)recordsArray
{
    [COMMON_SETTINGS showHUD];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:recordsArray forKey:@"lstQuestionAnswer"];
    
    if(![[parameters objectForKey:@"lstQuestionAnswer"]isKindOfClass:[NSNull class]] && [[parameters objectForKey:@"lstQuestionAnswer"]isKindOfClass:[NSArray class]] && [[parameters objectForKey:@"lstQuestionAnswer"] count]>0)
    {
        Service *callService = [[Service alloc] init];
        callService.delegate = self;
        [callService requestingURLString:weburl Service:Service_InsertUpdateAdhocQuestionDetails withParameters:parameters ContactID:[_playerDetails valueForKey:@"ContactID"]];
        _serviceCalled = Service_Called_Save;
    }
    else
    {
        [COMMON_SETTINGS hideHUD];
        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"No records to save" CancelButtonTitle:@"OK" InView:self];
    }
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstQuestionAnswer"] isKindOfClass:[NSNull class]])
            {
                fieldsToShowArray = [[NSMutableArray alloc]initWithArray:[responseDictionery objectForKey:@"lstQuestionAnswer"]];
                
                if([fieldsToShowArray count] > 0){
                    [self StartAddingControllers];
                    [_contentTableView reloadData];
                    [self.view bringSubviewToFront:self.vanigationBarView];
                }
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMRadioButton:(OLMRadioButton *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade =TRUE;
    
    CGPoint switchPositionPoint = [radioButton convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    if(![[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]isKindOfClass:[NSNull class]] && [[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"] count]>0)
    {
        NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
        NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
        
        if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
        {
            NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
            
            if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
            {
                if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMRadioButton class]])
                {
                    NSDictionary *answersDict = [radioButton GetAnswerInfo];
                    if(answersDict)
                    {
                        NSString *value;
                        for (id key in answersDict)
                            value = [answersDict objectForKey:key];
                        [rowDataDict setValue:value forKey:@"AHAYesNo"];
                    }
                }
            }
        }
        [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
        [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
        [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];
    }
}

#pragma mark - OLMCheckBox Delegate
-(void)OLMCheckBox:(OLMCheckBox *)checkBox didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade =TRUE;
    
    [self scrollDown];
    CGPoint switchPositionPoint = [checkBox convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
    
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
        
        if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMCheckBox class]])
            {
                OLMCheckBox *MultiCheckBoxObj  = (OLMCheckBox*)[dictMainAndSubControlls objectForKey:@"mainControler"];
                NSDictionary *answersDict = [MultiCheckBoxObj GetAnswerInfo];
                if(answersDict)
                {
                    if (MultiCheckBoxObj.selectedOptionIdArray.count >0) {
                        NSString *selectedId = [[MultiCheckBoxObj.selectedOptionIdArray valueForKey:@"AHOptionID"] componentsJoinedByString:@","];
                        [rowDataDict setValue:selectedId forKey:@"MultiSelectedOptions"];
                    }
                    else{
                        [rowDataDict setValue:@" " forKey:@"MultiSelectedOptions"];
                    }
                }
            }
        }
    }
    [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
    [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];

}

#pragma mark - OLMSingleCheckBox Delegate
-(void)OLMSingleCheckBox:(OLMSingleCheckBox*)singleCheckBox didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade =TRUE;
    
    [self scrollDown];
    CGPoint switchPositionPoint = [singleCheckBox convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
    
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
        
        if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMSingleCheckBox class]])
            {
                OLMSingleCheckBox *DatePickerObj  = (OLMSingleCheckBox*)[dictMainAndSubControlls objectForKey:@"mainControler"];
                NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
                if(answersDict)
                {
                    NSString *value;
                    for (id key in answersDict)
                        value = [answersDict objectForKey:key];
                    
                    [rowDataDict setValue:value forKey:@"IsCompleted"];
                }
            }
        }
        
        if(![[dictMainAndSubControlls objectForKey:@"subControlers"] isKindOfClass:[NSNull class]]&&([[dictMainAndSubControlls objectForKey:@"subControlers"] count]>0))
        {
            for(int i=0;i<[[dictMainAndSubControlls objectForKey:@"subControlers"]count];i++)
            {
                if([[[dictMainAndSubControlls objectForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
                {
                    OLMSingleCheckBox *DatePickerObj  = (OLMSingleCheckBox*)[[dictMainAndSubControlls objectForKey:@"subControlers"] objectAtIndex:i];
                    NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
                    if(answersDict)
                    {
                        NSString *value;
                        for (id key in answersDict)
                            value = [answersDict objectForKey:key];
                        
                        [rowDataDict setValue:value forKey:@"IsCompleted"];
                    }
                    break;
                }
            }
        }
    }
    [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
    [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade =TRUE;
    
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }

    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
    
    NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
    
    if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
    {
        if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[dictMainAndSubControlls objectForKey:@"mainControler"];

            NSString *value = textField.text;
            if(TextFieldObj.questionTypeId == AHFormatNumeric)
                [rowDataDict setValue:value forKey:@"AHAValue"];
            else
                [rowDataDict setValue:value forKey:@"Answer"];
        }
    }
    
    if(![[dictMainAndSubControlls objectForKey:@"subControlers"] isKindOfClass:[NSNull class]]&&([[dictMainAndSubControlls objectForKey:@"subControlers"] count]>0))
    {
        for(int i=0;i<[[dictMainAndSubControlls objectForKey:@"subControlers"]count];i++)
        {
            if([[[dictMainAndSubControlls objectForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMTextField class]])
            {
                NSString *value = textField.text;;
                [rowDataDict setValue:value forKey:@"Note"];
                //break;
            }
        }
    }
    [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
    [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextViewDidBeginEditing:(UITextView *)textView
{
    previousText = textView.text;
    [self scrollUp:textView];
    self.isChangesMade =TRUE;
    
}
-(void)OLMTextViewDidEndEditing:(UITextView*)textView
{
    currentText = textView.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
    
    CGPoint switchPositionPoint = [textView convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
    
    NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
    
    if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
    {
        if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMTextView class]])
        {
            NSString *value = textView.text;
            [rowDataDict setValue:value forKey:@"Answer"];
        }
    }
    [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
    [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade =TRUE;
    
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
    
    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
    
    NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
    
    if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
    {
        if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropDownObj  = (OLMDropDown*)[dictMainAndSubControlls objectForKey:@"mainControler"];
            NSDictionary *answersDict = [DropDownObj GetAnswerInfo];
            if(answersDict)
            {
                NSString *value;
                for (id key in answersDict)
                    value = [answersDict objectForKey:key];
                
                [rowDataDict setValue:value forKey:@"AHOptionId"];
            }
            else
            {
                [rowDataDict setValue:@"0" forKey:@"AHOptionId"];
            }
        }
    }
    [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
    [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];
}

-(void)OLMRadioButtonTextKeyType:(OLMRadioButtonTextKeyType*)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}

#pragma mark - OLMDatePicker Delegate
-(void)OLMDatepickerDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade =TRUE;
}

-(void)OLMDatepickerDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
    
    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:[self contentTableView]];
    NSIndexPath *indexPath = [[self contentTableView] indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableDictionary *lstGroupQuestionAnswer = [[NSMutableDictionary alloc] initWithDictionary:[fieldsToShowArray objectAtIndex:indexPath.section]];
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[lstGroupQuestionAnswer valueForKey:@"lstGroupQuestionAnswer"]];
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.row]];
    
    NSMutableDictionary *dictMainAndSubControlls = [[NSMutableDictionary alloc]initWithDictionary:[[[self.controlls objectAtIndex:currSection] objectForKey:@"Controls"] objectAtIndex:indexPath.row]];
    
    if(![[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
    {
        if([[dictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMDatePicker class]])
        {
            [rowDataDict setValue:textField.text forKey:@"AHADate"];
        }
    }
    
    if(![[dictMainAndSubControlls objectForKey:@"subControlers"] isKindOfClass:[NSNull class]]&&([[dictMainAndSubControlls objectForKey:@"subControlers"] count]>0))
    {
        for(int i=0;i<[[dictMainAndSubControlls objectForKey:@"subControlers"]count];i++)
        {
            if([[[dictMainAndSubControlls objectForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
            {
                [rowDataDict setValue:textField.text forKey:@"AHADate"];
                break;
            }
        }
    }
    [arrayToReplace replaceObjectAtIndex:indexPath.row withObject:rowDataDict];
    [lstGroupQuestionAnswer setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    [fieldsToShowArray replaceObjectAtIndex:indexPath.section withObject:lstGroupQuestionAnswer];
}

#pragma mark - TableViewDataSource & TableViewDelegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([controlls count]>0)
        return [controlls count];
    else
        return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows = 1;
    if(isSectionExpanded == TRUE && currSection==section)
    {
        NSMutableDictionary *dict = [controlls objectAtIndex:section];
        noOfRows = [[dict objectForKey:@"Controls"]count];
    }
    if(noOfRows>0)
        return noOfRows;
    else
        return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    height = 5;
    
    if(isSectionExpanded == TRUE && currSection==indexPath.section)
    {
        if(controlls.count>0)
        {
            height = [self GetRowHeight: indexPath];
            height = height + 10;
        }
    }
    return height;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return headerHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isSectionExpanded == TRUE && isSectionSelected && currSection==indexPath.section){
        
        cell = [_contentTableView dequeueReusableCellWithIdentifier:@"AdhocCustomCell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:currSection]];
        
        for (UIView *subview in [cell.contentView subviews])
            [subview removeFromSuperview];
        
        if(fieldsToShowArray.count>0)
        {
            NSMutableArray *ControlObjects = [[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"];
            if(![ControlObjects isKindOfClass:[NSNull class]]&&ControlObjects.count>0)
            {
                NSMutableDictionary *dictMainAndSubObjects = [[NSMutableDictionary alloc] init];
                dictMainAndSubObjects = [ControlObjects objectAtIndex:indexPath.row];
                
                [cell.contentView addSubview:[dictMainAndSubObjects objectForKey:@"mainControler"]];
                
                if(![[dictMainAndSubObjects valueForKey:@"subControlers"]isKindOfClass:[NSNull class]]&&[[dictMainAndSubObjects valueForKey:@"subControlers"] count]>0)
                {
                    for(int i=0;i<[[dictMainAndSubObjects valueForKey:@"subControlers"] count];i++)
                    {
                        if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMTextField class]])
                        {
                            OLMTextField *TextFieldObj  = (OLMTextField*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                            
                            [cell.contentView addSubview:TextFieldObj];
                        }
                        else  if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
                        {
                            OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                            [cell.contentView addSubview:DatePickerObj];
                        }
                        else if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
                        {
                            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                            [cell.contentView addSubview:CheckBoxObj];
                        }
                    }
                }
            }
        }
    }
    else
    {
        cell = [_contentTableView dequeueReusableCellWithIdentifier:@"AdhocDummyCell" forIndexPath:[NSIndexPath indexPathForRow:0 inSection:currSection]];
    }
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdhocDummyCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *viewForHeader = [[UIView alloc] initWithFrame:CGRectMake(_contentTableView.frame.origin.x, _contentTableView.frame.origin.y, _contentTableView.frame.size.width, headerHeight)];
    viewForHeader.tag = 200 + section;
    viewForHeader.backgroundColor =  [UIColor colorWithRed:0.8980 green:0.8980 blue:0.8980 alpha:1.0];
    
    UILabel *lblHeaderViewTitle = [[UILabel alloc] initWithFrame:CGRectMake(headertitlelblLeading, 0, (viewForHeader.frame.size.width - 70), viewForHeader.frame.size.height)];
    [lblHeaderViewTitle setTextAlignment:NSTextAlignmentLeft];
    [lblHeaderViewTitle setTextColor:[UIColor colorWithRed:0.9765 green:0.2627 blue:0.3294 alpha:1.0]];
    
    UILabel *lblHeaderViewArrowIndicator = [[UILabel alloc] initWithFrame:CGRectMake((viewForHeader.frame.size.width - subtractedWidth - headertitlelblLeading), 0, 30, viewForHeader.frame.size.height)];
    [lblHeaderViewArrowIndicator setTextAlignment:NSTextAlignmentCenter];
    [lblHeaderViewArrowIndicator setTextColor:[UIColor colorWithRed:0.9765 green:0.2627 blue:0.3294 alpha:1.0]];
    
    if(IS_IPAD)
    {
        [lblHeaderViewTitle setFont:app_font32];
        [lblHeaderViewArrowIndicator setFont:[UIFont fontWithName:@"icomoon" size:23]];
    }
    else if (IS_IPHONE)
    {
        [lblHeaderViewTitle setFont:app_font18];
        [lblHeaderViewArrowIndicator setFont:[UIFont fontWithName:@"icomoon" size:13]];
    }
    
    NSString *titleString;
    if(fieldsToShowArray.count>0)
    {
        NSMutableDictionary *dict = [fieldsToShowArray objectAtIndex:section];
        titleString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"GroupHeader"]];
        [lblHeaderViewTitle setText:titleString];
    }
    
    if(currSection == section && isSectionExpanded)
    {
        [lblHeaderViewArrowIndicator setText:@"\ue921"];
    }
    else
    {
        [lblHeaderViewArrowIndicator setText:@"\ue92c"];
    }
    
    [viewForHeader addSubview:lblHeaderViewTitle];
    [viewForHeader addSubview:lblHeaderViewArrowIndicator];
    
    UITapGestureRecognizer *tapOnView = [[UITapGestureRecognizer alloc]
                                         initWithTarget:self
                                         action:@selector(DidSelectHeader:) ];
    [viewForHeader addGestureRecognizer:tapOnView];
    
    return viewForHeader;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Implementation For Tap Gesture Method
-(void)DidSelectHeader:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
    //NSLog(@"selected Section is : %ld",(sender.view.tag - 200) );
    
    currSection = (sender.view.tag - 200);
    NSRange range = NSMakeRange(currSection, 1);
    NSIndexSet *selectedIndexSet = [NSIndexSet indexSetWithIndexesInRange:range];
    
    if(isSectionExpanded == FALSE){
        
        prvSection = currSection;
        isSectionExpanded = TRUE;
        isSectionSelected = TRUE;
        [_contentTableView reloadSections:selectedIndexSet withRowAnimation:UITableViewRowAnimationFade];
    }
    else if(isSectionExpanded == TRUE){
        
        if(prvSection == currSection){
            isSectionExpanded =FALSE;
            isSectionSelected = FALSE;
            [_contentTableView reloadSections:selectedIndexSet withRowAnimation:UITableViewRowAnimationFade];
            
        }
        else if(currSection != prvSection){
            
            isSectionExpanded = FALSE;
            isSectionSelected = FALSE;
            [_contentTableView reloadSections:[NSIndexSet indexSetWithIndex:prvSection] withRowAnimation:UITableViewRowAnimationFade];
            prvSection = currSection;
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                isSectionExpanded = TRUE;
                isSectionSelected = TRUE;
                [_contentTableView reloadSections:selectedIndexSet withRowAnimation:UITableViewRowAnimationFade];
            });
        }
    }
}

#pragma mark - Method to add questions on screen
-(void)StartAddingControllers
{
    CGRect frame = self.contentTableView.frame;
    int yCord = 10;
    if(![fieldsToShowArray isKindOfClass:[NSNull class]] && [fieldsToShowArray count] > 0)
    {
        controlls = [[NSMutableArray alloc]init];
        
        for(int sec=0;sec<fieldsToShowArray.count;sec++)
        {
            NSMutableArray *arrayGrpQueAns = [[fieldsToShowArray objectAtIndex:sec] objectForKey:@"lstGroupQuestionAnswer"];
            NSMutableDictionary *dictControlls = [[NSMutableDictionary alloc]init];
            NSMutableArray *arrayControlls = [[NSMutableArray alloc] init];
            
            if(![arrayGrpQueAns isKindOfClass:[NSNull class]] && arrayGrpQueAns.count>0)
            {
                for(int row=0;row<arrayGrpQueAns.count;row++)
                {
                    {
                        NSDictionary *QDictionery = [arrayGrpQueAns objectAtIndex:row];
                        int controlTypeId = [[QDictionery valueForKey:@"QuestionFormatId"] intValue];
                        
                        switch (controlTypeId){
                            case AHFormatText:
                            {
                                NSUInteger YIndex = 0;
                                //Field_TextField
                                OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"Answer"]isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"Answer"] keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
                                
                                controlObj.questionTypeId = AHFormatText;
                                controlObj.tag = row+1;
                                controlObj.isQuestionNeeded = TRUE;
                                //[arrayControlls addObject:controlObj];
                                
                                NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                                
                                YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                                
                                if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                {
                                    [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                }
                                [arrayControlls addObject:controllerDictionary];
                            }
                                break;
                            case AHFormatOption:
                            {
                                //Field_DropdownList
                                NSArray *optionsArr = [QDictionery valueForKey:@"ddlAnswerOption"];
                                if(![optionsArr isKindOfClass:[NSNull class]] && optionsArr && [optionsArr count]>0)
                                {
                                    NSString *iStr = [QDictionery valueForKey:@"AHOptionId"];
                                    int answerInt = 0;
                                    if(![iStr isKindOfClass:[NSNull class]])
                                    {
                                        answerInt = [iStr intValue];
                                    }
                                    
                                    NSUInteger YIndex = 0;
                                    
                                    OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] Options:optionsArr IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:answerInt delegate:self lineColor:color_lightGray];
                                    controlObj.questionTypeId = AHFormatOption;
                                    controlObj.tag = row+1;
                                    //[arrayControlls addObject:controlObj];
                                    
                                    NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                    [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                                    
                                    YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                                    
                                    if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                    {
                                        [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                    }
                                    [arrayControlls addObject:controllerDictionary];
                                }
                                else
                                {
                                    //NSLog(@"Option Array Not Found");
                                }
                            }
                                break;
                            case AHFormatDate:
                            {
                                NSUInteger YIndex = 0;
                                //Field_DatePicker
                                OLMDatePicker *controlObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]   fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"AHADate"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"AHADate"] delegate:self lineColor:color_lightGray];
                                controlObj.questionTypeId = AHFormatDate;
                                controlObj.tag = row+1;
                                //[arrayControlls addObject:controlObj];
                                
                                NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                                
                                YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                                
                                if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                {
                                    [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                }
                                [arrayControlls addObject:controllerDictionary];
                            }
                                break;
                            case AHFormatNumeric:
                            {
                                NSUInteger YIndex = 0;
                                //Field_NumericTextField
                                OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"AHAValue"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"AHAValue"] keyboardType:UIKeyboardTypeDecimalPad  delegate:self lineColor:color_lightGray];
                                
                                controlObj.questionTypeId = AHFormatNumeric;
                                controlObj.tag = row+1;
                                //[arrayControlls addObject:controlObj];
                                
                                NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                                
                                YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                                
                                if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                {
                                    [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                }
                                [arrayControlls addObject:controllerDictionary];
                            }
                                break;
                            case AHFormatYesNo:
                            {
                                //OLM CheckBox
                                NSArray *optionArray = [QDictionery valueForKey:@"ddlAnswerOption"];
                                if(![optionArray isKindOfClass:[NSNull class]] && optionArray && [optionArray count]>0)
                                {
                                    NSString *answer = [QDictionery valueForKey:@"AHAYesNo"];
                                    BOOL answerbool = FALSE;
                                    if(![answer isKindOfClass:[NSNull class]])
                                    {
                                        answerbool = [answer boolValue];
                                    }
                                    
                                    NSUInteger YIndex = 0;
                                    
                                    OLMRadioButton *radioButtonObj = [[OLMRadioButton alloc] initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"] fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] options:optionArray IsRequired:NO defaultValue:answerbool delegate:self lineColor:color_lightGray];
                                    radioButtonObj.questionTypeId = AHFormatYesNo;
                                    radioButtonObj.tag = row+1;
                                    //[arrayControlls addObject:radioButtonObj];
                                    
                                    NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                    [controllerDictionary setObject:radioButtonObj forKey:@"mainControler"];
                                    
                                    YIndex = radioButtonObj.frame.origin.y + radioButtonObj.frame.size.height;
                                    
                                    if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                    {
                                        [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                    }
                                    [arrayControlls addObject:controllerDictionary];
                                }
                            }
                                break;
                            case AHFormatTextMultiline:
                            {
                                //Field_TextField
                                NSUInteger YIndex = 0;
                                
                                OLMTextView *controlObj = [[OLMTextView alloc] initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"Answer"]isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"Answer"] keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
                                
                                controlObj.questionTypeId = AHFormatTextMultiline;
                                controlObj.tag = row+1;
                                //[arrayControlls addObject:controlObj];
                                
                                NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                                
                                YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                                
                                if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                {
                                    [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                }
                                [arrayControlls addObject:controllerDictionary];
                            }
                                break;
                            case AHFormatMulti:
                            {
                                //Field_MultipleCheckBox
                                NSArray *optionArray = [[NSArray alloc]init];
                                
                                if ([[QDictionery valueForKey:@"IsMultiSelected"] boolValue] == TRUE) {
                                    optionArray = [QDictionery valueForKey:@"lstMultiChoiceOption"];
                                }
                                else {
                                    optionArray = [QDictionery valueForKey:@"ddlAnswerOption"];
                                }
                                
                                if(![optionArray isKindOfClass:[NSNull class]] && optionArray && [optionArray count]>0)
                                {
                                    NSString *iStr = [QDictionery valueForKey:@"Answer"];
                                    int answerInt = 0;
                                    if(![iStr isKindOfClass:[NSNull class]])
                                    {
                                        answerInt = [iStr intValue];
                                    }
                                    
                                    NSUInteger YIndex = 0;
                                    
                                    OLMCheckBox *controlObj = [[OLMCheckBox alloc]initWithFrame:CGRectMake(10, yCord, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"QuestionHeader"] fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] options:optionArray IsRequired:NO defaultValue:answerInt delegate:self lineColor:color_lightGray];
                                    
                                    controlObj.questionTypeId = AHFormatMulti;
                                    controlObj.tag = row+1;
                                    //[arrayControlls addObject:controlObj];
                                    
                                    NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                                    [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                                    
                                    YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                                    
                                    if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                                    {
                                        [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:frame] forKey:@"subControlers"];
                                    }
                                    [arrayControlls addObject:controllerDictionary];
                                }
                            }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            [dictControlls setValue:[NSString stringWithFormat:@"%d",sec] forKey:@"SectionId"];
            [dictControlls setObject:arrayControlls forKey:@"Controls"];
            [controlls addObject:dictControlls];
        }
    }
    self.isChangesMade =FALSE;
}

#pragma mark - Method To Calculate Dynamic Height of Row
-(CGFloat)GetRowHeight: (NSIndexPath *)currentIndex
{
    NSMutableArray *ControlObjects = [[controlls objectAtIndex:currentIndex.section] objectForKey:@"Controls"];
    
    if([ControlObjects count]>0)
    {
        NSMutableDictionary *dictMainAndSubObjects = [[NSMutableDictionary alloc] init];
        
        dictMainAndSubObjects = [ControlObjects objectAtIndex:currentIndex.row];
        
        if(![[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            // Calculate Height for Sub Controllers
            CGFloat height = 0;
            if(![[dictMainAndSubObjects valueForKey:@"subControlers"]isKindOfClass:[NSNull class]]&&[[dictMainAndSubObjects valueForKey:@"subControlers"] count]>0)
            {
                for(int i=0;i<[[dictMainAndSubObjects valueForKey:@"subControlers"] count];i++)
                {
                    if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMTextField class]])
                    {
                        OLMTextField *TextFieldObj  = (OLMTextField*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                        height = height + TextFieldObj.frame.size.height;
                    }
                    else  if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
                    {
                        OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                        height = height + DatePickerObj.frame.size.height;
                    }
                    else if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
                    {
                        OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                        height = height + CheckBoxObj.frame.size.height;
                    }
                }
            }
            
            // Calculate Height for Main Controllers
            if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMRadioButton class]])
            {
                OLMRadioButton *RadioButtonObj = (OLMRadioButton*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + RadioButtonObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMDropDown class]])
            {
                OLMDropDown *DropdownObj = (OLMDropDown*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + DropdownObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMCheckBox class]])
            {
                OLMCheckBox *CheckBoxObj = (OLMCheckBox*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + CheckBoxObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMTextField class]])
            {
                OLMTextField *TextFieldObj = (OLMTextField*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + TextFieldObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMRadioButtonTextKeyType class]])
            {
                OLMRadioButtonTextKeyType *CheckBoxObj = (OLMRadioButtonTextKeyType*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + CheckBoxObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMDatePicker class]])
            {
                OLMDatePicker *DatePickerObj = (OLMDatePicker*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + DatePickerObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMTextView class]])
            {
                OLMTextView *TextViewObj = (OLMTextView*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + TextViewObj.frame.size.height;
            }
            return height;
        }
    }
    return 10.0f;
}

#pragma mark - Add Sub Controllers
-(NSMutableArray* )callMethodToAddSubControllers:(NSDictionary *)QDictionery andYIndex :(NSUInteger)YIndex andFrame :(CGRect)frame
{
    int IncludeNote = [[QDictionery valueForKey:@"IncludeNote"] intValue];
    int IncludeDate = [[QDictionery valueForKey:@"IncludeDate"] intValue];
    int IncludeCompleted = [[QDictionery valueForKey:@"IncludeCompleted"] intValue];
    
    NSMutableArray *subControlles = [[NSMutableArray alloc] init];
    
    if(IncludeNote)
    {
        OLMTextField *subcontrolObj = [[OLMTextField alloc] initWithFrame:CGRectMake(20, YIndex, frame.size.width-40, 1) Question:@"Notes"  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:FALSE defaultValue:[[QDictionery valueForKey:@"Note"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"Note"] keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
        
        subcontrolObj.questionTypeId = AHFormatText;
        subcontrolObj.tag = 777;
        [subControlles addObject:subcontrolObj];
        
        YIndex = YIndex + subcontrolObj.frame.size.height;
    }
    if(IncludeDate)
    {
        OLMDatePicker *subcontrolObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(20, YIndex, frame.size.width-40, 1) Question:@"Date" fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:FALSE defaultValue:[QDictionery valueForKey:@"AHADate"]  delegate:self lineColor:color_lightGray];
        
        subcontrolObj.questionTypeId = AHFormatDate;
        subcontrolObj.tag = 888;
        [subControlles addObject:subcontrolObj];
        
        YIndex = YIndex + subcontrolObj.frame.size.height;
    }
    if(IncludeCompleted)
    {
        OLMSingleCheckBox *subcontrolObj = [[OLMSingleCheckBox alloc]initWithFrame:CGRectMake(20, YIndex, frame.size.width - 40, 1) questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] OptionTitle:@"Completed" isRequired:FALSE defaultSelected:[[QDictionery valueForKey:@"IsCompleted"] boolValue] delegate:self lineColor:color_lightGray];
        
        subcontrolObj.questionTypeId = AHFormatSingleCheckBox;
        subcontrolObj.tag = 999;
        [subControlles addObject:subcontrolObj];
        
        YIndex = YIndex + subcontrolObj.frame.size.height;
    }
    return subControlles;
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.contentTableView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.contentTableView.window convertRect:self.contentTableView.bounds fromView:self.contentTableView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.contentTableView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.contentTableView setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)scrollDown {
    
    CGRect viewFrame = self.contentTableView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.contentTableView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

@end

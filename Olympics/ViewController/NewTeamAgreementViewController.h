//
//  NewTeamAgreementViewController.h
//  Olympics
//
//  Created by webwerks on 3/21/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "agreementsCollectionViewCell.h"
#import "OLMSingleCheckBox.h"
#import "AdhocCustomCell.h"
#import "OLMCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "OLMRadioButton.h"
#import "OLMDatePicker.h"
#import "Service.h"
#import "OLMRadioButtonTextKeyType.h"
#import "OLMSingleCheckBox.h"
#import "OLMTextView.h"

// custom controllers Imports
#import "Service.h"


@interface NewTeamAgreementViewController : UIViewController <Service_delegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (nonatomic, assign) Service_Called serviceCalled;
@property(nonatomic, strong) NSMutableArray *controlls;
@property(nonatomic, strong) NSMutableArray *contentViewControlls;
@property(nonatomic) NSInteger incrementedHeight;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;

@property (weak, nonatomic) IBOutlet UICollectionView *tapItemsCollectionView;
@property (weak, nonatomic) IBOutlet UIView *viewCollection;
@property (weak, nonatomic) IBOutlet UIView *viewButton;
@property (weak, nonatomic) IBOutlet UIButton *proceedBtn;


// Height Constraints
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewScrollContent;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeaderView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAgreementWebView;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewCollection;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewScrollContentViewTop;


- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectProceedButton:(id)sender;

@end

//
//  OutfitingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "OutfitingViewController.h"

#define USE_MG_DELEGATE 1

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 290;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface OutfitingViewController (){
    
    NSMutableArray *swipeCellButtons;
    NSDictionary *eventDetails;
    NSMutableArray *arrayOutfittingList;
    NSArray *eventtypeToShowArray;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
    
    CGFloat animatedDistance;
    BOOL isChangesOK;
}
@end

@implementation OutfitingViewController
@synthesize tableviewOutfiting;

- (void)viewDidLoad {
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue928"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    tableviewOutfiting.tableFooterView = [UIView  new];
    
    tableviewOutfiting.backgroundColor = color_white;
    self.headerWebView.backgroundColor = color_white;
    self.isChangesMade = FALSE;
    
    if(IS_IPAD)
    {
        [tableviewOutfiting registerNib:[UINib nibWithNibName:@"outfittingCustomCellIpad" bundle:nil] forCellReuseIdentifier:@"outfittingCustomCell"];
    }
    else
    {
        [tableviewOutfiting registerNib:[UINib nibWithNibName:@"outfittingCustomCell" bundle:nil] forCellReuseIdentifier:@"outfittingCustomCell"];
    }
    [self callServiceToGetOutfittinglist];
    [self SetupPickerViewForSize];
    // Do any additional setup after loading the view from its nib.
    // Removing Emptycells by using this line of code.
    //self.tableviewEventList.tableFooterView = [UIView new];
    self.isNeedToRefresh=FALSE;
    isChangesOK = FALSE;
}

- (IBAction)DidSelectQuickGuidLink:(id)sender {
    
    if(quickGuidUrl && [quickGuidUrl length] >0)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:quickGuidUrl]];
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Link can not load at this time." CancelButtonTitle:@"Ok" InView:self];
    }
    
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    isChangesOK = TRUE;
    [self.view endEditing:YES];
    
    if (isChangesOK == TRUE) {
        [self CallMethodToSaveData];
    }
    
}

-(void)CallMethodToSaveData
{
    NSMutableArray *responseArray = [[NSMutableArray alloc] init];
    for (int i = 0;  i < [arrayOutfittingList count];  i ++)
    {
        [responseArray addObjectsFromArray:[[arrayOutfittingList objectAtIndex:i] objectForKey:@"records"]];
    }
    
    [self callServiceToSaveOutfittinglistWithArray:responseArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.isChangesMade = FALSE;
}

#pragma - mark WebService Call
-(void)callServiceToGetOutfittinglist {
    
    [COMMON_SETTINGS showHUD];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    if(_isLogedInUsersDetail)
    {
        [parameters setValue:@1 forKey:@"IsPersonalDetail"];
    }
    else
    {
        [parameters setValue:@0 forKey:@"IsPersonalDetail"];
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetOutfittingAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
    
}

-(void)callServiceToSaveOutfittinglistWithArray:(NSMutableArray*)arrayResponse {
    
    [COMMON_SETTINGS showHUD];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:arrayResponse forKey:@"lstOutfitting"];
    
    if(_isLogedInUsersDetail)
    {
        [parameters setValue:@1 forKey:@"IsPersonalDetail"];
    }
    else
    {
        [parameters setValue:@0 forKey:@"IsPersonalDetail"];
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateOutfitting withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Save;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            quickGuidUrl = [responseDictionery valueForKey:@"quickGuidLink"];
            quickGuidUrl = @"https://nocausweb.amo-sport.com/WriteReadData/CMSInformation/CMSDocument/04122015031752_635847958729291034_OUTFITTING%20SIZE%20GUIDE.pdf";
            NSString *heaserHtmlString = [NSString stringWithFormat:@"%@",[responseDictionery valueForKey:@"OutfitHeaderHtml"]];
            [_headerWebView loadHTMLString:heaserHtmlString baseURL:nil];
            
            arrayOutfittingList = [[NSMutableArray alloc] initWithArray:[responseDictionery valueForKey:@"lstOutfitting"]];
            
            NSString *title;
            NSMutableArray *mainArray = [[NSMutableArray alloc] init];
            NSMutableArray *subArray;
            for (int i = 0; i < [arrayOutfittingList count]; i++) {
                NSString *localTitle = [[arrayOutfittingList objectAtIndex:i]valueForKey:@"Wardrobe"];
                if([localTitle isEqualToString:title])
                {
                    [subArray addObject:[arrayOutfittingList objectAtIndex:i]];
                }
                else
                {
                    if([subArray count] > 0)
                    {
                        NSMutableDictionary *singleTitle = [[NSMutableDictionary alloc] init];
                        [singleTitle setValue:title forKey:@"title"];
                        [singleTitle setObject:subArray forKey:@"records"];
                        [mainArray addObject:singleTitle];
                    }
                    title = localTitle;
                    subArray = [[NSMutableArray alloc] init];
                    [subArray addObject:[arrayOutfittingList objectAtIndex:i]];
                }
                if([arrayOutfittingList count]-1 == i)
                {
                    NSMutableDictionary *singleTitle = [[NSMutableDictionary alloc] init];
                    [singleTitle setValue:title forKey:@"title"];
                    [singleTitle setObject:subArray forKey:@"records"];
                    [mainArray addObject:singleTitle];
                }
            }
            [arrayOutfittingList removeAllObjects];
            arrayOutfittingList = [[NSMutableArray alloc] initWithArray:mainArray];

            [mainArray removeAllObjects];
            mainArray = nil;
            
            tableviewOutfiting.dataSource = self;
            tableviewOutfiting.delegate = self;
            [tableviewOutfiting reloadData];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if (_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - UIWebView Dlegate

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    NSInteger height = [result integerValue];
    _webViewSuperViewHeightConstraint.constant = height;
    [self.view setNeedsDisplay];
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *URL = [[request.URL absoluteString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![URL isEqualToString:@"about:blank"] )
    {
        [[UIApplication sharedApplication] openURL:request.URL];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}


/**************************** TABLEVIEW Functions *******************************************************/
#pragma mark - UITableView Delegate & Datasrouce

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrayOutfittingList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[[arrayOutfittingList objectAtIndex:section] objectForKey:@"records"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [[[arrayOutfittingList objectAtIndex:indexPath.section] valueForKey:@"records"] objectAtIndex:indexPath.row];
    NSInteger NoteExits = [[dict valueForKey:@"NoteExits"] integerValue];
    if(IS_IPAD){
        if(NoteExits)
            return 146;
        return 105;
    }
    else{
        if(NoteExits)
            return 98;
        return 65;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *itemCell = @"outfittingCustomCell";
    outfittingCustomCell *cell = (outfittingCustomCell *)[self.tableviewOutfiting dequeueReusableCellWithIdentifier:itemCell];
    
    if (cell == nil)
    {
        NSArray *nib;
        if(IS_IPAD)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"outfittingCustomCellIpad" owner:nil options:nil];
        }
        else
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"outfittingCustomCell" owner:nil options:nil];
        }
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    [cell.changeSizeButton addTarget:self action:@selector(DidSelectChangeSizeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDictionary *dict = [[[arrayOutfittingList objectAtIndex:indexPath.section] valueForKey:@"records"] objectAtIndex:indexPath.row];
    
    
    //Set Up Size Values
    NSInteger sizeSelected = [[dict valueForKey:@"SizeSelected"] integerValue];
    NSArray *sizeArrayLocal =  [dict objectForKey:@"cboSize"];
    if(![sizeArrayLocal isKindOfClass:[NSNull class]] && [sizeArrayLocal count]>0)
    {
        NSUInteger selectedValue = [sizeArrayLocal indexOfObjectPassingTest:
                                    ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
                                    {
                                        return [[dict objectForKey:@"Key"] integerValue] == sizeSelected;
                                    }
                                    ];
        if(selectedValue != NSNotFound)
        {
            cell.sizeValueLabel.text = [[sizeArrayLocal objectAtIndex:selectedValue] valueForKey:@"Value"];
        }
        else
        {
            cell.sizeValueLabel.text = [[sizeArrayLocal objectAtIndex:0] valueForKey:@"Value"];
        }
    }
    else
    {
        cell.sizeValueLabel.text = @"";
    }
    
    //Setup Titles For All Fields
    
    cell.titleLabel.textColor = color_pinkFont;
    cell.titleLabel.text = [dict valueForKey:@"Garment"];
    cell.StyleCodeLabel.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"StyleCode"]];
    cell.GarmentLabel.text = [dict valueForKey:@"Outfit"];
    cell.isAlowedValueTextField.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"NumberAllowed"]];
    
    //Setup Notes Text Field
    NSInteger NoteExits = [[dict valueForKey:@"NoteExits"] integerValue];
    cell.notesTextField.tag = 2;
    if(NoteExits == 1)
    {
        //NoteText;
        cell.notesTextField.hidden = FALSE;
        cell.notesTextField.text = [dict valueForKey:@"NoteText"];
        cell.notesTextField.delegate = self;
    }
    else
    {
        cell.notesTextField.hidden = TRUE;
        cell.notesTextField.text = @"";
        cell.notesTextField.delegate = nil;
        
        
    }
    
    //setup IsAwwoed TextField
    cell.isAlowedValueTextField.tag = 1;
    if([[dict valueForKey:@"NumberAllowedEdit"] integerValue])
    {
        cell.isAlowedValueTextField.userInteractionEnabled = TRUE;
        cell.isAlowedValueTextField.delegate = self;
        cell.isAlowedValueTextField.textColor = [UIColor blackColor];
    }
    else
    {
        cell.isAlowedValueTextField.userInteractionEnabled = FALSE;
        cell.isAlowedValueTextField.textColor = [UIColor grayColor];
        
    }
    
    self.isChangesMade = FALSE;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(IS_IPAD){
        return 60;
    }
    else{
        return 40;
    }
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = [NSString stringWithFormat:@"   Wardrobe - %@",[[arrayOutfittingList objectAtIndex:section] valueForKey:@"title"]];
    return title;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(0, 0, tableView.frame.size.width, [self tableView:tableView heightForHeaderInSection:section]);
    myLabel.textColor = [UIColor blackColor];
    myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    [headerView setBackgroundColor:[UIColor colorWithRed:0.8026 green:0.8026 blue:0.8026 alpha:1.0]];
    
    myLabel.layer.masksToBounds = NO;
    myLabel.layer.shadowRadius = 2;
    myLabel.layer.shadowColor = [UIColor colorWithRed:0.9490 green:0.9490 blue:0.9490 alpha:1.0].CGColor;
    myLabel.layer.shadowOpacity = 1;
    CGFloat indent = 0;
    CGRect innerRect = CGRectMake(-10,indent,myLabel.frame.size.width+20,myLabel.frame.size.height-2*indent);
    myLabel.layer.shadowPath = [UIBezierPath bezierPathWithRect:innerRect].CGPath;
    
    return headerView;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

/******************************************************************************************************************************************/

//-(NSArray *) createRightButtons: (int) number
//{
//    NSMutableArray *result = [NSMutableArray array];
//    NSString *titles[2] = {@"INFORMATION", @"PERSONAL"};
//    NSString *text1,*text2;
//    NSDictionary *attrDict;
//    text1 = @"\ue90c";
//    text2 = @"\ue90d";
//
//    CGRect rect = CGRectMake(0, 0, 32, 32);
//
//    if(IS_IPAD)
//    {
//        attrDict = @{
//                                   NSFontAttributeName : [UIFont fontWithName:@"icomoon" size:32.0],
//                                   NSForegroundColorAttributeName : [UIColor whiteColor]
//                                   };
//
//    }
//    else if (IS_IPHONE)
//    {
//        attrDict = @{
//                                   NSFontAttributeName : [UIFont fontWithName:@"icomoon" size:20.0],
//                                   NSForegroundColorAttributeName : [UIColor whiteColor]
//                                   };
//    }
//
//    UIGraphicsBeginImageContext(rect.size);
//    [text2 drawInRect:rect withAttributes:attrDict];
//    UIImage *newImage1 = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    UIGraphicsBeginImageContext(rect.size);
//    [text1 drawInRect:rect withAttributes:attrDict];
//    UIImage *newImage2 = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    //UIImage *personal = [UIImage imageNamed:@"edit"];
//    //UIImage *information = [UIImage imageNamed:@"delete"];
//    UIImage *icons[2] = {newImage1,newImage2};
//
//    UIColor *colors[2] = {[UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1], [UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1]};
//
//    for (int i = 0; i < 2; ++i)
//    {
//        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
//                                  {
//                                      return YES;
//                                  }];
//
//        CGRect frame = button.frame;
//        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
//        button.frame = frame;
//        [result addObject:button];
//    }
//    return result;
//}
//
//#if USE_MG_DELEGATE
//-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
//             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
//{
//
//    CGPoint buttonPosition = [cell.swipeContentView convertPoint:CGPointZero
//                                                          toView:self.tableviewOutfiting];
//    NSIndexPath *indexPath = [self.tableviewOutfiting indexPathForRowAtPoint:buttonPosition];
//    eventDetails = [arrayOutfittingList objectAtIndex:indexPath.row];
//
//    NSDictionary *dict = [arrayOutfittingList objectAtIndex:indexPath.row];
//    bool IsPrimary =  [[dict valueForKey:@"IsPrimary"] boolValue];
//
//    if (IsPrimary == true) {
//
//        return nil;
//    }
//    TestData *data = [swipeCellButtons objectAtIndex:0];
//    swipeSettings.transition = data.transition;
//
//    if (direction == MGSwipeDirectionLeftToRight)
//    {
//        expansionSettings.buttonIndex = data.leftExpandableIndex;
//        expansionSettings.fillOnTrigger = NO;
//        return 0;
//    }
//    else
//    {
//        expansionSettings.buttonIndex = data.rightExpandableIndex;
//        expansionSettings.fillOnTrigger = YES;
//        return [self createRightButtons:data.rightButtonsCount];
//    }
//}
//#endif
//
//-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
//{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
//
//
//    NSIndexPath *indexPath = [self.tableviewOutfiting indexPathForCell:cell];
//    eventDetails = [arrayOutfittingList objectAtIndex:indexPath.row];
//
//
//    if (index == 0) {
//        // Show popup.
//
//        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
//                                                                           message:@"Are you sure you want the Delete Your Email Details?"
//                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
//            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
//                                                                       style:(UIAlertActionStyleDefault)
//
//                                                                     handler:^(UIAlertAction * _Nonnull action) {
//
//                                                                         //Is User Wants to delete Email Details Then Continiue.
//                                                                         NSLog(@"Delete");
//                                                                         currentIndexPathToDelete = indexPath;
//                                                                         [self callServiceDeleteOutfittinglist];
//
//                                                                     }];
//            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
//                                                                      style:(UIAlertActionStyleCancel)
//                                                                    handler:^(UIAlertAction * _Nonnull action) {
//                                                                        //
//                                                                    }];
//            [alert addAction:alert_yes_action];
//            [alert addAction:alert_no_action];
//            [self presentViewController:alert animated:YES completion:nil];
//        } else {
//            // code to be written for version lower than ios 8.0...
//        }
//        //------------------------------------------------------------------------
//
//    } else {
//
//        [self callServiceEditOutfittinglist];
//    }
//
//    return YES;
//}
//

#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.isChangesMade = TRUE;
    [self scrollUp:textField];
    
    UIToolbar *DoneButtonToolbar = [[UIToolbar alloc] init];
    [DoneButtonToolbar sizeToFit];
    DoneButtonToolbar.tintColor = [UIColor colorWithRed:0.1176 green:0.0902 blue:0.2549 alpha:1.0f];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(ResignFirstResponderTextView)];
    doneButton.tag = textField.tag;
    [DoneButtonToolbar setItems:[NSArray arrayWithObjects:flexibleItem,doneButton, nil]];
    textField.inputAccessoryView = DoneButtonToolbar;
    
}

-(void)ResignFirstResponderTextView
{
    [self.view endEditing:YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self scrollDown];
    isChangesOK = TRUE;
    
    NSInteger tag = [textField tag];
    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:[self tableviewOutfiting]];
    NSIndexPath *indexPath = [[self tableviewOutfiting] indexPathForRowAtPoint:switchPositionPoint];
    if(tag == 1)
    {
        NSMutableDictionary *dictioneryToReplace = [[NSMutableDictionary alloc] initWithDictionary:[[[arrayOutfittingList objectAtIndex:indexPath.section] valueForKey:@"records"] objectAtIndex:indexPath.row]];
        
        NSString *allowedValue = [NSString stringWithFormat:@"%@",[dictioneryToReplace valueForKey:@"MaxNumberAllowed"]];
        
        if ([textField.text integerValue] > [allowedValue integerValue]) {
            NSLog(@"Not Possible!");
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"You can't choose a value greater than allowed number !" CancelButtonTitle:@"OK" InView:self];
            textField.text = allowedValue;
            isChangesOK = FALSE;
        }
        else{
            [dictioneryToReplace setValue:textField.text forKey:@"NumberAllowed"];
            [[[arrayOutfittingList objectAtIndex:indexPath.section] objectForKey:@"records"] replaceObjectAtIndex:indexPath.row withObject:dictioneryToReplace];
        }
    }
    else if(tag == 2)
    {
        NSMutableDictionary *dictioneryToReplace = [[NSMutableDictionary alloc] initWithDictionary:[[[arrayOutfittingList objectAtIndex:indexPath.section] valueForKey:@"records"] objectAtIndex:indexPath.row]];
        
        [dictioneryToReplace setValue:textField.text forKey:@"NoteText"];
        [[[arrayOutfittingList objectAtIndex:indexPath.section] objectForKey:@"records"] replaceObjectAtIndex:indexPath.row withObject:dictioneryToReplace];
    }
}

#pragma mark - UIPickerView Delegate

-(void)DidSelectChangeSizeButton:(UIButton*)sender
{
    self.isChangesMade = TRUE;
    CGPoint switchPositionPoint = [sender convertPoint:CGPointZero toView:[self tableviewOutfiting]];
    NSIndexPath *indexPath = [[self tableviewOutfiting] indexPathForRowAtPoint:switchPositionPoint];
    
    selectedIndexPath = indexPath;
    NSDictionary *dict = [[[arrayOutfittingList objectAtIndex:indexPath.section] valueForKey:@"records"] objectAtIndex:indexPath.row];
    NSArray *sizeArray = [dict objectForKey:@"cboSize"];
    if(![sizeArray isKindOfClass:[NSNull class]] && [sizeArray count] > 0)
    {
        [sizeSelectionTextField becomeFirstResponder];
        [sizePickerView reloadAllComponents];
        
        NSInteger sizeSelected = [[dict valueForKey:@"SizeSelected"] integerValue];
        NSUInteger selectedValue = [sizeArray indexOfObjectPassingTest:
                                    ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop)
                                    {
                                        return [[dict objectForKey:@"Key"] integerValue] == sizeSelected;
                                    }
                                    ];
        if(selectedValue != NSNotFound)
        {
            [self pickerView:sizePickerView didSelectRow:selectedValue inComponent:0];
            [sizePickerView selectRow:selectedValue inComponent:0 animated:YES];
        }
        else
        {
            [self pickerView:sizePickerView didSelectRow:0 inComponent:0];
            [sizePickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
    else
    {
        [sizeSelectionTextField resignFirstResponder];
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Size Not Found." CancelButtonTitle:@"Ok" InView:self];
    }
}

-(void)SetupPickerViewForSize
{
    sizeSelectionTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [self.view addSubview:sizeSelectionTextField];
    sizePickerView = [[UIPickerView alloc] init];
    sizePickerView.dataSource = self;
    sizePickerView.delegate = self;
    
    UIToolbar *DoneButtonToolbar = [[UIToolbar alloc] init];
    [DoneButtonToolbar sizeToFit];
    DoneButtonToolbar.tintColor = [UIColor colorWithRed:0.1176 green:0.0902 blue:0.2549 alpha:1.0f];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(sizePickerViewShouldHide)];
    [DoneButtonToolbar setItems:[NSArray arrayWithObjects:flexibleItem,doneButton, nil]];
    sizeSelectionTextField.inputAccessoryView = DoneButtonToolbar;
    sizeSelectionTextField.inputView = sizePickerView;
}

-(void)sizePickerViewShouldHide
{
    [sizeSelectionTextField resignFirstResponder];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSDictionary *dict = [[[arrayOutfittingList objectAtIndex:selectedIndexPath.section] valueForKey:@"records"] objectAtIndex:selectedIndexPath.row];
    NSArray *sizeArray = [dict objectForKey:@"cboSize"];
    return  [sizeArray count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary *dict = [[[arrayOutfittingList objectAtIndex:selectedIndexPath.section] valueForKey:@"records"] objectAtIndex:selectedIndexPath.row];
    NSArray *sizeArray = [dict objectForKey:@"cboSize"];
    return [[sizeArray objectAtIndex:row] valueForKey:@"Value"];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    NSMutableDictionary *dictioneryToReplace = [[NSMutableDictionary alloc] initWithDictionary:[[[arrayOutfittingList objectAtIndex:selectedIndexPath.section] valueForKey:@"records"] objectAtIndex:selectedIndexPath.row]];
    NSMutableArray *sizeArray = [[NSMutableArray alloc] initWithArray:[dictioneryToReplace objectForKey:@"cboSize"]];
    
    outfittingCustomCell *cell = (outfittingCustomCell*)[tableviewOutfiting cellForRowAtIndexPath:selectedIndexPath];
    cell.sizeValueLabel.text = [[sizeArray objectAtIndex:row] valueForKey:@"Value"];
    
    NSString *selectedKey = [[sizeArray objectAtIndex:row] valueForKey:@"Key"];
    [dictioneryToReplace setValue:selectedKey forKey:@"SizeSelected"];
    [[[arrayOutfittingList objectAtIndex:selectedIndexPath.section] objectForKey:@"records"] replaceObjectAtIndex:selectedIndexPath.row withObject:dictioneryToReplace];
}

#pragma mark - View Scrolling
-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.tableviewOutfiting.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.tableviewOutfiting.window convertRect:self.tableviewOutfiting.bounds fromView:self.tableviewOutfiting];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.tableviewOutfiting.frame;
    viewFrame.origin.y -= animatedDistance;
    
    //[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.tableviewOutfiting setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.tableviewOutfiting.frame;
    viewFrame.origin.y += animatedDistance;
    
    //[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.tableviewOutfiting setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:Alert_Title message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end

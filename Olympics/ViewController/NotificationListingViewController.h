//
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "NotificationCustomCell.h"

@interface NotificationListingViewController : UIViewController<UITableViewDataSource,MGSwipeTableCellDelegate, Service_delegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property(nonatomic,assign) Service_Called serviceCalled;
@property (nonatomic, assign) BOOL isNeedToRefresh;
@property (nonatomic, assign) BOOL isFirstTime;
@property (nonatomic, strong) UIViewController *owner;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableviewAddreslist;

@end

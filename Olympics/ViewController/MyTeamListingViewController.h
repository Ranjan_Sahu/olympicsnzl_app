//
//  MyTeamListingViewController.h
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "addressListTableViewCell.h"
#import "AddEventViewController.h"
#import "PersonalDetailViewController.h"
#import "InformationListViewController.h"

// custom controllers Imports
#import "Question_Object.h"
#import "OLMLable.h"
#import "OLMCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "OLMRadioButton.h"
#import "OLMDatePicker.h"
#import "Service.h"
#import "OLMRadioButtonTextKeyType.h"
#import "FooterCustomCell.h"

@interface MyTeamListingViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MGSwipeTableCellDelegate, Service_delegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (nonatomic, assign) Service_Called serviceCalled;
@property (nonatomic, assign) BOOL isNeedToRefresh;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSearchButton:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tableviewMyTeamList;

// SlideView Outlets
@property (weak, nonatomic) IBOutlet UIView *viewSlide;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slideViewWidth;
@property (nonatomic) NSInteger incrementedHeight;
@property (nonatomic, strong) NSMutableArray *controlls;
@property (weak, nonatomic) IBOutlet UIImageView *sideImageView;

@end

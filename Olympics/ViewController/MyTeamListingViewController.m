//
//  EmailListingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "MyTeamListingViewController.h"
#import "EventListCustomCell.h"

#define USE_MG_DELEGATE 1

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface MyTeamListingViewController (){
    
    NSMutableArray *swipeCellButtons, *controlls;
    NSDictionary *eventDetails;
    NSMutableArray *arrayMyTeamList,*searchResults;
    NSArray *eventtypeToShowArray,*drpArrayTeam,*drpArraySportDiscipline,
    *drpArraySelectionStatus,*drpArrayFunction,*drpArrayAdminGroup;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
    BOOL isSideMenuOpen;
    CGFloat animatedDistance;
}
@end

@implementation MyTeamListingViewController
@synthesize tableviewMyTeamList,incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue912"];
    [_searchButton setTitle:@"\ue92b" forState:UIControlStateNormal];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if(IS_IPAD)
    {
        _slideViewWidth.constant = 480;
        [self.view layoutIfNeeded];
        [self.view updateConstraintsIfNeeded];
    }
    
    [self callServiceToGetMyTeamlist];
    
    _sideImageView.layer.masksToBounds = NO;
    _sideImageView.layer.shadowRadius = 2;
    _sideImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    _sideImageView.layer.shadowOpacity = 0.5;
    CGFloat indent = 1.5;
    CGRect innerRect = CGRectMake(indent,indent,_sideImageView.frame.size.width-2*indent,_sideImageView.frame.size.height-2*indent);
    _sideImageView.layer.shadowPath = [UIBezierPath bezierPathWithRect:innerRect].CGPath;
    
    [self.baseScrollView setNeedsDisplay];
    self.isNeedToRefresh=FALSE;
    
}
- (void)viewDidLayoutSubviews
{
    if (isSideMenuOpen)
    {
        CGRect frame = _viewSlide.frame;
        frame.origin.x = (self.view.bounds.size.width - _viewSlide.frame.size.width);
        _viewSlide.frame = frame;
    }
    else
    {
        CGRect frame = _viewSlide.frame;
        frame.origin.x = self.view.bounds.size.width;
        _viewSlide.frame = frame;
        tableviewMyTeamList.userInteractionEnabled = TRUE;
    }
    
}
- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)DidSelectSearchButton:(id)sender {
    
    tableviewMyTeamList.userInteractionEnabled = FALSE;
    if(isSideMenuOpen)
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^
         {
             CGRect frame = _viewSlide.frame;
             frame.origin.x = self.view.bounds.size.width;
             _viewSlide.frame = frame;
             tableviewMyTeamList.userInteractionEnabled = TRUE;
             
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"Completed");
             isSideMenuOpen=false;
             [self.view endEditing:YES];
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionShowHideTransitionViews
                         animations:^
         {
             CGRect frame = _viewSlide.frame;
             frame.origin.x = (self.view.bounds.size.width - _viewSlide.frame.size.width);
             _viewSlide.frame = frame;
             
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"Completed");
             isSideMenuOpen=true;
             
         }];
        [UIView commitAnimations];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

-(void)dealloc
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_ArrayMyTeamListString];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_MyTeamListCurrentIndex];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark TouchEvents

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if((isSideMenuOpen)&&(![touch.view isKindOfClass:[UIScrollView class]]))
    {
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^
         {
             CGRect frame = _viewSlide.frame;
             frame.origin.x = self.view.bounds.size.width;
             _viewSlide.frame = frame;
             tableviewMyTeamList.userInteractionEnabled = TRUE;
             
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"Completed");
             isSideMenuOpen=false;
             [self.view endEditing:YES];
             
         }];
        
    }
    else
    {
        //NSLog(@"DO NOthing");
    }
    [UIView commitAnimations];
}

#pragma - mark WebService Call
-(void)callServiceToGetMyTeamlist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetMyTeamAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_GetDetail;
    
}

#pragma mark - Method to add questions on SlideView
-(void)StartAddingControllers {
    
    isSideMenuOpen=false;
    incrementedHeight = 10;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    CGRect buttonFrame;
    
    // Field_TextField.
    OLMTextField *controlObj1 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Name" fieldIdName:@"Name" questionId:101 IsRequired:NO defaultValue:@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightBlack];
    controlObj1.questionTypeId = Field_TextField;
    controlObj1.questionLbl.textColor = color_white;
    controlObj1.textFieldView.textColor = color_white;
    controlObj1.tag = 1;
    [controlls addObject:controlObj1];
    [self.baseScrollView addSubview:controlObj1];
    incrementedHeight = incrementedHeight + controlObj1.frame.size.height + 5;
    
    // Field_DropDown.
    if(![drpArrayAdminGroup isKindOfClass:[NSNull class]] && drpArrayAdminGroup && [drpArrayAdminGroup count]>0)
    {
        OLMDropDown *controlObj2 = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Admin Group" fieldIdName:@"AdminGroup" questionId:102 Options:drpArrayAdminGroup IsRequired:NO defaultValue:0 delegate:self lineColor:color_lightBlack];
        controlObj2.questionTypeId = Field_DropdownList;
        controlObj2.questionLbl.textColor = color_white;
        controlObj2.optionTextfield.textColor = color_white;
        controlObj2.tag = 2;
        [controlls addObject:controlObj2];
        [self.baseScrollView addSubview:controlObj2];
        incrementedHeight = incrementedHeight + controlObj2.frame.size.height + 5;
    }
    
    // Field_DropDown.
    if(![drpArraySportDiscipline isKindOfClass:[NSNull class]] && drpArraySportDiscipline && [drpArraySportDiscipline count]>0)
    {
        OLMDropDown *controlObj3 = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Sport/Disciplune" fieldIdName:@"Sport" questionId:103 Options:drpArraySportDiscipline IsRequired:NO defaultValue:0 delegate:self lineColor:color_lightBlack];
        controlObj3.questionTypeId = Field_DropdownList;
        controlObj3.questionLbl.textColor = color_white;
        controlObj3.optionTextfield.textColor = color_white;
        controlObj3.tag = 3;
        [controlls addObject:controlObj3];
        [self.baseScrollView addSubview:controlObj3];
        incrementedHeight = incrementedHeight + controlObj3.frame.size.height + 5;
    }
    
    // Field_DropDown.
    if(![drpArrayTeam isKindOfClass:[NSNull class]] && drpArrayTeam && [drpArrayTeam count]>0)
    {
        OLMDropDown *controlObj4 = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Team" fieldIdName:@"Team" questionId:104 Options:drpArrayTeam IsRequired:NO defaultValue:0 delegate:self lineColor:color_lightBlack];
        controlObj4.questionTypeId = Field_DropdownList;
        controlObj4.questionLbl.textColor = color_white;
        controlObj4.optionTextfield.textColor = color_white;
        controlObj4.tag = 4;
        [controlls addObject:controlObj4];
        [self.baseScrollView addSubview:controlObj4];
        incrementedHeight = incrementedHeight + controlObj4.frame.size.height + 5;
    }
    
    // Field_DropDown.
    if(![drpArrayTeam isKindOfClass:[NSNull class]] && drpArrayTeam && [drpArrayTeam count]>0)
    {
        OLMDropDown *controlObj5 = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Selection Section" fieldIdName:@"SelectionStatus" questionId:105 Options:drpArraySelectionStatus IsRequired:NO defaultValue:0 delegate:self lineColor:color_lightBlack];
        controlObj5.questionTypeId = Field_DropdownList;
        controlObj5.questionLbl.textColor = color_white;
        controlObj5.optionTextfield.textColor = color_white;
        controlObj5.tag = 5;
        [controlls addObject:controlObj5];
        [self.baseScrollView addSubview:controlObj5];
        incrementedHeight = incrementedHeight + controlObj5.frame.size.height + 5;
    }
    // Field_DropDown.
    if(![drpArrayFunction isKindOfClass:[NSNull class]] && drpArrayFunction && [drpArrayFunction count]>0)
    {
        OLMDropDown *controlObj6 = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Function" fieldIdName:@"Function" questionId:106 Options:drpArrayFunction IsRequired:NO defaultValue:0 delegate:self lineColor:color_lightBlack];
        controlObj6.questionTypeId = Field_DropdownList;
        controlObj6.questionLbl.textColor = color_white;
        controlObj6.optionTextfield.textColor = color_white;
        controlObj6.tag = 6;
        [controlls addObject:controlObj6];
        [self.baseScrollView addSubview:controlObj6];
        incrementedHeight = incrementedHeight + controlObj6.frame.size.height + 5;
        buttonFrame = controlObj6.frame;
    }
    
    // UIButton
    CGFloat btnHeight = 34;
    UIButton *applyButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonFrame.origin.x, incrementedHeight+16,buttonFrame.size.width , btnHeight)];
    
    [applyButton.titleLabel setFont:app_font_bold_18];
    if(IS_IPAD) {
        btnHeight = 50;
        [applyButton.titleLabel setFont:app_font_bold_24];
    }
    [applyButton setBackgroundColor: color_white];
    [applyButton setTitle:@"APPLY" forState:UIControlStateNormal];
    [applyButton setTitleColor:[UIColor colorWithRed:0.8941 green:0.5451 blue:0.0000 alpha:1.0] forState:UIControlStateNormal];
    applyButton.layer.shadowOffset = CGSizeMake(1,1);
    applyButton.layer.shadowColor = [UIColor blackColor].CGColor;
    applyButton.layer.shadowOpacity = 0.3;
    applyButton.layer.shadowRadius = 1;
    [applyButton addTarget:self action:@selector(DidSelectApplyButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.baseScrollView addSubview:applyButton];
    incrementedHeight = incrementedHeight + applyButton.frame.size.height + 5;
    [applyButton setHidden:FALSE];
}

-(NSMutableDictionary *)saveEnteredData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
            }
        }
    }
    return requestParameters;
}

-(void)DidSelectApplyButton:(UIButton *)sender
{
    NSMutableDictionary *searchParam =  [self saveEnteredData];
    NSMutableDictionary *searchData = [NSMutableDictionary dictionaryWithObject:[searchParam valueForKey:@"Name"] forKey:@"Name"];
    
    if(!([[searchParam valueForKey:@"AdminGroup"]integerValue]==0))
    {
        for(int i = 0;i<drpArrayAdminGroup.count;i++)
        {
            if([[[[drpArrayAdminGroup objectAtIndex:i] valueForKey:@"Key"] stringValue]isEqualToString:[[searchParam valueForKey:@"AdminGroup"] stringValue]])
            {
                NSDictionary *answer = @{@"AdminGroup":[[drpArrayAdminGroup objectAtIndex:i] valueForKey:@"Value"]};
                [searchData addEntriesFromDictionary:answer];
                break;
            }
            else
            {
                //NSLog(@"Data not Found");
            }
        }
        
    }
    if(!([[searchParam valueForKey:@"Function"]integerValue]==0))
    {
        for(int i = 0;i<drpArrayFunction.count;i++)
        {
            if([[[[drpArrayFunction objectAtIndex:i] valueForKey:@"Key"] stringValue]isEqualToString:[[searchParam valueForKey:@"Function"] stringValue]])
            {
                NSDictionary *answer = @{@"Function":[[drpArrayFunction objectAtIndex:i] valueForKey:@"Value"]};
                [searchData addEntriesFromDictionary:answer];
                break;
            }
            else
            {
                //NSLog(@"Data not Found");
            }
        }
        
    }
    if(!([[searchParam valueForKey:@"SelectionStatus"]integerValue]==0))
    {
        for(int i = 0;i<drpArraySelectionStatus.count;i++)
        {
            if([[[[drpArraySelectionStatus objectAtIndex:i] valueForKey:@"Key"] stringValue]isEqualToString:[[searchParam valueForKey:@"SelectionStatus"] stringValue]])
            {
                NSDictionary *answer = @{@"SelectionStatus":[[drpArraySelectionStatus objectAtIndex:i] valueForKey:@"Value"]};
                [searchData addEntriesFromDictionary:answer];
                break;
            }
            else
            {
                //NSLog(@"Data not Found");
            }
        }
        
    }
    if(!([[searchParam valueForKey:@"Sport"] integerValue]==0))
    {
        for(int i = 0;i<drpArraySportDiscipline.count;i++)
        {
            NSString *str1 = [NSString stringWithFormat:@"%@",[[drpArraySportDiscipline objectAtIndex:i] valueForKey:@"Key"]];
            NSString *str2 = [NSString stringWithFormat:@"%@",[searchParam valueForKey:@"Sport"]];
            
            if([str1 isEqualToString:str2])
            {
                NSDictionary *answer = @{@"Sport":[[drpArraySportDiscipline objectAtIndex:i] valueForKey:@"Value"]};
                [searchData addEntriesFromDictionary:answer];
                break;
            }
            else
            {
                //NSLog(@"Data not Found");
            }
        }
        
    }
    if(!([[searchParam valueForKey:@"Team"] integerValue]==0))
    {
        for(int i = 0;i<drpArrayTeam.count;i++)
        {
            if([[[[drpArrayTeam objectAtIndex:i] valueForKey:@"Key"] stringValue]isEqualToString:[[searchParam valueForKey:@"Team"] stringValue]])
            {
                NSDictionary *answer = @{@"Team":[[drpArrayTeam objectAtIndex:i] valueForKey:@"Value"]};
                [searchData addEntriesFromDictionary:answer];
                break;
            }
            else
            {
                //NSLog(@"Data not Found");
            }
        }
        
    }
    
    [self filterContentForSearchText:searchData scope:@"ALL"];
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstMyTeamList"] isKindOfClass:[NSNull class]])
            {
                arrayMyTeamList = [[responseDictionery objectForKey:@"lstMyTeamList"] mutableCopy];
                searchResults = [[NSMutableArray alloc] initWithArray:arrayMyTeamList];
            }
            drpArrayAdminGroup = [[responseDictionery objectForKey:@"drpAdminGroup"] mutableCopy];
            drpArraySportDiscipline = [[responseDictionery objectForKey:@"drpSportDiscipline"] mutableCopy];
            drpArrayTeam = [[responseDictionery objectForKey:@"drpTeam"] mutableCopy];
            drpArraySelectionStatus = [[responseDictionery objectForKey:@"drpSelectionStatus"] mutableCopy];
            drpArrayFunction = [[responseDictionery objectForKey:@"drpFunction"] mutableCopy];
            
            // Store MyTeamList in NSUserDefaults
            if(![arrayMyTeamList isKindOfClass:[NSNull class]] && [arrayMyTeamList count]>0)
            {
                NSData *arrayMyTeamListjsonData = [NSJSONSerialization dataWithJSONObject:arrayMyTeamList options:NSJSONWritingPrettyPrinted error:&error];
                
                NSString *arrayMyTeamListString = [[NSString alloc] initWithData:arrayMyTeamListjsonData encoding:NSUTF8StringEncoding];
                [[NSUserDefaults standardUserDefaults] setObject:arrayMyTeamListString forKey:UD_ArrayMyTeamListString];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"No Records Found !" CancelButtonTitle:@"OK" InView:self];
            }

            [self StartAddingControllers];
            
            //If Array Has the data then only Reload Data.
            if([searchResults count] > 0) {
                [self.tableviewMyTeamList reloadData];
            }
        }
        else
        {
            if(![[responseDictionery valueForKey:@"ErrorMessage"] isKindOfClass:[NSNull class]])
                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
            else
                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Exceptional Case !!" CancelButtonTitle:@"Ok" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/**************************** TABLEVIEW Functions ***************************/
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchResults.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
        return 124;
    else
        return 94;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *itemCell = @"EventListCustomCell";
    EventListCustomCell *cell = (EventListCustomCell *)[self.tableviewMyTeamList dequeueReusableCellWithIdentifier:itemCell];
    
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventListCustomCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.delegate = self;
    if(IS_IPAD){
        [cell.lblEventNameText setFont:app_font_bold_24];
        [cell.lblPositionText setFont:app_font23];
        [cell.lblFieldText setFont:app_font18];
        [cell.btnFormSubmitted.titleLabel setFont:app_font_bold_18];
    }
    else if(IS_IPHONE){
        [cell.lblEventNameText setFont:app_font_bold_20];
        [cell.lblPositionText setFont:app_font19];
        [cell.lblFieldText setFont:app_font15];
        [cell.btnFormSubmitted.titleLabel setFont:app_font_bold_15];
    }
    
    NSDictionary *dict = [searchResults objectAtIndex:indexPath.row];
    cell.lblEventNameText.text = [dict valueForKey:@"Name"];
    cell.lblPositionText.text = [dict valueForKey:@"Sport"];
    cell.lblFieldText.text = [dict valueForKey:@"SelectionStatus"];
    
    NSString *formTitle = [[dict valueForKey:@"Function"] uppercaseString];
    formTitle = [formTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    [cell.btnFormSubmitted setTitle:formTitle forState:UIControlStateNormal];
    
    NSString *functionString = formTitle;
    
    CGFloat buttonWidth = [COMMON_SETTINGS getWidthForText:functionString withFont:cell.btnFormSubmitted.titleLabel.font andHeight:cell.btnFormSubmitted.frame.size.height minWidth:30];
    cell.btnFormSubmittedWidth.constant = buttonWidth + 30;
    [cell.btnFormSubmitted layoutIfNeeded];
    
    for (CALayer *layer in cell.viewFormBack.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    CAShapeLayer *labelView = [self GetLableForFormSubmitedBackgroundInFrame:cell.btnFormSubmitted.frame];
    [cell.viewFormBack.layer addSublayer:labelView];
    
    if ([[dict valueForKey:@"Function"]isEqualToString:@"Official"])
    {
        labelView.fillColor = [UIColor colorWithRed:0.7804 green:0.7804 blue:0.7804 alpha:1.0].CGColor;
        [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
    }
    else
    {
        labelView.fillColor = [UIColor colorWithRed:0.8784 green:0.6510 blue:0.0627 alpha:1.0].CGColor;
        [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
    }
    return cell;
}

-(CAShapeLayer*)GetLableForFormSubmitedBackgroundInFrame:(CGRect)frame
{
    NSInteger arrowSize;
    
    if(IS_IPAD){
        arrowSize = 17;
    }else{
        arrowSize = 13;
    }
    CGPoint point1 = CGPointMake(0,frame.size.height/2);
    CGPoint point2 = CGPointMake(frame.size.height/2, 0);
    CGPoint point3 = CGPointMake(frame.size.width, 0);
    CGPoint point4 = CGPointMake(frame.size.width, frame.size.height);
    CGPoint point5 = CGPointMake(frame.size.height/2, frame.size.height);
    
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:point1];
    [linePath addLineToPoint:point2];
    [linePath addLineToPoint:point3];
    [linePath addLineToPoint:point4];
    [linePath addLineToPoint:point5];
    [linePath addLineToPoint:point1];
    
    line.lineWidth = 1.5f;
    line.path=linePath.CGPath;
    line.fillColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:0.50].CGColor;
    return line;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    eventDetails = [searchResults objectAtIndex:indexPath.row];
    PersonalDetailViewController *hsVC=[[PersonalDetailViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"PersonalDetailViewController"] bundle:nil];
    hsVC.playerDetails = eventDetails;
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)indexPath.row] forKey:UD_MyTeamListCurrentIndex];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    hsVC.isLogedInUsersDetail = FALSE;
    [self.navigationController pushViewController:hsVC animated:YES];

}

//#pragma mark - Swipe TableView Cell Implementation
//-(NSArray *) createRightButtons: (int) number
//{
//    NSMutableArray *result = [NSMutableArray array];
//    NSString *titles[2] = {@"INFORMATION", @"PERSONAL"};
//    
//    UIImage *delete = [UIImage imageNamed:@"information"];
//    UIImage *edit = [UIImage imageNamed:@"personalInformation"];
//    UIImage *icons[2] = {delete,edit};
//    NSDictionary *attrDict;
//    
//    if(IS_IPAD)
//    {
//        attrDict = @{
//                     NSFontAttributeName : [UIFont fontWithName:@"icomoon" size:32.0],
//                     NSForegroundColorAttributeName : [UIColor whiteColor]
//                     };
//        
//    }
//    else if (IS_IPHONE_6)
//    {
//        attrDict = @{
//                     NSFontAttributeName : [UIFont fontWithName:@"icomoon" size:20.0],
//                     NSForegroundColorAttributeName : [UIColor whiteColor]
//                     };
//    }
//    
//    UIColor *colors[2] = {[UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1], [UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1]};
//    
//    for (int i = 0; i < 2; ++i)
//    {
//        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
//                                  {
//                                      return YES;
//                                  }];
//        
//        CGRect frame = button.frame;
//        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
//        button.frame = frame;
//        if (IS_IPHONE_5)
//            [button.titleLabel setFont:app_font_bold_10];
//        
//        [result addObject:button];
//    }
//    return result;
//}
//
//#if USE_MG_DELEGATE
//-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
//             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
//{
//    
//    CGPoint buttonPosition = [cell.swipeContentView convertPoint:CGPointZero
//                                                          toView:self.tableviewMyTeamList];
//    NSIndexPath *indexPath = [self.tableviewMyTeamList indexPathForRowAtPoint:buttonPosition];
//    eventDetails = [searchResults objectAtIndex:indexPath.row];
//    
//    NSDictionary *dict = [searchResults objectAtIndex:indexPath.row];
//    bool IsPrimary =  [[dict valueForKey:@"IsPrimary"] boolValue];
//    
//    if (IsPrimary == true) {
//        
//        return nil;
//    }
//    TestData *data = [swipeCellButtons objectAtIndex:0];
//    swipeSettings.transition = data.transition;
//    
//    if (direction == MGSwipeDirectionLeftToRight)
//    {
//        expansionSettings.buttonIndex = data.leftExpandableIndex;
//        expansionSettings.fillOnTrigger = NO;
//        return 0;
//    }
//    else
//    {
//        expansionSettings.buttonIndex = data.rightExpandableIndex;
//        expansionSettings.fillOnTrigger = YES;
//        return [self createRightButtons:data.rightButtonsCount];
//    }
//}
//#endif
//
//-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
//{
////    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
////          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
//    
//    NSIndexPath *indexPath = [self.tableviewMyTeamList indexPathForCell:cell];
//    //NSLog(@"%@",indexPath);
//    eventDetails = [searchResults objectAtIndex:indexPath.row];
//    
//    if (index == 0) {
//        //NSLog(@"INFORMATION");
//        InformationListViewController *hsVC=[[InformationListViewController alloc]initWithNibName:@"InformationListViewController" bundle:nil];
//        hsVC.playerDetails = eventDetails;
//        hsVC.isLogedInUsersDetail = FALSE;
//        [self.navigationController pushViewController:hsVC animated:YES];
//        
//    } else {
//        //NSLog(@"PERSONAL");
//        PersonalDetailViewController *hsVC=[[PersonalDetailViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"PersonalDetailViewController"] bundle:nil];
//        hsVC.playerDetails = eventDetails;
//        
//        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",indexPath.row] forKey:UD_MyTeamListCurrentIndex];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        
//        hsVC.isLogedInUsersDetail = FALSE;
//        [self.navigationController pushViewController:hsVC animated:YES];
//        
//    }
//    
//    return YES;
//}

#pragma mark - OLMTextView Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollUp:textField];
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    [self scrollUp:textField];
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
}

#pragma mark - View Scrolling
-(void)scrollUp:(id)sender
{
    //NSLog(@"%f",self.viewSlide.frame.origin.x);
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    //NSLog(@"%f",self.viewSlide.frame.origin.x);
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

- (void)filterContentForSearchText:(NSMutableDictionary *)searchParam scope:(NSString*)scope
{
    // searchResults = [[NSMutableArray alloc]init];
    NSString *nameRange,*adminGroupRange,*sportRange,*SelectionRange,*functionRange;//*teamRange;
    nameRange = [searchParam valueForKey:@"Name"] ;
    adminGroupRange = [searchParam valueForKey:@"AdminGroup"];
    sportRange = [searchParam valueForKey:@"Sport"];
    //    teamRange = [searchParam valueForKey:@"Team"];
    SelectionRange = [searchParam valueForKey:@"SelectionStatus"];
    functionRange = [searchParam valueForKey:@"Function"];
    
    NSMutableArray *searchPredicates = [[NSMutableArray alloc] init];
    if(nameRange && [nameRange length] > 0)
    {
        NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"Name CONTAINS[cd] %@", nameRange];
        [searchPredicates addObject:namePredicate];
    }
    if(adminGroupRange && [adminGroupRange length] > 0)
    {
        NSPredicate *adminGroupPredicate = [NSPredicate predicateWithFormat:@"AdminGroup MATCHES[cd] %@", adminGroupRange];
        [searchPredicates addObject:adminGroupPredicate];
    }
    if(sportRange && [sportRange length] > 0)
    {
        NSPredicate *sportsPredicate = [NSPredicate predicateWithFormat:@"Sport MATCHES[cd] %@", sportRange];
        [searchPredicates addObject:sportsPredicate];
    }
    //    if(teamRange && [teamRange length] > 0)
    //    {
    //        NSPredicate *teamPredicate = [NSPredicate predicateWithFormat:@"NAME MATCHES[cd] %@", teamRange];
    //        [searchPredicates addObject:teamPredicate];
    //    }
    if(SelectionRange && [SelectionRange length] > 0)
    {
        NSPredicate *selectionPredicate = [NSPredicate predicateWithFormat:@"SelectionStatus MATCHES[cd] %@", SelectionRange];
        [searchPredicates addObject:selectionPredicate];
    }
    if(functionRange && [functionRange length] > 0)
    {
        NSPredicate *functionalPredicate = [NSPredicate predicateWithFormat:@"%K MATCHES[cd] %@",@"Function", functionRange];
        [searchPredicates addObject:functionalPredicate];
    }
    
    NSPredicate *finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:searchPredicates];
    //NSLog(@"%@",finalPredicate);
    [searchResults removeAllObjects];
    searchResults = [[NSMutableArray alloc] initWithArray:[arrayMyTeamList filteredArrayUsingPredicate:finalPredicate]];
    [self.tableviewMyTeamList reloadData];
}
@end

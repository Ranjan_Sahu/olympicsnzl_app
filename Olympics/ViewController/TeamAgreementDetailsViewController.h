//
//  TeamAgreementDetailsViewController.h
//  Olympics
//
//  Created by webwerks on 3/22/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMSingleCheckBox.h"
#import "AdhocCustomCell.h"
#import "OLMCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "OLMRadioButton.h"
#import "OLMDatePicker.h"
#import "Service.h"
#import "OLMRadioButtonTextKeyType.h"
#import "OLMSingleCheckBox.h"
#import "OLMTextView.h"
#import "Service.h"

@interface TeamAgreementDetailsViewController : UIViewController <Service_delegate,UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (nonatomic, assign) Service_Called serviceCalled;
@property (strong, nonatomic) IBOutlet UIWebView *agreementsWebView;
@property(nonatomic, strong) NSMutableArray *controlls;
@property(nonatomic, strong) NSMutableArray *contentViewControlls;
@property(nonatomic) NSInteger incrementedHeight;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;

@property (nonatomic, strong) NSMutableDictionary *agreementDict;
@property (nonatomic,strong) NSString *agreementHeader;
@property (nonatomic, strong) NSMutableDictionary *agreementResponseDict;

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

// Height Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewScrollContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeaderView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightAgreementWebView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewScrollContentViewTop;

@property (weak, nonatomic) IBOutlet UIWebView *headerWebView;


- (IBAction)DidSelectBackButton:(id)sender;
@end

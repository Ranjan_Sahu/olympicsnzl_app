//
//  EnterPinViewController.h
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterPinViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewUserPin;
@property (weak, nonatomic) IBOutlet UITextField *txtPin1;
@property (weak, nonatomic) IBOutlet UITextField *txtPin2;
@property (weak, nonatomic) IBOutlet UITextField *txtPin3;
@property (weak, nonatomic) IBOutlet UITextField *txtPin4;
@property (weak, nonatomic) IBOutlet UITextField *txtUserPin;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterPin;
@property (weak, nonatomic) IBOutlet UILabel *lblPinRecieved;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPIN;

- (IBAction)onClickLogin:(UIButton *)sender;
- (IBAction)DidSelectForgotPIN:(id)sender;

@end

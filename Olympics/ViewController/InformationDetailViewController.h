//
//  InformationDetailViewController
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationDetailViewController : UIViewController  <UIWebViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *vanigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogi;

@property (strong, nonatomic) NSMutableDictionary *infromationDetailDictionary;
@property (strong, nonatomic) NSMutableArray *fieldsToShowArray;
@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (nonatomic) UIView *viewInfoDetailsHeader;
@property (nonatomic) CGRect frameInfoDetailsHeader;

- (IBAction)DidSelectBackButton:(id)sender;


@end

//
//  InformationListingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "InformationListViewController.h"
#import "InformationListCustomCell.h"
#import "InformationDetailViewController.h"


#define USE_MG_DELEGATE 1

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface InformationListViewController (){
    
    NSMutableArray *swipeCellButtons, *controlls;
    NSDictionary *InformationDetails;
    NSMutableArray *arrayInformationList, *searchResults;
    NSArray *eventtypeToShowArray;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
    BOOL isSideMenuOpen;
    NSInteger incrementedHeight;
    NSArray *drpArrayCategoryTitle;
    CGFloat animatedDistance;
    
}
@end

@implementation InformationListViewController
@synthesize tableviewInformationList;

static NSString *CellIdentifier = @"InformationListCustomCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue90d"];
    [_searchButton setTitle:@"\ue92b" forState:UIControlStateNormal];
    self.tableviewInformationList.backgroundColor = color_white;

    if(IS_IPAD)
    {
        _slideViewWidth.constant = 480;
        [self.view layoutIfNeeded];
        [self.view updateConstraintsIfNeeded];
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableviewInformationList.estimatedRowHeight = 44;
    self.tableviewInformationList.rowHeight = UITableViewAutomaticDimension;
    [self.tableviewInformationList setNeedsLayout];
    [self.tableviewInformationList layoutIfNeeded];
    
    [self registerCellWithTableView:CellIdentifier];
    [self callServiceToGetInformationlist];
    
}
-(void)registerCellWithTableView:(NSString *)strName
{
    UINib *nib = [UINib nibWithNibName:strName bundle:nil];
    [self.tableviewInformationList registerNib:nib forCellReuseIdentifier:strName];
}

- (void)viewDidLayoutSubviews
{
    if (isSideMenuOpen)
    {
        CGRect frame = _viewSlide.frame;
        frame.origin.x = (self.view.bounds.size.width - _viewSlide.frame.size.width);
        _viewSlide.frame = frame;
    }
    else
    {
        CGRect frame = _viewSlide.frame;
        frame.origin.x = self.view.bounds.size.width;
        _viewSlide.frame = frame;
        tableviewInformationList.userInteractionEnabled = TRUE;
    }
    
}

- (IBAction)idiSelectSearchButton:(id)sender {
    
    tableviewInformationList.userInteractionEnabled = FALSE;
    if(isSideMenuOpen)
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^
         {
             CGRect frame = _viewSlide.frame;
             frame.origin.x = self.view.bounds.size.width;
             _viewSlide.frame = frame;
             tableviewInformationList.userInteractionEnabled = TRUE;
             
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"Completed");
             isSideMenuOpen=false;
             [self.view endEditing:YES];
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationOptionShowHideTransitionViews
                         animations:^
         {
             CGRect frame = _viewSlide.frame;
             frame.origin.x = (self.view.bounds.size.width - _viewSlide.frame.size.width);
             _viewSlide.frame = frame;
             
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"Completed");
             isSideMenuOpen=true;
             
         }];
        [UIView commitAnimations];
        
    }
    
}


- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.tableviewInformationList reloadData];
}

#pragma mark TouchEvents

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if((isSideMenuOpen)&&(![touch.view isKindOfClass:[UIScrollView class]]))
    {
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^
         {
             CGRect frame = _viewSlide.frame;
             frame.origin.x = self.view.bounds.size.width;
             _viewSlide.frame = frame;
             tableviewInformationList.userInteractionEnabled = TRUE;
             
         }
                         completion:^(BOOL finished)
         {
             //NSLog(@"Completed");
             isSideMenuOpen=false;
             [self.view endEditing:YES];
             
         }];
        
    }
    else
    {
        //NSLog(@"DO NOthing");
    }
    [UIView commitAnimations];
}


#pragma mark - Method to add questions on SlideView
-(void)StartAddingControllers {
    
    isSideMenuOpen=false;
    incrementedHeight = 10;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    CGRect buttonFrame;
    
    // Field_TextField.
    OLMTextField *controlObj1 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Name" fieldIdName:@"Name" questionId:101 IsRequired:NO defaultValue:@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_midBlack];
    controlObj1.questionTypeId = Field_TextField;
    controlObj1.questionLbl.textColor = color_white;
    controlObj1.textFieldView.textColor = color_white;
    controlObj1.tag = 1;
    [controlls addObject:controlObj1];
    [self.baseScrollView addSubview:controlObj1];
    incrementedHeight = incrementedHeight + controlObj1.frame.size.height + 5;
    
    // Field_DropDown.
    if(![drpArrayCategoryTitle isKindOfClass:[NSNull class]] && drpArrayCategoryTitle && [drpArrayCategoryTitle count]>0)
    {
        OLMDropDown *controlObj2 = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Category" fieldIdName:@"Category" questionId:102 Options:drpArrayCategoryTitle IsRequired:NO defaultValue:0 delegate:self lineColor:color_midBlack];
        controlObj2.questionTypeId = Field_DropdownList;
        controlObj2.questionLbl.textColor = color_white;
        controlObj2.optionTextfield.textColor = color_white;
        controlObj2.tag = 2;
        [controlls addObject:controlObj2];
        [self.baseScrollView addSubview:controlObj2];
        incrementedHeight = incrementedHeight + controlObj2.frame.size.height + 5;
        
        buttonFrame = controlObj2.frame;
        
    }
    
    // UIButton
    CGFloat btnHeight = 34;
    UIButton *applyButton = [[UIButton alloc] initWithFrame:CGRectMake(buttonFrame.origin.x, incrementedHeight+16,buttonFrame.size.width , btnHeight)];
    
    [applyButton.titleLabel setFont:app_font_bold_18];
    if(IS_IPAD) {
        btnHeight = 50;
        [applyButton.titleLabel setFont:app_font_bold_24];
    }
    [applyButton setBackgroundColor: color_white];
    [applyButton setTitle:@"APPLY" forState:UIControlStateNormal];
    [applyButton setTitleColor:[UIColor colorWithRed:0.0275 green:0.5216 blue:0.2157 alpha:1.0] forState:UIControlStateNormal];
    applyButton.layer.shadowOffset = CGSizeMake(1,1);
    applyButton.layer.shadowColor = [UIColor blackColor].CGColor;
    applyButton.layer.shadowOpacity = 0.3;
    applyButton.layer.shadowRadius = 1;
    [applyButton addTarget:self action:@selector(DidSelectApplyButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.baseScrollView addSubview:applyButton];
    incrementedHeight = incrementedHeight + applyButton.frame.size.height + 5;
    [applyButton setHidden:FALSE];
}


#pragma - mark WebService Call
-(void)callServiceToGetInformationlist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetInformationList withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        self.tableviewInformationList.estimatedRowHeight = 50;
        self.tableviewInformationList.rowHeight = UITableViewAutomaticDimension;
        [self.tableviewInformationList setNeedsLayout];
        [self.tableviewInformationList layoutIfNeeded];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            
            if(![[responseDictionery objectForKey:@"lstGridInformation"] isKindOfClass:[NSNull class]])
            {
                arrayInformationList = [[responseDictionery objectForKey:@"lstGridInformation"] mutableCopy];
                searchResults = [[NSMutableArray alloc] initWithArray:arrayInformationList];
            }
            drpArrayCategoryTitle = [[responseDictionery objectForKey:@"drpCategory"] mutableCopy];
            [self StartAddingControllers];
            
            // add shadow effect to searchView
            _searchImageView.layer.masksToBounds = NO;
            _searchImageView.layer.shadowRadius = 2;
            _searchImageView.layer.shadowColor = [UIColor blackColor].CGColor;
            _searchImageView.layer.shadowOpacity = 0.5;
            CGFloat indent = 1.5;
            CGRect innerRect = CGRectMake(indent,indent,_searchImageView.frame.size.width-2*indent,_searchImageView.frame.size.height-2*indent);
            _searchImageView.layer.shadowPath = [UIBezierPath bezierPathWithRect:innerRect].CGPath;
            [self.baseScrollView setNeedsDisplay];
            
            if([searchResults count] > 0) {
                
                [self.tableviewInformationList reloadData];
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

-(NSMutableDictionary *)saveEnteredData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [controlls count]; i++)
    {
        if([[controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
            }
        }
    }
    return requestParameters;
}


-(void)DidSelectApplyButton:(UIButton *)sender
{
    NSMutableDictionary *searchParam =  [self saveEnteredData];
    NSMutableDictionary *searchData = [NSMutableDictionary dictionaryWithObject:[searchParam valueForKey:@"Name"] forKey:@"Name"];
    
    if(!([[searchParam valueForKey:@"Category"]integerValue]==0))
    {
        for(int i = 0;i<drpArrayCategoryTitle.count;i++)
        {
            if([[[[drpArrayCategoryTitle objectAtIndex:i] valueForKey:@"Key"] stringValue]isEqualToString:[[searchParam valueForKey:@"Category"] stringValue]])
            {
                NSDictionary *answer = @{@"Category":[[drpArrayCategoryTitle objectAtIndex:i] valueForKey:@"Value"]};
                [searchData addEntriesFromDictionary:answer];
                break;
            }
            else
            {
                //NSLog(@"Data not Found");
            }
        }
        
    }
    
    [self filterContentForSearchText:searchData scope:@"ALL"];
}

- (void)filterContentForSearchText:(NSMutableDictionary *)searchParam scope:(NSString*)scope
{
    // searchResults = [[NSMutableArray alloc]init];
    NSString *nameRange,*categoryGroupRange;
    nameRange = [searchParam valueForKey:@"Name"] ;
    categoryGroupRange = [searchParam valueForKey:@"Category"];
    
    
    NSMutableArray *searchPredicates = [[NSMutableArray alloc] init];
    if(nameRange && [nameRange length] > 0)
    {
        NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"ANY lstKeyword.keyword CONTAINS[cd] %@", nameRange];
        
        //NSPredicate *namePredicate = [NSPredicate predicateWithFormat:@"TopicTitle CONTAINS[cd] %@", nameRange];
        [searchPredicates addObject:namePredicate];
    }
    if(categoryGroupRange && [categoryGroupRange length] > 0)
    {
        NSPredicate *categoryGroupPredicate = [NSPredicate predicateWithFormat:@"CategoryTitle MATCHES[cd] %@", categoryGroupRange];
        [searchPredicates addObject:categoryGroupPredicate];
    }
    NSPredicate *finalPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:searchPredicates];
    //NSLog(@"%@",finalPredicate);
    
    [searchResults removeAllObjects];
    searchResults = [[NSMutableArray alloc] initWithArray:[arrayInformationList filteredArrayUsingPredicate:finalPredicate]];
    [tableviewInformationList reloadData];
}

/**************************** TABLEVIEW Functions ****************************************************/
/********************************************************************************************************/
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return searchResults.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(IS_IPAD){
//        return 124;
//    }
//    else{
//        return 94;
//    }
//    
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //InformationListCustomCell *cell = (InformationListCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    InformationListCustomCell *cell = nil;
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InformationListCustomCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dict = [searchResults objectAtIndex:indexPath.row];
    
    cell.lblEventNameText.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"TopicTitle"]];
    cell.lblFieldText.text = [NSString stringWithFormat:@"%@",[[dict valueForKey:@"TopicBlurb"] isKindOfClass:[NSNull class]]?@" ":[dict valueForKey:@"TopicBlurb"]];
    
    //cell.DateLabel.text = [COMMON_SETTINGS getDateOnlyString:[dict valueForKey:@"CreatedDate"]];
    cell.DateLabel.text = [dict valueForKey:@"CreatedDate"];
    
    [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
    [cell.btnFormSubmitted setTitle:[dict valueForKey:@"CategoryTitle"] forState:UIControlStateNormal];
    
    NSString *formSubmittedString = [cell.btnFormSubmitted.currentTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGFloat buttonWidth = [COMMON_SETTINGS getWidthForText:formSubmittedString withFont:cell.btnFormSubmitted.titleLabel.font andHeight:cell.btnFormSubmitted.frame.size.height minWidth:30];
    cell.btnFormSubmittedWidth.constant = buttonWidth + 15;
    [cell.btnFormSubmitted layoutIfNeeded];
    
    //CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:cell.lblFieldText.text  withFont:cell.lblFieldText.font andWidth:cell.lblFieldText.frame.size.width minHeight:22];
    //if(descriptionHeight > 45)
    //   descriptionHeight = 45;
    //cell.lblFieldTextHeightConstraint .constant = descriptionHeight;
    //[cell.lblFieldText layoutIfNeeded];
    
    
    for (CALayer *layer in cell.viewFormBack.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    CAShapeLayer *labelView = [self GetLableForFormSubmitedBackgroundInFrame:cell.btnFormSubmitted.frame];
    labelView.fillColor = [UIColor colorWithRed:0.1333 green:0.6118 blue:0.2784 alpha:1.0].CGColor;
    [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
    [cell.viewFormBack.layer addSublayer:labelView];
    
    return cell;
}

-(CAShapeLayer*)GetLableForFormSubmitedBackgroundInFrame:(CGRect)frame
{
    NSInteger arrowSize;
    
    if(IS_IPAD){
        arrowSize = 17;
    }else{
        arrowSize = 13;
    }
    //    CGPoint point1 = CGPointMake(0,frame.size.height/2);
    //    CGPoint point2 = CGPointMake(frame.size.height/2, 0);
    //    CGPoint point3 = CGPointMake(frame.size.width, 0);
    //    CGPoint point4 = CGPointMake(frame.size.width, frame.size.height);
    //    CGPoint point5 = CGPointMake(frame.size.height/2, frame.size.height);
    
    CGPoint point1 = CGPointMake(0,0);
    CGPoint point2 = CGPointMake(frame.size.width, 0);
    CGPoint point3 = CGPointMake((frame.size.width+frame.size.height/2), frame.size.height/2);
    CGPoint point4 = CGPointMake(frame.size.width, frame.size.height);
    CGPoint point5 = CGPointMake(0, frame.size.height);
    
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:point1];
    [linePath addLineToPoint:point2];
    [linePath addLineToPoint:point3];
    [linePath addLineToPoint:point4];
    [linePath addLineToPoint:point5];
    [linePath addLineToPoint:point1];
    
    line.lineWidth = 1.5f;
    line.path=linePath.CGPath;
    line.fillColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:0.50].CGColor;
    return line;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    InformationDetailViewController *infoDetails = [[InformationDetailViewController alloc]initWithNibName:@"InformationDetailViewController" bundle:nil];
    
    //Show, User Wants to add details or edit details.
    infoDetails.infromationDetailDictionary = [searchResults objectAtIndex:indexPath.row];
    
    InformationListCustomCell *cell = [self.tableviewInformationList cellForRowAtIndexPath:indexPath];
    
    CGRect cellFrame = cell.contentView.frame;
    cellFrame.size.height = cellFrame.size.height - cell.lblFieldText.frame.size.height;
    cell.contentView.frame = cellFrame;
    cell.lblFieldText.text = @"";
    
    UIView *viewHeader = [[UIView alloc]init];
    viewHeader = cell.contentView;
    viewHeader.frame = cellFrame;
    infoDetails.viewInfoDetailsHeader = viewHeader;
    infoDetails.frameInfoDetailsHeader = cellFrame;
    
    [self.navigationController pushViewController:infoDetails animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - OLMTextView Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollUp:textField];
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    [self scrollUp:textField];
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
}


#pragma mark - View Scrolling
-(void)scrollUp:(id)sender
{
    //NSLog(@"%f",self.viewSlide.frame.origin.x);
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    //NSLog(@"%f",self.viewSlide.frame.origin.x);
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}


/***********************************************************************/

//-(NSArray *) createRightButtons: (int) number
//{
//    NSMutableArray *result = [NSMutableArray array];
//    NSString *titles[2] = {@"DELETE", @"EDIT"};
//
//    UIImage *delete = [UIImage imageNamed:@"delete"];
//    UIImage *edit = [UIImage imageNamed:@"edit"];
//    UIImage *icons[2] = {delete,edit};
//
//    UIColor *colors[2] = {[UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1], [UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1]};
//
//    for (int i = 0; i < 2; ++i)
//    {
//        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
//                                  {
//                                      return YES;
//                                  }];
//
//        CGRect frame = button.frame;
//        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
//        button.frame = frame;
//        [result addObject:button];
//    }
//    return result;
//}

//#if USE_MG_DELEGATE
//-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
//             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
//{
//
//    CGPoint buttonPosition = [cell.swipeContentView convertPoint:CGPointZero
//                                                          toView:self.tableviewInformationList];
//    NSIndexPath *indexPath = [self.tableviewInformationList indexPathForRowAtPoint:buttonPosition];
//    eventDetails = [arrayInformationList objectAtIndex:indexPath.row];
//
//    NSDictionary *dict = [arrayInformationList objectAtIndex:indexPath.row];
//    bool IsPrimary =  [[dict valueForKey:@"IsPrimary"] boolValue];
//
//    if (IsPrimary == true) {
//
//        return nil;
//    }
//    TestData *data = [swipeCellButtons objectAtIndex:0];
//    swipeSettings.transition = data.transition;
//
//    if (direction == MGSwipeDirectionLeftToRight)
//    {
//        expansionSettings.buttonIndex = data.leftExpandableIndex;
//        expansionSettings.fillOnTrigger = NO;
//        return 0;
//    }
//    else
//    {
//        expansionSettings.buttonIndex = data.rightExpandableIndex;
//        expansionSettings.fillOnTrigger = YES;
//        return [self createRightButtons:data.rightButtonsCount];
//    }
//}
//#endif
//
//-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
//{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
//
//
//    NSIndexPath *indexPath = [self.tableviewInformationList indexPathForCell:cell];
      //NSLog(@"%@",indexPath);
//    eventDetails = [arrayInformationList objectAtIndex:indexPath.row];
//
//
//    if (index == 0) {
//        // Show popup.
//
//        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
//                                                                           message:@"Are you sure you want the Delete Your Email Details?"
//                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
//            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
//                                                                       style:(UIAlertActionStyleDefault)
//
//                                                                     handler:^(UIAlertAction * _Nonnull action) {
//
//                                                                         //Is User Wants to delete Email Details Then Continiue.
//                                                                         NSLog(@"Delete");
//                                                                         currentIndexPathToDelete = indexPath;
//                                                                         [self callServiceDeleteInformation];
//
//                                                                     }];
//            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
//                                                                      style:(UIAlertActionStyleCancel)
//                                                                    handler:^(UIAlertAction * _Nonnull action) {
//                                                                        //
//                                                                    }];
//            [alert addAction:alert_yes_action];
//            [alert addAction:alert_no_action];
//            [self presentViewController:alert animated:YES completion:nil];
//        } else {
//            // code to be written for version lower than ios 8.0...
//        }
//        //------------------------------------------------------------------------
//
//    } else {
//
//        [self callServiceEditEvent];
//    }
//
//    return YES;
//}

@end

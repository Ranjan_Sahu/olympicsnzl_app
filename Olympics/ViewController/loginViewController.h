//
//  ViewController.h
//  Olympics
//
//  Created by webwerks on 05/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GooglePlus/GooglePlus.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "Constant.h"
#import "Service.h"
@class GIDSignInButton;

@interface loginViewController : UIViewController<UITextFieldDelegate,Service_delegate,UIWebViewDelegate,GIDSignInDelegate, GIDSignInUIDelegate,UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *LBLloginText;
@property (weak, nonatomic) IBOutlet UITextField *TFUserName;
@property (weak, nonatomic) IBOutlet UITextField *TFPassword;
@property (weak, nonatomic) IBOutlet UIButton *BTNLogin;
@property (weak, nonatomic) IBOutlet UIButton *BTNForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *BTNFacebook;
@property (weak, nonatomic) IBOutlet GIDSignInButton *BTNGmail;
@property (weak, nonatomic) IBOutlet UIButton *BTNTweeter;

@property (weak, nonatomic) IBOutlet UIImageView *IMGLogo;
@property (weak, nonatomic) IBOutlet UIView *ViewSeperator;
@property (nonatomic, retain) NSString *isLogin;

- (IBAction)DidSelectLoginButton:(id)sender;
- (IBAction)DidSelectForgotPassword:(id)sender;
- (IBAction)DidSelectFacebookLogin:(id)sender;
- (IBAction)DidSelectGmailLogin:(id)sender;
- (IBAction)DidSelectTweeterLogin:(id)sender;


@end


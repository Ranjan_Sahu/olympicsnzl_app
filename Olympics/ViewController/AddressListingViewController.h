//
//  AddressListingViewController.h
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "AddAddressListViewController.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "addressListTableViewCell.h"
#import "FooterCustomCell.h"

@interface AddressListingViewController : UIViewController<UITableViewDataSource,MGSwipeTableCellDelegate, Service_delegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (strong, nonatomic) IBOutlet UITableView *tableviewAddreslist;
@property(nonatomic,assign) Service_Called serviceCalled;
@property (nonatomic, assign) BOOL isNeedToRefresh;
@property (nonatomic, assign) BOOL isFirstTime;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;



@end

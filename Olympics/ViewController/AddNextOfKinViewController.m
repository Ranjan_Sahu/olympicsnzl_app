//
//  AddEventViewController.m
//  Olympics
//
//  Created by webwerks2 on 7/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AddNextOfKinViewController.h"
#import "NextOfKinListViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface AddNextOfKinViewController ()
{
    CGFloat animatedDistance;
    NSMutableArray *eventsDropDownArray;
    NSString *previousText, *currentText;
}

@end

@implementation AddNextOfKinViewController
@synthesize incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirstTime = true;
    
    // Do any additional setup after loading the view from its nib.
    _titleLabel.font = app_font_bold_15;
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue911"];
    self.isChangesMade = FALSE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_isFirstTime)
    {
        [self StartAddingControllers];
        _isFirstTime = false;
    }
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if ([self.owner class]==[NextOfKinListViewController class]) {
        [(NextOfKinListViewController *)self.owner setIsNeedToRefresh:FALSE];
    }
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    
    
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

#pragma - mark WebService Call
-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    
    if(_serviceCalled == Service_Called_Edit){
        
        [userinformation setValue:[NSString stringWithFormat:@"%@",[_nextOfKinDetails valueForKey:@"NextOfKinId"]] forKey:@"NextOfKinId"];
    }
    else if (_serviceCalled == Service_Called_Add){
        
        [userinformation setValue:@0 forKey:@"NextOfKinId"];
        
    }
    else{
        
        //NSLog(@"Not Valid type found!!");
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateNextOfKinAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
    {
        
        if ([self.owner class]==[NextOfKinListViewController class]){
            [(NextOfKinListViewController *)self.owner setIsNeedToRefresh:TRUE];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:@"" Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"" Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers {
    
    //Field_DropdownList.
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    
    // Field_TextField.
    OLMTextField *controlObj1 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"First Name" fieldIdName:@"FirstName" questionId:101 IsRequired:YES defaultValue:(_serviceCalled == Service_Called_Edit) ?(_nextOfKinDetails[@"FirstName"]?_nextOfKinDetails[@"FirstName"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
    controlObj1.questionTypeId = Field_TextField;
    controlObj1.tag = 1;
    [controlls addObject:controlObj1];
    [self.baseScrollView addSubview:controlObj1];
    incrementedHeight = incrementedHeight + controlObj1.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj2 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Last Name" fieldIdName:@"LastName" questionId:102 IsRequired:YES defaultValue:(_serviceCalled == Service_Called_Edit) ?(_nextOfKinDetails[@"LastName"]?_nextOfKinDetails[@"LastName"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
    controlObj2.questionTypeId = Field_TextField;
    controlObj2.tag = 2;
    [controlls addObject:controlObj2];
    [self.baseScrollView addSubview:controlObj2];
    incrementedHeight = incrementedHeight + controlObj2.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj3 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Relationship" fieldIdName:@"Relationship" questionId:103 IsRequired:NO defaultValue:(_serviceCalled == Service_Called_Edit) ?(_nextOfKinDetails[@"Relationship"]?_nextOfKinDetails[@"Relationship"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
    controlObj3.questionTypeId = Field_TextField;
    controlObj3.tag = 3;
    [controlls addObject:controlObj3];
    [self.baseScrollView addSubview:controlObj3];
    incrementedHeight = incrementedHeight + controlObj3.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj4 = [[OLMTextField alloc] initWithFrame:CGRectMake(10,  incrementedHeight, frame.size.width-20, 1) Question:@"Contact Details" fieldIdName:@"ContactDetails" questionId:104 IsRequired:NO defaultValue:(_serviceCalled == Service_Called_Edit) ? (_nextOfKinDetails[@"ContactDetails"]?_nextOfKinDetails[@"ContactDetails"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
    controlObj4.questionTypeId = Field_TextField;
    controlObj4.tag = 4;
    [controlls addObject:controlObj4];
    [self.baseScrollView addSubview:controlObj4];
    incrementedHeight = incrementedHeight + controlObj4.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *controlObj5 = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Note" fieldIdName:@"Note" questionId:105 IsRequired:NO defaultValue:(_serviceCalled == Service_Called_Edit) ? (_nextOfKinDetails[@"Note"]?_nextOfKinDetails[@"Note"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
    controlObj5.questionTypeId = Field_TextField;
    controlObj5.tag = 5;
    [controlls addObject:controlObj5];
    [self.baseScrollView addSubview:controlObj5];
    incrementedHeight = incrementedHeight + controlObj5.frame.size.height + 5;
    
    self.isChangesMade = FALSE;
}

#pragma mark - OLMTextView Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end

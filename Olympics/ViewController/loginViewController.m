//
//  ViewController.m
//  Olympics
//
//  Created by webwerks on 05/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "loginViewController.h"
#import "HomeScreenViewController.h"
#import "EnterPinViewController.h"
//#import <GoogleOpenSource/GoogleOpenSource.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "OAuthConsumer.h"

//Google
//static NSString * const keyClientIdGoogle = @"743649508352-bh0empqf8ffdmur6u75jgu9mfpasbvvf.apps.googleusercontent.com";


////LinkedIN
//static NSString * const keyClientIdLinkedIn = @"81k0c5jvb8eeva";
//static NSString * const keyClientSecretLinkedIn = @"XIifhJGZK9sOi2cM";
//static NSString * const authorizationEndPointLinkedIn = @"https://www.linkedin.com/oauth/v2/authorization";
//static NSString * const accessTokenEndPointLinkedIn = @"https://www.linkedin.com/oauth/v2/accessToken";

//Twitter
static NSString * const keyClientIdTwitter = @"lOZFJ97MTYtdPkK3OOMxN9xJr";
static NSString * const keyClientSecretTwitter = @"uTEM3CNdKfP5F8Tq5yjkfObNqgEPHi1Pl2AgtF3wQswrtenxiz";
static NSString * const callbackTwitter = @"http://example.com/auth/twitter/callback/";
static NSString * const requestTokenUrlTwitter = @"https://api.twitter.com/oauth/request_token";
static NSString * const authorizationUrlTwitter = @"https://api.twitter.com/oauth/authorize";
static NSString * const accessTokenUrlTwitter = @"https://api.twitter.com/oauth/access_token";

@interface loginViewController ()
{
    UIView *otherSocialLoginView;
    UIWebView *webView;
    NSString *linkedInAcessCode,*linkedInAccessToken,*socialLoginEmail;
    //GPPSignIn *signIn;
    OAConsumer *consumerTwitter;
    OAToken *requestTokenTwitter,*accessTokenTwitter;
    Service_Called _serviceCalled;
}
@end

@implementation loginViewController

#pragma - mark View Controller Life Cycle


- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignInButton class];
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;

    [GIDSignIn sharedInstance].uiDelegate = self;
    
    _TFUserName.layer.borderWidth = 0.5;
    _TFPassword.layer.borderWidth = 0.5;
    _TFUserName.layer.borderColor = [UIColor colorWithRed:0.8980 green:0.8980 blue:0.8980 alpha:1.0].CGColor;
    
    _TFPassword.layer.borderColor = [UIColor colorWithRed:0.8980 green:0.8980 blue:0.8980 alpha:1.0].CGColor;

    // Add Tap gesture to dismiss editing when tap on view
    UITapGestureRecognizer *tapOnBkView =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnBackGroundView)];
    [self.view addGestureRecognizer:tapOnBkView];
    
    tapOnBkView.cancelsTouchesInView = false;

   // [self initialMethodForGoogleSignIn];
    [self SetTextFieldsPadding];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
    _TFUserName.text = @"";
    
    _TFPassword.text = @"";
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_UserSecretPin];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_UserInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)viewDidLayoutSubviews
{
    [self SetFontForSubviews];
}

#pragma - mark Set Up subviews

-(void)SetTextFieldsPadding
{
//    _TFPassword.backgroundColor = [UIColor redColor];
    [APP_DELEGATE SetLeftPaddingFor:_TFUserName width:30 imageIfAny:[UIImage imageNamed:@"User_Placeholder"]];
    [APP_DELEGATE SetLeftPaddingFor:_TFPassword width:30 imageIfAny:[UIImage imageNamed:@"Lock"]];
}

-(void)SetFontForSubviews
{
    //    [_TFUserName setFont:app_font15];
    //    [_TFPassword setFont:app_font15];
    //    [_BTNForgotPassword.titleLabel setFont:app_font15];
    //    [_LBLloginText setFont:app_font_bold_24];
    //    [_BTNLogin.titleLabel setFont:app_font_bold_18];

    [_BTNFacebook setTitle:@"\ue91c" forState:UIControlStateNormal];
    _BTNFacebook.layer.cornerRadius = _BTNFacebook.frame.size.width/2;
    _BTNFacebook.clipsToBounds = YES;
  //  [_BTNGmail setTitle:@"\ue91f" forState:UIControlStateNormal];
    _BTNGmail.style = kGIDSignInButtonStyleIconOnly;
    _BTNGmail.layer.cornerRadius = _BTNGmail.frame.size.width/5;
    _BTNGmail.clipsToBounds = YES;
    _BTNGmail.layer.masksToBounds = YES;
    
    [_BTNTweeter setTitle:@"\ue92a" forState:UIControlStateNormal];
    _BTNTweeter.layer.cornerRadius = _BTNTweeter.frame.size.width/2;
    _BTNTweeter.clipsToBounds = YES;
}

#pragma - mark UITextField Deleget

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return TRUE;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return TRUE;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return TRUE;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

#pragma - mark Tap Gesture method
-(void)tapOnBackGroundView
{
    [self.view endEditing:TRUE];
}

#pragma - mark IBActionMethods

- (IBAction)DidSelectLoginButton:(id)sender {
    
    [self.view endEditing:TRUE];
    
    if([_TFUserName.text length] == 0)
    {
        //NSLog(@"InvalidUserName");
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Enter username" CancelButtonTitle:@"Ok" InView:self];
    }
    else if ([_TFPassword.text length] == 0)
    {
        //NSLog(@"InvalidPassword");
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Enter password" CancelButtonTitle:@"Ok" InView:self];
    }
    else
    {
        [COMMON_SETTINGS showHUD];
        NSDictionary *parameters = @{ @"UserName": _TFUserName.text,
                                      @"Password": _TFPassword.text};
        
        Service *callService = [[Service alloc] init];
        callService.delegate = self;
        [callService requestingURLString:weburl Service:Service_AccountDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
        _serviceCalled = Service_Called_Login;
    }
}

-(void)socialLoginSuccessCall:(NSString *)emailID :(int)typeID
{
    [COMMON_SETTINGS showHUD];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:emailID forKey:@"Email"];
    [parameters setValue:[NSNumber numberWithInt:typeID] forKey:@"SocialType"];
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_SocialLoginApi withParameters:parameters ContactID:@""];
    _serviceCalled = Service_Called_SocialLogin;
}

- (IBAction)DidSelectForgotPassword:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please provide following details to recover your password"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button CANCEL");
    }];
    [alert addAction:cancelAction];
    
    UIAlertAction *continueAction = [UIAlertAction actionWithTitle:@"CONTINUE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        //NSLog(@"You pressed button CONTINUE");
        //ForgotPassword
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setObject:[[alert textFields] objectAtIndex:0].text forKey:@"FirstName"];
        [parameters setObject:[[alert textFields] objectAtIndex:1].text forKey:@"LastName"];
        [parameters setObject:[[alert textFields] objectAtIndex:2].text forKey:@"EmailId"];
        
        [self CallServiceForgotPasswordWithParameters:parameters];
    }];
    [alert addAction:continueAction];
    continueAction.enabled = FALSE;
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"First Name";
        textField.keyboardType = UIKeyboardTypeDefault;
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
            
            if(([[alert textFields] objectAtIndex:0].text.length != 0)&&([[alert textFields] objectAtIndex:1].text.length != 0)&&([[alert textFields] objectAtIndex:2].text.length != 0))
                continueAction.enabled = TRUE;
            else
                continueAction.enabled = FALSE;
        }];
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Last Name";
        textField.keyboardType = UIKeyboardTypeDefault;
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
            
            if(([[alert textFields] objectAtIndex:0].text.length != 0)&&([[alert textFields] objectAtIndex:1].text.length != 0)&&([[alert textFields] objectAtIndex:2].text.length != 0))
                continueAction.enabled = TRUE;
            else
                continueAction.enabled = FALSE;
        }];
        
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email Address";
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification object:textField queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
            
            if(([[alert textFields] objectAtIndex:0].text.length != 0)&&([[alert textFields] objectAtIndex:1].text.length != 0)&&([[alert textFields] objectAtIndex:2].text.length != 0)&&([COMMON_SETTINGS validateEmailWithString:[[alert textFields] objectAtIndex:2].text]))
                continueAction.enabled = TRUE;
            else
                continueAction.enabled = FALSE;
        }];
        
    }];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)CallServiceForgotPasswordWithParameters:(NSMutableDictionary*)parameters
{
    [COMMON_SETTINGS showHUD];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_ForgotPassword withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_ForgotPassword;
}

#pragma - mark Service Deleget
-(void)Service_Error:(NSError *)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if(_serviceCalled == Service_Called_ForgotPassword)
    {
        if([[jsonResponse valueForKey:@"Status"]intValue] == 1)
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[jsonResponse valueForKey:@"Message"] CancelButtonTitle:@"Ok" InView:self];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[jsonResponse valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_Login || _serviceCalled == Service_Called_SocialLogin)
    {
        if([[jsonResponse valueForKey:@"Status"]intValue] == 1)
        {
            NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"ContactID"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"ContactID"] forKey:@"ContactID"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"GamesId"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"GamesId"] forKey:@"GamesId"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"IsMyTeamVisible"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"IsMyTeamVisible"] forKey:@"IsMyTeamVisible"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"LId"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"LId"] forKey:@"LId"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"Password"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"Password"] forKey:@"Password"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"SiteId"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"SiteId"] forKey:@"SiteId"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"TenantId"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"TenantId"] forKey:@"TenantId"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"UserName"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"UserName"] forKey:@"UserName"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"UserID"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"UserID"] forKey:@"UserID"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"Discipline"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"Discipline"] forKey:@"Discipline"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"ThumbnailProfileImage"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"ThumbnailProfileImage"] forKey:@"ThumbnailProfileImage"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"FullSizeProfileImage"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"FullSizeProfileImage"] forKey:@"FullSizeProfileImage"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"LogoURL"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"LogoURL"] forKey:@"LogoURL"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"Phone"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"Phone"] forKey:@"Phone"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"FullName"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"FullName"] forKey:@"FullName"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"Email"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"Email"] forKey:@"Email"];
            ;
            
            [userInfo setValue:[[jsonResponse valueForKey:@"GameURL"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"GameURL"] forKey:@"GameURL"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"IsAgreement"] isKindOfClass:[NSNull class]]?@"0":[jsonResponse valueForKey:@"IsAgreement"] forKey:@"IsAgreement"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"IsInformation"] isKindOfClass:[NSNull class]]?@"0":[jsonResponse valueForKey:@"IsInformation"] forKey:@"IsInformation"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"IsMyTeamVisible"] isKindOfClass:[NSNull class]]?@"0":[jsonResponse valueForKey:@"IsMyTeamVisible"] forKey:@"IsMyTeamVisible"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"IsPersonal"] isKindOfClass:[NSNull class]]?@"1":@"1" forKey:@"IsPersonal"];
            
            [userInfo setValue:[[jsonResponse valueForKey:@"BasicAuthUid"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"BasicAuthUid"] forKey:@"BasicAuthUid"];
            
           // [userInfo setValue:[[jsonResponse valueForKey:@"IsConsent"] isKindOfClass:[NSNull class]]?@"":[jsonResponse valueForKey:@"IsConsent"] forKey:@"IsConsent"];
            
            [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:UD_UserInfo];
            
            EnterPinViewController *hsVC=[[EnterPinViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"EnterPinViewController"] bundle:nil];
            [self.navigationController pushViewController:hsVC animated:YES];
        }
        else
        {
            //Invalid Credentials
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[jsonResponse valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[jsonResponse valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:@"OLM" Message:@"No response received, Please try after some time." CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - SocialMedia Button Actions
- (IBAction)DidSelectFacebookLogin:(id)sender {
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    
    [loginManager setLoginBehavior:FBSDKLoginBehaviorWeb];
    [loginManager logOut];
    
    [loginManager
     logInWithReadPermissions: @[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         
         if (error) {
             //NSLog(@"Process error");
         } else if (result.isCancelled) {
             //NSLog(@"Cancelled");
         } else {
             //NSLog(@"Logged in");
             [self fetchUserInfoFromFacebook];
         }
     }];
}

- (IBAction)DidSelectGmailLogin:(id)sender {
    
    //NSLog(@"DidSelectGmailLogin");
  //  [signIn trySilentAuthentication];
}


- (IBAction)DidSelectTweeterLogin:(id)sender {
    
    [self callMethodForTweeterWebView];
    [self startAuthorizationForTwitter];
}



#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    if (error) {
        //NSLog(@"Failed to access useremail");
        return;
    }
    NSString *emailAddress = user.profile.email;
    [self socialLoginSuccessCall:emailAddress :Type_ID_Gmail];
    
}

- (void)signIn:(GIDSignIn *)signIn
        didDisconnectWithUser:(GIDGoogleUser *)user
        withError:(NSError *)error {
    if (error) {
        //NSLog(@"Failed to access useremail");
    } else {
        //_signInAuthStatus.text = [NSString stringWithFormat:@"Status: Disconnected"];
        
    }
}


- (void)presentSignInViewController:(UIViewController *)viewController {
    [[self navigationController] pushViewController:viewController animated:YES];
}

/*-(void)initialMethodForGoogleSignIn
{
    signIn = [GPPSignIn sharedInstance];
    signIn.shouldFetchGooglePlusUser = YES;
    signIn.shouldFetchGoogleUserEmail = YES;
    
    // "Initialize the Google+ client"
    signIn.clientID = keyClientIdGoogle;
    
    // "https://www.googleapis.com/auth/plus.login" scope
    signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    
    signIn.scopes = @[ @"profile"]; // "profile" scope
    signIn.delegate = self;
}

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth error: (NSError *) error {
    if(error)
    {
        //NSLog(@"Received error %@ and auth object %@",error, auth);
    }
    else
    {
        [self fetchUserInfoFromGmail];
    }
}

- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        [NSString stringWithFormat:@"Status: Failed to disconnect: %@", error];
    } else {
        [NSString stringWithFormat:@"Status: Disconnected"];
    }
}

-(void)fetchUserInfoFromGmail
{
    // GTLServicePlus instance to send a request to Google+.
    GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
    plusService.retryEnabled = YES;
    
    // Set a valid GTMOAuth2Authentication object as the authorizer.
    [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
    
    // Create a GTLQuery object to get the details of the user with the given user ID
    GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
    
    [plusService executeQuery:query
            completionHandler:^(GTLServiceTicket *ticket,
                                GTLPlusPerson *person,
                                NSError *error) {
                if (error) {
                    GTMLoggerError(@"Error: %@", error);
                } else {
                    //NSLog(@"About Me : %@",person.aboutMe);
                    //NSLog(@"Name : %@ ",person.name);
                    //NSLog(@"URL : %@ ",person.url);
                    //NSLog(@"User Data : %@ ",person.userData);
                    //NSLog(@"User Image : %@ ",person.image);
                    //NSLog(@"Birthday : %@ ",person.birthday);
                    socialLoginEmail = signIn.authentication.userEmail;
                    [signIn signOut];
                    [self socialLoginSuccessCall:socialLoginEmail :Type_ID_Gmail];
                }
            }];
}
*/

#pragma mark - FaceBook SignIn implementation
- (void)fetchUserInfoFromFacebook {
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name,first_name,last_name"}];
        
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                // Store User's Facebook data result in a dictionary
                NSDictionary *userData = (NSDictionary *)result;
                //NSLog(@"FB Data%@",userData);
                [FBSDKAccessToken refreshCurrentAccessToken:^(FBSDKGraphRequestConnection *connection, id resul1t, NSError *error)
                 {
                     
                 }];
                socialLoginEmail = [userData valueForKey:@"email"];
                [self socialLoginSuccessCall:socialLoginEmail :Type_ID_Facebook];
            }
        }];
    }
}

#pragma mark - LinkedIn SignIn implementation
-(void)callMethodForTweeterWebView
{
    otherSocialLoginView = [[UIView alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    [otherSocialLoginView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.40]];
    [self.view addSubview:otherSocialLoginView];
    CGRect frame = otherSocialLoginView.frame;
    frame.size.width = frame.size.width - 40;
    frame.size.height = frame.size.height - 60;
    
    webView = [[UIWebView alloc] initWithFrame:frame];
    webView.center = otherSocialLoginView.center;
    [otherSocialLoginView addSubview:webView];
    webView.delegate = self;
    
    [UIView animateKeyframesWithDuration:0.3 delay:0 options:UIViewKeyframeAnimationOptionAutoreverse animations:^{
        webView.frame = otherSocialLoginView.frame;
        webView.center = otherSocialLoginView.center;
    } completion:^(BOOL finished) {
        webView.frame = frame;
        webView.center = otherSocialLoginView.center;
        
        UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 20, 20)];
        closeButton.center = webView.frame.origin;
        [closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(DidSelectCloseLinkedInView:) forControlEvents:UIControlEventTouchUpInside];
        [closeButton setBackgroundColor:color_white];
        closeButton.layer.cornerRadius = 10;
        closeButton.clipsToBounds = YES;
        [otherSocialLoginView addSubview:closeButton];
        webView.layer.masksToBounds = NO;
        webView.layer.shadowRadius = 3;
        webView.layer.shadowColor = [UIColor blackColor].CGColor;
        webView.layer.shadowOpacity = 0.5;
        CGFloat indent = 1.5;
        CGRect innerRect = CGRectMake(indent,indent,webView.frame.size.width-2*indent,webView.frame.size.height-2*indent);
        webView.layer.shadowPath = [UIBezierPath bezierPathWithRect:innerRect].CGPath;
    }];
}

-(void)DidSelectCloseLinkedInView:(UIButton*)sender
{
    [self dismissLinkedInView];
}

-(void)dismissLinkedInView
{
    for (UIView *view in otherSocialLoginView.subviews) {
        [view removeFromSuperview];
    }
    [otherSocialLoginView removeFromSuperview];
}

//Using OAuth
/*
 -(void)startAuthorizationForLinkedIn
 {
 NSURLComponents *urlComponents = [NSURLComponents componentsWithString:authorizationEndPointLinkedIn];
 NSURLQueryItem *query1 = [NSURLQueryItem queryItemWithName:@"response_type" value:@"code"];
 NSURLQueryItem *query2 = [NSURLQueryItem queryItemWithName:@"client_id" value:keyClientIdLinkedIn];
 NSURLQueryItem *query3 = [NSURLQueryItem queryItemWithName:@"redirect_uri" value:@"https://com.appcoda.linkedin.oauth/oauth"];
 NSURLQueryItem *query4 = [NSURLQueryItem queryItemWithName:@"state" value:@"DCEeFWf45A53sdfKef424"];
 NSURLQueryItem *query5 = [NSURLQueryItem queryItemWithName:@"scope" value:@"r_basicprofile+r_emailaddress"];
 
 urlComponents.queryItems = @[query1,query2,query3,query4,query5];
 NSURL *url = urlComponents.URL;
 
 NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
 [request addValue:@"application/json" forHTTPHeaderField:@"content-type"];
 [request setHTTPMethod:@"GET"];
 
 [webView loadRequest:request];
 }
 
 -(void)requestForAcessToken:(NSString *)requestCode
 {
 NSString *code = requestCode;
 
 NSURLComponents *urlComponents = [NSURLComponents componentsWithString:accessTokenEndPointLinkedIn];
 NSURLQueryItem *query1 = [NSURLQueryItem queryItemWithName:@"grant_type" value:@"authorization_code"];
 NSURLQueryItem *query2 = [NSURLQueryItem queryItemWithName:@"code" value:code];
 NSURLQueryItem *query3 = [NSURLQueryItem queryItemWithName:@"redirect_uri" value:@"https://com.appcoda.linkedin.oauth/oauth"];
 NSURLQueryItem *query4 = [NSURLQueryItem queryItemWithName:@"client_id" value:keyClientIdLinkedIn];
 NSURLQueryItem *query5 = [NSURLQueryItem queryItemWithName:@"client_secret" value:keyClientSecretLinkedIn];
 
 urlComponents.queryItems = @[query1,query2,query3,query4,query5];
 NSURL *url = urlComponents.URL;
 
 NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
 [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
 [request setHTTPMethod:@"POST"];
 
 NSURLSession *session = [NSURLSession sharedSession];
 NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
 {
 NSString *serverResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
 
 if([[response valueForKey:@"statusCode"] integerValue] == 200)
 {
 NSData *serverData = [serverResponse dataUsingEncoding:NSUTF8StringEncoding];
 NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:serverData options:kNilOptions error:&error];
 
 NSLog(@"%@",responseDictionery);
 linkedInAccessToken = [responseDictionery valueForKey:@"access_token"];
 [self getUserInfoFromLinkedIn];
 dispatch_sync(dispatch_get_main_queue(), ^{
 [self dismissLinkedInView];
 });
 
 }
 
 }];
 [dataTask resume];
 
 }
 
 - (void)getUserInfoFromLinkedIn
 {
 NSString *targetUrlString = @"https://api.linkedin.com/v1/people/~:(id,location,first-name,last-name,email-address,picture-url)?format=json";
 NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:targetUrlString]];
 
 [request setHTTPMethod:@"GET"];
 [request addValue:[NSString stringWithFormat:@"Bearer\%@",linkedInAccessToken] forHTTPHeaderField:@"Authorization"];
 NSURLSession *session = [NSURLSession sharedSession];
 NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
 
 if([[response valueForKey:@"statusCode"] integerValue] == 200)
 {
 NSString *serverResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
 
 NSData *serverData = [serverResponse dataUsingEncoding:NSUTF8StringEncoding];
 NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:serverData options:kNilOptions error:&error];
 
 NSLog(@"%@",responseDictionery);
 socialLoginEmail = [responseDictionery valueForKey:@"emailAddress"];
 [self socialLoginSuccessCall:socialLoginEmail :@"typeLinkedInLogin"];
 }
 }];
 
 [dataTask resume];
 
 }
 
 -(NSMutableDictionary *)fetchParamFromUrl:(NSString *)urlStr {
 
 NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
 NSArray *urlComponents = [urlStr componentsSeparatedByString:@"&"];
 
 for (NSString *keyValuePair in urlComponents){
 NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
 NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
 NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
 
 [queryStringDictionary setObject:value forKey:key];
 }
 return queryStringDictionary;
 }
 
 #pragma mark - WebView Delegates
 -(void)webViewDidStartLoad:(UIWebView *)webView
 {
 NSLog(@"webViewDidStartLoad");
 }
 
 -(void)webViewDidFinishLoad:(UIWebView *)webView
 {
 NSLog(@"webViewDidFinishLoad");
 }
 
 -(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
 {
 NSLog(@"shouldStartLoadWithRequest : %@",request.URL);
 
 NSString *stringFromUrl = [request.URL absoluteString];
 NSString *urlString = [[[stringFromUrl componentsSeparatedByString:@"?"] objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
 
 NSMutableDictionary *paramDict = [self fetchParamFromUrl:urlString];
 
 linkedInAcessCode = [paramDict valueForKey:@"code"];
 if(linkedInAcessCode)
 {
 [self requestForAcessToken:linkedInAcessCode];
 }
 else if ([[paramDict valueForKey:@"error"]isEqualToString:@"user_cancelled_login"])
 {
 [self dismissLinkedInView];
 }
 else if ([[paramDict valueForKey:@"error"]isEqualToString:@"user_cancelled_authorize"])
 {
 [self dismissLinkedInView];
 }
 
 return YES;
 }
 */

#pragma mark - Twitter SignIn implementation
-(void)startAuthorizationForTwitter {
    consumerTwitter =[[OAConsumer alloc]initWithKey:keyClientIdTwitter secret:keyClientSecretTwitter];
    NSURL* requestTokenUrl = [NSURL URLWithString:@"https://api.twitter.com/oauth/request_token"];
    OAMutableURLRequest* requestTokenRequest = [[OAMutableURLRequest alloc] initWithURL:requestTokenUrl
                                                                               consumer:consumerTwitter
                                                                                  token:nil
                                                                                  realm:nil
                                                                      signatureProvider:nil];
    OARequestParameter* callbackParam = [[OARequestParameter alloc] initWithName:@"oauth_callback" value:callbackTwitter];
    [requestTokenRequest setHTTPMethod:@"POST"];
    [requestTokenRequest setParameters:[NSArray arrayWithObject:callbackParam]];
    OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
    [dataFetcher fetchDataWithRequest:requestTokenRequest
                             delegate:self
                    didFinishSelector:@selector(didReceiveRequestToken:data:)
                      didFailSelector:@selector(didFailOAuth:error:)];
}

- (void)didReceiveRequestToken:(OAServiceTicket*)ticket data:(NSData*)data{
    NSString* httpBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    requestTokenTwitter = [[OAToken alloc] initWithHTTPResponseBody:httpBody];
    
    NSURL* authorizeUrl = [NSURL URLWithString:authorizationUrlTwitter];
    OAMutableURLRequest* authorizeRequest = [[OAMutableURLRequest alloc] initWithURL:authorizeUrl
                                                                            consumer:nil
                                                                               token:nil
                                                                               realm:nil
                                                                   signatureProvider:nil];
    NSString* oauthToken = requestTokenTwitter.key;
    OARequestParameter* oauthTokenParam = [[OARequestParameter alloc] initWithName:@"oauth_token" value:oauthToken];
    [authorizeRequest setParameters:[NSArray arrayWithObject:oauthTokenParam]];
    
    [webView loadRequest:authorizeRequest];
}

- (void)didReceiveAccessToken:(OAServiceTicket*)ticket data:(NSData*)data {
    
    NSString* httpBody = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    accessTokenTwitter = [[OAToken alloc] initWithHTTPResponseBody:httpBody];
    
    if (accessTokenTwitter) {
        NSURL* userDataRequest = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
        OAMutableURLRequest* requestTokenRequest = [[OAMutableURLRequest alloc] initWithURL:userDataRequest
                                                                                   consumer:consumerTwitter
                                                                                      token:accessTokenTwitter
                                                                                      realm:nil
                                                                          signatureProvider:nil];
        
        OARequestParameter* verifierParam = [[OARequestParameter alloc] initWithName:@"include_email" value:@"true"];
        
        [requestTokenRequest setHTTPMethod:@"GET"];
        [requestTokenRequest setParameters:[NSArray arrayWithObjects:verifierParam,nil]];
        OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
        [dataFetcher fetchDataWithRequest:requestTokenRequest
                                 delegate:self
                        didFinishSelector:@selector(didReceiveuserdata:data:)
                          didFailSelector:@selector(didFailOAuth:error:)];
    }
    else {
        //NSLog(@" AccessToken not found");
    }
}

- (void)didReceiveuserdata:(OAServiceTicket*)ticket data:(NSData*)data {
    
    NSError* error;
    NSDictionary* userDataTwitter = [NSJSONSerialization JSONObjectWithData:data
                                                                    options:kNilOptions
                                                                      error:&error];
    //NSLog(@"%@",userDataTwitter);
    if([userDataTwitter valueForKey:@"email"]){
        socialLoginEmail = [userDataTwitter valueForKey:@"email"];
        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"];
        NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
        for (NSHTTPCookie *cookie in cookies)
        {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
        [self socialLoginSuccessCall:socialLoginEmail :Type_ID_Twitter];
    }
    else{
        //NSLog(@"Failed to access useremail");
    }
}

#pragma mark - WebView Delegates & DataSources
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *temp = [NSString stringWithFormat:@"%@",request];
    NSRange textRange = [[temp lowercaseString] rangeOfString:[callbackTwitter lowercaseString]];
    
    if(textRange.location != NSNotFound){
        // Extract oauth_verifier from URL query
        NSString* verifier = nil;
        NSArray* urlParams = [[[request URL] query] componentsSeparatedByString:@"&"];
        
        for (NSString* param in urlParams){
            NSArray* keyValue = [param componentsSeparatedByString:@"="];
            NSString* key = [keyValue objectAtIndex:0];
            
            if ([key isEqualToString:@"oauth_verifier"]){
                verifier = [keyValue objectAtIndex:1];
                break;
            }
        }
        
        if (verifier){
            NSURL* accessTokenUrl = [NSURL URLWithString:accessTokenUrlTwitter];
            OAMutableURLRequest* accessTokenRequest = [[OAMutableURLRequest alloc] initWithURL:accessTokenUrl consumer:consumerTwitter token:requestTokenTwitter realm:nil signatureProvider:nil];
            OARequestParameter* verifierParam = [[OARequestParameter alloc] initWithName:@"oauth_verifier" value:verifier];
            [accessTokenRequest setHTTPMethod:@"POST"];
            [accessTokenRequest setParameters:[NSArray arrayWithObject:verifierParam]];
            OADataFetcher* dataFetcher = [[OADataFetcher alloc] init];
            [dataFetcher fetchDataWithRequest:accessTokenRequest
                                     delegate:self
                            didFinishSelector:@selector(didReceiveAccessToken:data:)
                              didFailSelector:@selector(didFailOAuth:error:)];
        } else {
            //NSLog(@"User is not verified");
        }
        
        [self dismissLinkedInView];
        
        return NO;
    }
    
    return YES;
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    //NSLog(@"didFailLoadWithError");
}

- (void)didFailOAuth:(OAServiceTicket*)ticket error:(NSError*)error {
    // ERROR!
    //NSLog(@"didFailOAuth");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // [indicator stopAnimating];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    // [indicator stopAnimating];
}

@end

//
//  MyGamesViewController.m
//  Olympics
//
//  Created by webwerks on 11/14/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "MyGamesViewController.h"
#import "HomeScreenViewController.h"
#import "myGamesCollectionViewCell.h"
#import "DropDownCustomCell.h"
#import "MyGamesViewController.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"


@interface MyGamesViewController ()
{
    NSMutableArray *myGamesData, *imageUrlArray;
    int flag;
    CGPoint point1,point2,point3,point4;
    UIBezierPath *linePath;
    CAShapeLayer *line;
    NSTimer *notificationCountTimer;
    //BOOL *commonWealthStatusValue;
    //UIImage* image;
}
@end

@implementation MyGamesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [_btnDropDown setTitle:@"\ue921" forState:UIControlStateNormal];
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo]];
    _lblUserName.text = [userInfo valueForKey:@"FullName"];
    _messageCount = @"0";
    
    flag = 0;
    _imageUserPic.hidden = TRUE;
    
    [self CallServiceToGetMyGamesDetails];
    
    _dropDownBgImage.hidden = TRUE;
    imageUrlArray = [[NSMutableArray alloc]init];
    imageCache = [[NSCache alloc] init];
    
    _dropDownView.hidden = TRUE;
    _dropDownTableView.delegate = self;
    _dropDownTableView.dataSource = self;
    
    [self.gamesCollectionView registerNib:[UINib nibWithNibName:@"myGamesCollectionViewCell" bundle:nil] forCellWithReuseIdentifier: @"myGamesCollectionViewCell"];
    
    [self.gamesCollectionView registerNib:[UINib nibWithNibName:@"myGamesCollectionViewCellRight" bundle:nil] forCellWithReuseIdentifier: @"myGamesCollectionViewCellRight"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
    [self shouldShowDropDown:FALSE];
    self.btnDropDown.hidden = TRUE;
    
    notificationCountTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(callNotificationCount) userInfo:nil repeats:YES];
//    if(_isNeedToRefresh)
//    {
//        [self CallServiceTogetNotificationCount];
//    }
    //Set User Image from image Url
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo];
    NSString *thumbImage = [[NSUserDefaults standardUserDefaults] valueForKey:UD_ThumbnailProfileImage];
    
    if(![thumbImage isKindOfClass:[NSNull class]] && ![thumbImage isEqualToString:@""])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [_imageUserPic sd_setImageWithURL:[NSURL URLWithString:thumbImage] placeholderImage:[UIImage imageNamed:@"testImage"]];
        });
    }
//    dispatch_queue_t queue1 = dispatch_queue_create("a", DISPATCH_QUEUE_SERIAL);
//    dispatch_async(queue1, ^(void) {
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:thumbImage]];
//        UIImage* imageUser = [[UIImage alloc] initWithData:imageData];
//        if (imageUser) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                _imageUserPic.image = imageUser;
//            });
//        }
//    });
}

-(void)callNotificationCount
{
    [self CallServiceTogetNotificationCount];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _imageUserPic.hidden = FALSE;
    self.btnDropDown.hidden = FALSE;
    [self SetUpSubviews];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [notificationCountTimer invalidate];
}
#pragma - mark WebService Call
-(void)CallServiceToGetMyGamesDetails
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ };
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetMyGamesDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceTogetNotificationCount
{
    //[COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ };
    NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetNotificationCountDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[userinfo valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_NotificationCount;
}

-(void)CallServiceTogetGameDetails
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ };
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetMainMenuVisibilityList withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_Get_Game_Detail;
}

-(void)CallServiceToLogout
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{ };
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_Logout withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:@""];
    _serviceCalled = Service_Called_Logout;
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if (_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"GameList"] isKindOfClass:[NSNull class]])
            {
                
                myGamesData = [[NSMutableArray alloc]initWithArray:[responseDictionery objectForKey:@"GameList"]];
                _messageCount = [responseDictionery objectForKey:@"NotificationCount"];
                if([_messageCount isKindOfClass:[NSNull class]])
                    _messageCount = @"0";
                
                for(int i=0;i<myGamesData.count;i++)
                {
                    NSString *imageUrl = [[myGamesData objectAtIndex:i] valueForKey:@"TemplatePath"];
                    [imageUrlArray addObject:imageUrl];
                }
                
                if(myGamesData.count==1)
                {
                    NSString *selectedGameID = [[myGamesData objectAtIndex:0] valueForKey:@"GameID"];
                    [[NSUserDefaults standardUserDefaults] setObject:selectedGameID forKey:@"Game_ID"];
                    
                    self.commonWealthStatusValue = [[[myGamesData objectAtIndex:0] valueForKey:@"IsCommonwealth"] boolValue];
                    NSString *selectedGameUrlString = [[myGamesData objectAtIndex:0] valueForKey:@"TemplatePath"];
                    [[NSUserDefaults standardUserDefaults] setBool:[[[myGamesData objectAtIndex:0] valueForKey:@"IsCommonwealth"] boolValue] forKey:Game_Type];
                    
                    if(![selectedGameUrlString isKindOfClass:[NSNull class]]){
                        [[NSUserDefaults standardUserDefaults] setObject:selectedGameUrlString forKey:Game_ImageURL];
                    }
                    else{
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:Game_ImageURL];
                    }
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self CallServiceTogetGameDetails];
                    
                    /*
                    NSString *selectedGameID = [[myGamesData objectAtIndex:0] valueForKey:@"GameID"];
                    [[NSUserDefaults standardUserDefaults] setObject:selectedGameID forKey:Game_Id];
                    HomeScreenViewController *homeView = [[HomeScreenViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"HomeScreenViewController"] bundle:nil];
                    homeView.messageCount = self.messageCount;
                    APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
                    APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
                    [APP_DELEGATE.window makeKeyAndVisible];
                     */
                }
                else
                {
                    [self.gamesCollectionView reloadData];
                    [self.dropDownTableView reloadData];
                }
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_NotificationCount)
    {
        [COMMON_SETTINGS hideHUD];
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"ThumbnailProfileImage"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"ThumbnailProfileImage"] forKey:UD_ThumbnailProfileImage];
            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"ResourceID"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"ResourceID"] forKey:Resource_Id];
            [[NSUserDefaults standardUserDefaults] synchronize];

            _messageCount = [responseDictionery objectForKey:@"NotificationCount"];
            
            if([_messageCount isKindOfClass:[NSNull class]])
                _messageCount = @"0";

            [self.dropDownTableView reloadData];
        }
    }
    else if (_serviceCalled == Service_Called_Get_Game_Detail)
    {
        [COMMON_SETTINGS hideHUD];
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"IsConsent"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"IsConsent"]forKey:@"IS_CONSENT_GAME"];
        
        if(self.presentingViewController)
        {
            HomeScreenViewController *homeView = [[HomeScreenViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"HomeScreenViewController"] bundle:nil];
            homeView.messageCount = self.messageCount;
            homeView.commonWealthStatus = _commonWealthStatusValue;
            
            APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
            APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
            [APP_DELEGATE.window makeKeyAndVisible];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
        else
        {
            [UIView animateWithDuration:0.2 animations:^{
                
                //            cell.imgViewGameBanner.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
                
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.2 animations:^{
                    
                    //                cell.imgViewGameBanner.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
                    
                } completion:^(BOOL finished) {
                    
                    [UIView animateWithDuration:0.2 animations:^{
                        
                        //                    cell.imgViewGameBanner.transform = CGAffineTransformIdentity;
                        
                    } completion:^(BOOL finished) {
                        
                        HomeScreenViewController *homeView = [[HomeScreenViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"HomeScreenViewController"] bundle:nil];
                        homeView.messageCount = self.messageCount;
                        homeView.commonWealthStatus = _commonWealthStatusValue;
                        
                        APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
                        APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
                        [APP_DELEGATE.window makeKeyAndVisible];
                    }];
                    
                }];
            }];
        }
    }
    else if (_serviceCalled == Service_Called_Logout)
    {
        [COMMON_SETTINGS hideHUD];
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            [APP_DELEGATE RemoveUserDataAndSetRootViewController];
            NSLog(@"You have logged out");
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark Touch Implementations
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self shouldShowDropDown:FALSE];
}

#pragma - mark IBAction Implementation
- (IBAction)onClickDropDown:(id)sender
{
    if(flag == 0)
    {
        [self shouldShowDropDown:FALSE];
    }
    else if(flag == 1)
    {
        [self shouldShowDropDown:TRUE];
    }
}

#pragma - mark collectionView Deleget & Data Sources
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [myGamesData count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    myGamesCollectionViewCell *cell;
    if(cell)
        cell = nil;
    
    if((indexPath.row%2) == 0)
    {
        static NSString *cellIdentifier = @"myGamesCollectionViewCell";
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    }
    else
    {
        static NSString *cellIdentifier = @"myGamesCollectionViewCellRight";
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    }
    
    if(cell.imgViewGameBanner.image)
        cell.imgViewGameBanner.image = nil;
    if(cell.lblGameTitle.text)
        cell.lblGameTitle.text = nil;
    
    if((myGamesData.count%2 == 0)  && ((indexPath.row == myGamesData.count-2)||(indexPath.row == myGamesData.count-1)))
    {
        cell.lblBottom.hidden = TRUE;
    }
    else if ((myGamesData.count%2 != 0) && (indexPath.row == myGamesData.count-1))
    {
        cell.lblBottom.hidden = TRUE;
    }
    
    NSString *gameBannerName = [[myGamesData objectAtIndex:indexPath.row]valueForKey:@"Game"];
    [cell.lblGameTitle setText:gameBannerName];
    
    NSString *imageUrlString =[imageUrlArray objectAtIndex:indexPath.row];
    
    if([imageUrlString isKindOfClass:[NSNull class]])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cell.imgViewGameBanner.image = [UIImage imageNamed:@"banner_Placeholder"] ;
        });
    }
    else
    {
        NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [cell.imgViewGameBanner sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"banner_Placeholder"]];
        });
    }
    
//    if([imageCache objectForKey:imageUrlString])
//    {
//        UIImage *gameBannerImage = [imageCache objectForKey:imageUrlString];
//        [cell.imgViewGameBanner setImage:gameBannerImage];
//    }
//    else
//    {
//        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//        dispatch_async(queue, ^(void) {
//            
//            NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                UIImage* gameBannerImage = [[UIImage alloc] initWithData:imageData];
//                [cell.imgViewGameBanner setImage:gameBannerImage];
//                [imageCache setObject:gameBannerImage forKey:imageUrlString];
//            });
//        });
//    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(IS_IPAD)
        return CGSizeMake(375.5, 379.5);
    else if(IS_IPHONE_5)
        return CGSizeMake(151.5, 194);
    else if (IS_IPHONE_6)
        return CGSizeMake(179, 224);
    else if (IS_IPHONE_6P)
        return CGSizeMake(198.5, 248);
    else
        return CGSizeMake(100, 140);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedGameID = [[myGamesData objectAtIndex:indexPath.row] valueForKey:@"GameID"];
    [[NSUserDefaults standardUserDefaults] setObject:selectedGameID forKey:@"Game_ID"];
    
    _commonWealthStatusValue = [[[myGamesData objectAtIndex:indexPath.row] valueForKey:@"IsCommonwealth"] boolValue];

    NSString *selectedGameUrlString = [[myGamesData objectAtIndex:indexPath.row] valueForKey:@"TemplatePath"];
    [[NSUserDefaults standardUserDefaults] setBool:[[[myGamesData objectAtIndex:indexPath.row] valueForKey:@"IsCommonwealth"] boolValue] forKey:Game_Type];
    
    //[[NSUserDefaults standardUserDefaults] setObject:[[myGamesData objectAtIndex:indexPath.row] valueForKey:@"IsCommonwealth"] forKey:Game_Type];
    
    if(![selectedGameUrlString isKindOfClass:[NSNull class]])
    {
        [[NSUserDefaults standardUserDefaults] setObject:selectedGameUrlString forKey:Game_ImageURL];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:Game_ImageURL];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];

    //myGamesCollectionViewCell *cell = (id)[self.gamesCollectionView cellForItemAtIndexPath:indexPath];
    [self CallServiceTogetGameDetails];
    
}

#pragma mark - TableView Delegats & Data Sources
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _dropDownTableView.bounds.size.height/4 ;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DropDownCustomCell *cell = (DropDownCustomCell *)[self.dropDownTableView dequeueReusableCellWithIdentifier:@"DropDownCustomCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DropDownCustomCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
    }
    
    [cell.lblTitleText setTextColor:color_white];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if(!IS_IPAD){
        [cell.lblTitleText setFont:app_font18];
    }
    
    cell.viewMsgCount.hidden = TRUE;
    
    switch (indexPath.row) {
        case 0:
        {
            cell.lblTitleText.text = @"My Games";
            cell.userInteractionEnabled = FALSE;
            cell.lblTitleText.alpha = 0.5;
        }
            break;

        case 1:
        {
            //cell.lblTitleText.text = [NSString stringWithFormat:@"Messages (%@)",self.messageCount];
            cell.lblTitleText.text = @"Messages";
            
            if ([self.messageCount integerValue] > 0) {
                cell.lblMsgCount.text = [NSString stringWithFormat:@"%@",self.messageCount];
                cell.viewMsgCount.hidden = FALSE;
                
                //Add Shadow to viewMsgCount
                cell.viewMsgCount.layer.shadowOffset = CGSizeMake(-1.0f, 1.0f);
                cell.viewMsgCount.layer.shadowRadius = 1.0f;
                cell.viewMsgCount.layer.shadowOpacity = 0.2;
                cell.viewMsgCount.layer.shadowColor = color_white.CGColor;
                
            }
            else {
                cell.viewMsgCount.hidden = TRUE;
            }
        }
            break;
        case 2:
        {
            cell.lblTitleText.text = @"Change Pin";
        }
            break;
        case 3:
        {
            cell.lblTitleText.text = @"Logout";
        }
            break;
            
        default:
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [_dropDownTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0:
        {
//            MyGamesViewController *myGameVC=[[MyGamesViewController alloc]initWithNibName:@"MyGamesViewController" bundle:nil];
//            [self.navigationController pushViewController:myGameVC animated:YES];
        }
            break;

        case 1:
        {
            NotificationListingViewController *notificationVC=[[NotificationListingViewController alloc]initWithNibName:@"NotificationListingViewController" bundle:nil];
            notificationVC.owner = self;
            
            if(self.presentingViewController)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:notificationVC animated:YES completion:nil];
                });
            }
            else
            {
                [self.navigationController pushViewController:notificationVC animated:YES];
            }

        }
            break;
        case 2:
        {
            ChangePasswordViewController *changePassword=[[ChangePasswordViewController alloc]initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"ChangePasswordViewController"] bundle:nil];
            
            if(self.presentingViewController)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:changePassword animated:YES completion:nil];
                });
            }
            else
            {
              [self.navigationController pushViewController:changePassword animated:YES];
            }
            
        }
            break;
        case 3:
        {
            [self CallServiceToLogout];
        }
            break;
            
        default:
            break;
    }
}

-(void)SetUpSubviews
{
    [_btnDropDown.layer setCornerRadius:_btnDropDown.bounds.size.width/2];
    [_btnDropDown.layer setMasksToBounds:YES];
    _btnDropDown.layer.borderWidth = 3.0;
    _btnDropDown.layer.borderColor = [UIColor whiteColor].CGColor;
    [_imageUserPic.layer setCornerRadius:_imageUserPic.bounds.size.width/2];
    [_imageUserPic.layer setMasksToBounds:YES];
}

#pragma - mark Drop Down Functinality Implementations
-(void)shouldShowDropDown:(BOOL)setShow
{
    //self.dropDownTableView.userInteractionEnabled = TRUE;
    _dropDownView.hidden = !setShow;
    self.dropDownBgImage.hidden = !setShow;
    flag = !setShow;
    if(setShow)
    {
        [self drawArrow];
        self.gamesCollectionView.userInteractionEnabled = FALSE;
    }
    else
    {
        [self clearArrow];
        self.gamesCollectionView.userInteractionEnabled = TRUE;
    }
}
-(void)drawArrow {
    NSInteger arrowSize;
    
    if(IS_IPAD){
        arrowSize = 17;
    }else{
        arrowSize = 13;
    }
    point1 = CGPointMake((_btnDropDown.frame.origin.x + _btnDropDown.frame.size.width*0.5f), (_btnDropDown.frame.origin.y + _btnDropDown.frame.size.height + 2));
    
    point2 = CGPointMake((_btnDropDown.frame.origin.x + 2) , (_btnDropDown.frame.origin.y +_btnDropDown.frame.size.height + arrowSize));
    
    point3 = CGPointMake((_btnDropDown.frame.origin.x + _btnDropDown.frame.size.width - 2) , (_btnDropDown.frame.origin.y + _btnDropDown.frame.size.height + arrowSize));
    
    line = [CAShapeLayer layer];
    linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:point1];
    [linePath addLineToPoint:point2];
    [linePath addLineToPoint:point3];
    [linePath addLineToPoint:point1];
    
    line.lineWidth = 1.5f;
    line.path=linePath.CGPath;
    line.fillColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:1.0].CGColor;
    line.strokeColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:1.0].CGColor;
    [[self.view layer] addSublayer:line];
    [self.view bringSubviewToFront:_dropDownView];
    
}

- (void)clearArrow {
    
    [line removeFromSuperlayer];
    [self.view setNeedsDisplay];
}

@end

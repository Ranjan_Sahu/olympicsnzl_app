//
//  TeamListingViewController
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "TeamListingViewController.h"

#define USE_MG_DELEGATE 1
@interface TeamListingViewController (){
    
    NSMutableArray *swipeCellButtons;
    NSDictionary *RecordDetailDictionery;
    NSMutableArray *SelectedRecordsArray;
    NSMutableArray *AllRecordsArray;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
}
@end

@implementation TeamListingViewController
@synthesize tableviewRecordList;

- (void)viewDidLoad {
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue912"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // Do any additional setup after loading the view from its nib.
    [self callServiceToGetlslist];
    self.isNeedToRefresh=FALSE;
}

- (IBAction)DidSelectBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
        [self callServiceToGetlslist];
    }
}

#pragma - mark WebService Call
-(void)callServiceToGetlslist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetTeamAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}


-(void)callServiceDelete{
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"TeamId" : [RecordDetailDictionery valueForKey:@"TeamId"]
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteTeamDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Delete;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if (_serviceCalled == Service_Called_Delete)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            [SelectedRecordsArray removeObjectAtIndex:currentIndexPathToDelete.row];
            [tableviewRecordList beginUpdates];
            [tableviewRecordList deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [tableviewRecordList endUpdates];
            currentIndexPathToDelete = nil;
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"TeamId == %@",[responseDictionery valueForKey:@"TeamId"]];
            NSArray *filteredValues = [AllRecordsArray filteredArrayUsingPredicate:filterPredicate];
            
            if(filteredValues.count){
                NSInteger
                index = [AllRecordsArray indexOfObject:[filteredValues lastObject]];
                //NSLog(@"%ld",(long)index);
                NSMutableDictionary *singleRecord = [[NSMutableDictionary alloc] initWithDictionary:[filteredValues lastObject]];
                [singleRecord setValue:@0 forKey:@"IsSelected"];
                [AllRecordsArray replaceObjectAtIndex:index withObject:[NSDictionary dictionaryWithDictionary:singleRecord]];
            }
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if (_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstTeamList"] isKindOfClass:[NSNull class]])
            {
                SelectedRecordsArray = [[NSMutableArray alloc] init];
                NSArray * TeamListArray = [responseDictionery objectForKey:@"lstTeamList"];
                for (NSDictionary *records in TeamListArray) {
                    
                    if([[records valueForKey:@"IsSelected"] intValue] != 0){
                        [SelectedRecordsArray addObject:records];
                    }
                }
                AllRecordsArray = [[NSMutableArray alloc] initWithArray:TeamListArray];
                
                if([SelectedRecordsArray count] > 0){
                    [self.tableviewRecordList reloadData];
                }
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/**************************** TABLEVIEW Functions *******************************/
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return SelectedRecordsArray.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [SelectedRecordsArray count]) {
        
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewRecordList dequeueReusableCellWithIdentifier:itemCell];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        CGFloat cellHeight = 30.0f;
        NSDictionary *dictionary = [SelectedRecordsArray objectAtIndex:indexPath.row];
        NSString *descriptionString = [[dictionary valueForKey:@"SportDiscipline"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"Team"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cellHeight = titleHeight + descriptionHeight + cellHeight;
        return cellHeight;
        
    }
    else{
        if(IS_IPAD) {
            return 84;
        }
        else{
            return 60;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row < [SelectedRecordsArray count])
    {
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewRecordList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        NSDictionary *dictionary = [SelectedRecordsArray objectAtIndex:indexPath.row];
        
        NSString *descriptionString = [[dictionary valueForKey:@"SportDiscipline"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"Team"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cell.titleHeightConstraint.constant = titleHeight;
        cell.descriptionHeightConstraint.constant = descriptionHeight;
        
        cell.lblAddressName.text = titleString;
        cell.lblDisplayAddress.text = descriptionString;
        
        cell.lblLock.hidden = TRUE;
        cell.delegate = self;
        return cell;
    }
    else
    {
        static NSString *itemCell = @"FooterCustomCell";
        FooterCustomCell *cell = (FooterCustomCell *)[self.tableviewRecordList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FooterCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.titleLabel.text = @"Add Team";
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [SelectedRecordsArray count]){
        
        AddTeamViewController *AddTeamObj = [[AddTeamViewController alloc]initWithNibName:@"AddTeamViewController" bundle:nil];
        AddTeamObj.isLogedInUsersDetail = _isLogedInUsersDetail;
        AddTeamObj.playerDetails = _playerDetails;
        AddTeamObj.serviceCalled = Service_Called_Add;
        AddTeamObj.RecordsArray = [[NSMutableArray alloc]initWithArray:AllRecordsArray];
        AddTeamObj.owner=self;
        [self.navigationController pushViewController:AddTeamObj animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


-(NSArray *) createRightButtons: (int) number height:(int)height width:(int)width
{
    NSMutableArray *result = [NSMutableArray array];
    NSString *titles[1] = {@"DELETE",};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *icons[1] = {delete};
    
    UIColor *colors[1] = {[UIColor colorWithRed:0.2431 green:0.2431 blue:0.2431 alpha:1]};
    
    for (int i = 0; i < 1; ++i)
    {
        
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i] backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:data.rightButtonsCount height:cell.frame.size.height/4*3 width:cell.frame.size.height];
    }
}
#endif

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    NSIndexPath *indexPath = [self.tableviewRecordList indexPathForCell:cell];
    RecordDetailDictionery = [SelectedRecordsArray objectAtIndex:indexPath.row];
    
    
    if (index == 0) {
        // Show popup.
        
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete Your Team Details?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         //Is User Wants to delete Details Then Continiue.
                                                                         //NSLog(@"Delete");
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDelete];
                                                                         
                                                                     }];
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // code to be written for version lower than ios 8.0...
        }
        //------------------------------------------------------------------------
        
    }
    
    return YES;
}

@end

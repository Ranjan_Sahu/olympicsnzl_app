//
//  MeasurementViewController.m
//  Olympics
//
//  Created by webwerks on 12/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "MeasurementViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface MeasurementViewController ()
{
    UIImageView *largeImageView;
    UIView *zoomView;
    BOOL isBtnSizeMenuTapped;
    NSString *dateMeasured;
    NSString *previousText,*currentText;
}
@end

@implementation MeasurementViewController

@synthesize previousLocation,incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue91b"];
    self.isChangesMade = FALSE;
    
    self.baseScrollView.delegate = self;
    self.rightScrollView.delegate = self;
    self.measurementContentView.backgroundColor = color_white;
    
    if(IS_IPAD){
        _widthRightScrollView.constant = 240;
        _trailingSpaceSeperatorLabel.constant = 240;
    }
    
    [self CallServiceTogetSubMenues];
    
    [_sideViewButton setBackgroundImage:[UIImage imageNamed:@"viewSize-menu"] forState:UIControlStateNormal];
    
    _SideviewImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(DidTapOnSideImageView:)];
    tapGesture.numberOfTapsRequired = 1;
    [tapGesture setDelegate:self];
    [_SideviewImage addGestureRecognizer:tapGesture];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
    [self.SlideView setUserInteractionEnabled:YES];
    [self.SlideView addGestureRecognizer:panGesture];
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)DidSelectBackButton:(id)sender
{
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if(_isLogedInUsersDetail)
    {
        [parameters setValue:@1 forKey:@"IsPersonalDetail"];
    }
    else
    {
        [parameters setValue:@0 forKey:@"IsPersonalDetail"];
    }

    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_MeasurementAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
    
}

-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    
    if(_isLogedInUsersDetail)
    {
        [userinformation setValue:@1 forKey:@"IsPersonalDetail"];
    }
    else
    {
        [userinformation setValue:@0 forKey:@"IsPersonalDetail"];
    }

    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateMeasurementAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Save;
    
}
#pragma mark - Method to add questions on screen

-(void)StartAddingControllers {
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    
    //Field_DatePicker
    int y = 9;
    if(IS_IPAD)
        y = 19;
    else if (IS_IPHONE_5)
        y = 4;
    OLMDatePicker *controlObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(10, y, self.dateView.frame.size.width-20, 1) Question:@"Capture date "   fieldIdName:@"DateMeasured" questionId:1 IsRequired:YES defaultValue:dateMeasured  delegate:self lineColor:color_lightGray];
    controlObj.questionTypeId = Field_DatePicker;
    controlObj.tag = 1;
    [controlls addObject:controlObj];
    [self.dateView addSubview:controlObj];
    
    for (int i = 0 ; i < [self.fieldsToShowArray count]; i++)
    {
        NSDictionary *QDictionery = [self.fieldsToShowArray objectAtIndex:i];
        
        //Field_NumericTextField
        NSString *qustStr = [NSString stringWithFormat:@"%@ ( Units In %@ )",[QDictionery valueForKey:@"MeasurementName"],[QDictionery valueForKey:@"Units"]];
        OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:qustStr  fieldIdName:[NSString stringWithFormat:@"%@",[QDictionery valueForKey:@"MeasurementId"]] questionId:[[QDictionery valueForKey:@"MeasurementId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[QDictionery valueForKey:@"MeasurementValue"] keyboardType:UIKeyboardTypeDefault  delegate:self lineColor:color_lightGray];
        
        controlObj.questionTypeId = Field_NumericTextField;
        controlObj.tag = i;
        [controlls addObject:controlObj];
        [self.baseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
        
        CGRect frameLbl =  CGRectMake(controlObj.frame.origin.x, controlObj.frame.origin.y, self.rightScrollView.frame.size.width - (controlObj.frame.origin.x * 2) - 30, controlObj.frame.size.height);
        UILabel *rightScrollLabel = [[UILabel alloc] initWithFrame:frameLbl];
        
        rightScrollLabel.font = app_font_bold_15;
        if(IS_IPAD)
            rightScrollLabel.font = app_font_bold_22;
        
        rightScrollLabel.textColor = color_gray;
        rightScrollLabel.backgroundColor = [UIColor clearColor];
        rightScrollLabel.textAlignment = NSTextAlignmentCenter;
        rightScrollLabel.text = [QDictionery valueForKey:@"DateMeasured"];
        [self.rightScrollView addSubview:rightScrollLabel];
        rightScrollLabel.hidden = FALSE;
    }
    incrementedHeight = incrementedHeight +30 ;
    
    self.isChangesMade = FALSE;
}

#pragma - mark Service Deleget

-(void)Service_Error:(id)error
{
}
-(void)Service_Success:(NSString *)responseStr
{
    if(_serviceCalled == Service_Called_GetDetail)
    {
        [COMMON_SETTINGS hideHUD];
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        NSString *measurementHTMlString = [responseDictionery valueForKey:@"MeasurementInfo"];
        [_headerWebView loadHTMLString:measurementHTMlString baseURL:nil];
        CGFloat lblHeight = SCREEN_HEIGHT/3.5;
        _heightHeaderView.constant = lblHeight;
        
        
        if(![[responseDictionery objectForKey:@"lstMeasurement"] isKindOfClass:[NSNull class]])
        {
            self.fieldsToShowArray = [responseDictionery objectForKey:@"lstMeasurement"];
            NSString *imageUrlStr = [responseDictionery objectForKey:@"MeasurementImageUrl"];
            imageUrlStr = [imageUrlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSData *imagedata = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrlStr]];
            _SideviewImage.image = [UIImage imageWithData:imagedata];

            if([self.fieldsToShowArray count] > 0)
            {
                dateMeasured = [[responseDictionery objectForKey:@"DateMeasured"]isKindOfClass:[NSNull class]]?@"":[responseDictionery objectForKey:@"DateMeasured"];
                
                [self StartAddingControllers];
                self.baseScrollView.contentSize = CGSizeMake(self.baseScrollView.frame.size.width, incrementedHeight + 20 );
                
                _SideviewImage.layer.masksToBounds = NO;
                _SideviewImage.layer.shadowRadius = 2;
                _SideviewImage.layer.shadowColor = [UIColor blackColor].CGColor;
                _SideviewImage.layer.shadowOpacity = 0.5;
                CGFloat indent = 1.5;
                CGRect innerRect = CGRectMake(indent,indent,_SideviewImage.frame.size.width-2*indent,_SideviewImage.frame.size.height-2*indent);
                _SideviewImage.layer.shadowPath = [UIBezierPath bezierPathWithRect:innerRect].CGPath;
                
                [self.baseScrollView setNeedsDisplay];
            }
        }
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                    error:&error];
        [COMMON_SETTINGS hideHUD];
        if([[responseDictionery valueForKey:@"Status"]boolValue])
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
}
-(void)no_Response_Function
{
    
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText =textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText =textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDatePicker Delegate
-(void)OLMDatepickerDidStartEditing:(UITextField*)textField
{
    previousText =textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}

-(void)OLMDatepickerDidEndEditing:(UITextField*)textField
{
    currentText =textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - Other IBAction methods
-(void)DidTapOnSideImageView:(UIImageView*)imageView
{
    if(zoomView) {
        [zoomView removeFromSuperview];
        zoomView = nil; }
    if(largeImageView) {
        [largeImageView removeFromSuperview];
        largeImageView = nil; }
    
    CGRect screenSize = [UIScreen mainScreen].bounds;
    zoomView = [[UIView alloc] initWithFrame:screenSize];
    [zoomView setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:zoomView];
    CGRect scrollFrame = CGRectMake(40, 40,screenSize.size.width - 80, screenSize.size.height - 80);
    UIImage *bigImage = _SideviewImage.image;
    largeImageView = [[UIImageView alloc] initWithImage:bigImage];
    largeImageView.frame = CGRectMake(0, 0, scrollFrame.size.width, scrollFrame.size.height);
    [largeImageView setContentMode:UIViewContentModeScaleAspectFit];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:scrollFrame];
    scrollView.minimumZoomScale = 1.0;
    scrollView.maximumZoomScale = 3.0;
    scrollView.delegate = self;
    [scrollView setBackgroundColor:[UIColor grayColor]];
    scrollView.showsVerticalScrollIndicator = false;
    scrollView.showsHorizontalScrollIndicator = false;
    [scrollView addSubview:largeImageView];
    scrollView.contentSize = largeImageView.frame.size;
    [zoomView addSubview:scrollView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(scrollView.frame.size.width+scrollView.frame.origin.x - 10, scrollView.frame.origin.y - 10, 20, 20)];
    [closeButton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(DidSelectCloseZoomView:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setBackgroundColor:color_white];
    closeButton.layer.cornerRadius = 10;
    closeButton.clipsToBounds = YES;
    [zoomView addSubview:closeButton];
    
    UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, zoomView.frame.size.width, 50)];
    info.textColor = color_lightGray;
    info.shadowColor = color_gray;
    info.font = app_font_bold_15;
    info.textAlignment = NSTextAlignmentCenter;
    info.text = @"PINCH OUT TO ZOOM";
    [zoomView addSubview:info];
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

-(void)DidSelectCloseZoomView:(UIButton*)sender
{
    for (UIView *view in zoomView.subviews) {
        [view removeFromSuperview];
    }
    [zoomView removeFromSuperview];
}

- (IBAction)DidSelectSideViewButton:(id)sender
{
    
    if(isBtnSizeMenuTapped)
    {
        isBtnSizeMenuTapped = FALSE;
        [self.view layoutIfNeeded];
        _ConstraintSlideViewTreallingSpace.constant = _sideViewButton.frame.size.width -_SlideView.frame.size.width;
        [UIView animateWithDuration:.5 animations:^{
            [self.view layoutIfNeeded];
        }];
        [_sideViewButton setBackgroundImage:[UIImage imageNamed:@"viewSize-menu"] forState:UIControlStateNormal];
    }
    else
    {
        isBtnSizeMenuTapped = TRUE;
        [self.view layoutIfNeeded];
        _ConstraintSlideViewTreallingSpace.constant = -2;
        [UIView animateWithDuration:0.5 animations:^{
            [self.view layoutIfNeeded];
        }];
        [_sideViewButton setBackgroundImage:[UIImage imageNamed:@"viewSize"] forState:UIControlStateNormal];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *dateCreated;
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
        {
            OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
            if(answersDict)
            {
                dateCreated = [[NSMutableDictionary alloc] initWithDictionary:answersDict];
                
            }
            else if(DatePickerObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Please Select Date" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    NSMutableArray *requestArray = [[NSMutableArray alloc] init];
    NSArray *keys = [requestParameters allKeys];
    for (int i = 0; i < [keys count]; i++) {
        NSString *ketstr = [NSString stringWithFormat:@"%@",[keys objectAtIndex:i]];
        NSDictionary *rdict = @{@"MeasurementId":ketstr,
                                @"MeasurementValue":[requestParameters valueForKey:ketstr]};
        [requestArray addObject:rdict];
    }
    [requestParameters removeAllObjects];
    [requestParameters setObject:requestArray forKey:@"lstMeasurement"];
    if(dateCreated)
    {
        [requestParameters addEntriesFromDictionary:dateCreated];
    }
    //hardcoded Value for now
    [requestParameters setValue:@1 forKey:@"IsPersonalDetail"];
    
    [self  CallServiceToSaveDataWithDictionary:requestParameters];

}

#pragma mark - Gesture Methods

-(void)handlePanGesture:(UIPanGestureRecognizer *)sender
{
    if ([sender state] == UIGestureRecognizerStateBegan)
    {
        previousLocation = [sender locationInView:self.view];
    }
    else if ([sender state] == UIGestureRecognizerStateChanged)
    {
        CGPoint temp = [sender locationInView:self.view];
        if(temp.x > previousLocation.x)
        {
            int maxXCordinate = _ConstraintSlideViewTreallingSpace.constant - (temp.x - previousLocation.x)*1.2;
            if(maxXCordinate > -_SlideView.frame.size.width)
            {
                _ConstraintSlideViewTreallingSpace.constant = maxXCordinate;
            }
            previousLocation = [sender locationInView:self.view];
            
        }
        else if(temp.x < previousLocation.x)
        {
            int maxXCordinate = _ConstraintSlideViewTreallingSpace.constant + (previousLocation.x - temp.x)*1.2;
            
            if(maxXCordinate < 0)
            {
                _ConstraintSlideViewTreallingSpace.constant = maxXCordinate;
            }
            previousLocation = [sender locationInView:self.view];
            
        }
    }
    else if ([sender state] == UIGestureRecognizerStateEnded)
    {
        if(_ConstraintSlideViewTreallingSpace.constant >-90)
        {
            [self.view layoutIfNeeded];
            _ConstraintSlideViewTreallingSpace.constant = -2;
            [UIView animateWithDuration:0.5 animations:^{
                [self.view layoutIfNeeded];
            }];
            [_sideViewButton setBackgroundImage:[UIImage imageNamed:@"viewSize"] forState:UIControlStateNormal];
        }
        else
        {
            [self.view layoutIfNeeded];
            _ConstraintSlideViewTreallingSpace.constant = _sideViewButton.frame.size.width -_SlideView.frame.size.width;
            [UIView animateWithDuration:.5 animations:^{
                [self.view layoutIfNeeded];
            }];
            [_sideViewButton setBackgroundImage:[UIImage imageNamed:@"viewSize-menu"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == self.baseScrollView)
    {
        [self.rightScrollView setContentOffset:scrollView.contentOffset animated:YES];
    }
    else if(scrollView == self.rightScrollView)
    {
        [self.baseScrollView setContentOffset:scrollView.contentOffset animated:YES];
    }
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end

//
//  PersonalDetailViewController.h
//  Olympics
//
//  Created by webwerks on 07/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "DynamicFormViewController.h"
#import "myProfileCollectionViewCell.h"
#import "AddressListingViewController.h"
#import "EmailListingViewController.h"
#import "ContactListngViewController.h"
#import "MeasurementViewController.h"
#import "DisabilityViewController.h"
#import "ResidenceViewController.h"
#import "PassportViewController.h"
#import "FinancialViewController.h"
#import "AccreditationViewController.h"
#import "GamesViewController.h"
#import "EventListViewController.h"
#import "NextOfKinListViewController.h"
#import "SportsListingViewController.h"
#import "TeamListingViewController.h"
#import "EventListViewController.h"
#import "OutfitingViewController.h"
#import "AdhocViewController.h"


@interface PersonalDetailViewController : UIViewController<Service_delegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *subMenuArray;
    CGRect savedNewFrame;
    UIPanGestureRecognizer *previousGesture;
}
@property (weak, nonatomic) IBOutlet UICollectionView *subViewCollectionView;
@property (weak, nonatomic) IBOutlet UIView *expendView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *viewCollection;
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserLargePic;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfileSmall;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintFullImage;
@property (weak, nonatomic) IBOutlet UIImageView *blackVinightImage;

@property (weak, nonatomic) IBOutlet UILabel *userEmailAddress;
@property (weak, nonatomic) IBOutlet UILabel *userMobileNo;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userSport;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;

@property (assign, nonatomic) CGPoint previousLocation;

@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (weak, nonatomic) IBOutlet UIImageView *headerBgImage;
@property (nonatomic, assign) BOOL commonWealthStatus;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightCollectionView;

- (IBAction)DidSelectBackButton:(id)sender;

@end
